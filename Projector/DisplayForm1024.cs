using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Projector
{
   public partial class DisplayForm1024 : Form
   {
      public DisplayForm1024(string location)
      {
         SetStyle(ControlStyles.SupportsTransparentBackColor, true);
         InitializeComponent();
         InitializeMyComponent(location);
      }

      #region InitializeMyComponent
      Label lblSubject;
      Label lblSpeaker;
      Label lblSongsTranslator;
      Label lblVolChapSecPage;
      Label lblScripture;
      Label lblNewPoem;
      Label lblOldPoem;
      Label lblName2;
      Label lblName3;
      private void InitializeMyComponent(string location)
      {
         lblSubject = new Label();
         lblSpeaker = new Label();
         lblSongsTranslator = new Label();
         lblVolChapSecPage = new Label();
         lblScripture = new Label();
         lblNewPoem = new Label();
         lblOldPoem = new Label();
         lblName2 = new Label();
         lblName3 = new Label();
         //
         // lblSubject
         //
         //lblSubject.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
         //            | System.Windows.Forms.AnchorStyles.Right)));
         lblSubject.BackColor = System.Drawing.Color.Transparent;
         //lblSubject.Font = new System.Drawing.Font(ShareMem.oMainFrmAlias.lblSubject.Font.Name,
         //                                          ShareMem.oMainFrmAlias.lblSubject.Font.Size * ShareMem.Factor,
         //                                          ShareMem.oMainFrmAlias.lblSubject.Font.Style,
         lblSubject.Font = new System.Drawing.Font(ShareMem.oMainFrmAlias.txtBoxSubject.Font.Name,
                                                   ShareMem.oMainFrmAlias.txtBoxSubject.Font.Size * ShareMem.Factor,
                                                   ShareMem.oMainFrmAlias.txtBoxSubject.Font.Style,
                                                   System.Drawing.GraphicsUnit.Point,
                                                   ((byte)(136)));
         //lblSubject.Location = new System.Drawing.Point((int)(ShareMem.oMainFrmAlias.lblSubject.Location.X * ShareMem.Factor),
         //                                               (int)(ShareMem.oMainFrmAlias.lblSubject.Location.Y * ShareMem.Factor));
         //lblSubject.Location = new System.Drawing.Point((int)(ShareMem.oMainFrmAlias.txtBoxSubject.Location.X * ShareMem.Factor),
         //                                               (int)((ShareMem.oMainFrmAlias.txtBoxSubject.Location.Y + 2) * ShareMem.Factor));
         SetSubjectLocation(location);
         lblSubject.Margin = new System.Windows.Forms.Padding(20, 20, 20, 0);
         lblSubject.Name = "lblSubject";
         //lblSubject.Size = new System.Drawing.Size((int)(ShareMem.oMainFrmAlias.lblSubject.Size.Width * ShareMem.Factor),
         //                                          (int)(ShareMem.oMainFrmAlias.lblSubject.Size.Height * ShareMem.Factor));
         lblSubject.Size = new System.Drawing.Size((int)(ShareMem.oMainFrmAlias.txtBoxSubject.Size.Width * ShareMem.Factor),
                                                   (int)(ShareMem.oMainFrmAlias.txtBoxSubject.Size.Height * ShareMem.Factor));
         lblSubject.TabIndex = 0;
         lblSubject.ForeColor = ShareMem.oMainFrmAlias.colorDialog1.Color;
         lblSubject.Text = "";
         lblSubject.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
         this.Controls.Add(lblSubject);
         // 
         // lblSpeaker
         // 
         this.lblSpeaker.BackColor = System.Drawing.Color.Transparent;
         this.lblSpeaker.Font = new System.Drawing.Font(ShareMem.oMainFrmAlias.txtBoxName1.Font.Name,
                                                     ShareMem.oMainFrmAlias.txtBoxName1.Font.Size * ShareMem.Factor,
                                                     ShareMem.oMainFrmAlias.txtBoxName1.Font.Style,
                                                     System.Drawing.GraphicsUnit.Point,
                                                     ((byte)(136)));
         this.lblSpeaker.Location = new System.Drawing.Point((int)((ShareMem.oMainFrmAlias.txtBoxName1.Location.X + 1) * ShareMem.Factor),
                                                          (int)(ShareMem.oMainFrmAlias.txtBoxName1.Location.Y * ShareMem.Factor));
         this.lblSpeaker.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
         this.lblSpeaker.Name = "lblNameTitle";
         this.lblSpeaker.Size = new System.Drawing.Size((int)((ShareMem.oMainFrmAlias.txtBoxName1.Size.Width + 80) * ShareMem.Factor),
                                                     (int)(ShareMem.oMainFrmAlias.txtBoxName1.Size.Height * ShareMem.Factor));
         this.lblSpeaker.TabIndex = 2;
         //this.lblNameTitle.Text = "  魏腓利 傳道";
         this.lblSpeaker.ForeColor = ShareMem.oMainFrmAlias.colorDialog2.Color;
         this.lblSpeaker.Text = "";
         this.lblSpeaker.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
         this.Controls.Add(lblSpeaker);
         // 
         // lblSongsTranslator
         // 
         this.lblSongsTranslator.BackColor = System.Drawing.Color.Transparent;
         this.lblSongsTranslator.Font = new System.Drawing.Font(ShareMem.oMainFrmAlias.lblPoem1.Font.Name,
                                                     ShareMem.oMainFrmAlias.lblPoem1.Font.Size * ShareMem.Factor,
                                                     ShareMem.oMainFrmAlias.lblPoem1.Font.Style,
                                                     System.Drawing.GraphicsUnit.Point,
                                                     ((byte)(136)));
         this.lblSongsTranslator.Location = new System.Drawing.Point((int)((ShareMem.oMainFrmAlias.lblPoem1.Location.X - 24)* ShareMem.Factor),
                                                          (int)(ShareMem.oMainFrmAlias.lblPoem1.Location.Y * ShareMem.Factor) + 4);
         this.lblSongsTranslator.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
         this.lblSongsTranslator.Name = "lblPoemTranslator";
         this.lblSongsTranslator.Size = new System.Drawing.Size((int)((ShareMem.oMainFrmAlias.lblPoem1.Size.Width + 160) * ShareMem.Factor),
                                                     (int)(ShareMem.oMainFrmAlias.lblPoem1.Size.Height * ShareMem.Factor));
         this.lblSongsTranslator.TabIndex = 5;
         this.lblSongsTranslator.ForeColor = ShareMem.oMainFrmAlias.colorDialog2.Color;
         this.lblSongsTranslator.Text = "詩 " + string.Format("{0, 3:G}", "") + "、" + string.Format("{0, 3:G}", "") + " 首";
         //this.lblSongsTranslator.Text = "翻譯：吳逸凡 弟兄";
         this.lblSongsTranslator.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
         this.Controls.Add(lblSongsTranslator);
         // 
         // lblVolChapSecPage
         // 
         this.lblVolChapSecPage.BackColor = System.Drawing.Color.Transparent;
         this.lblVolChapSecPage.Font = new System.Drawing.Font(ShareMem.oMainFrmAlias.lblVolChapSecPage.Font.Name,
                                                     ShareMem.oMainFrmAlias.lblVolChapSecPage.Font.Size * ShareMem.Factor,
                                                     ShareMem.oMainFrmAlias.lblVolChapSecPage.Font.Style,
                                                     System.Drawing.GraphicsUnit.Point,
                                                     ((byte)(136)));
         this.lblVolChapSecPage.Location = new System.Drawing.Point((int)(ShareMem.oMainFrmAlias.lblVolChapSecPage.Location.X * ShareMem.Factor),
                                                          (int)(ShareMem.oMainFrmAlias.lblVolChapSecPage.Location.Y * ShareMem.Factor));
         this.lblVolChapSecPage.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
         this.lblVolChapSecPage.Name = "lblVolChapSecPage";
         this.lblVolChapSecPage.Size = new System.Drawing.Size((int)(ShareMem.oMainFrmAlias.lblVolChapSecPage.Size.Width * ShareMem.Factor),
                                                     (int)(ShareMem.oMainFrmAlias.lblVolChapSecPage.Size.Height * ShareMem.Factor));
         this.lblVolChapSecPage.TabIndex = 15;
         this.lblVolChapSecPage.Text = "";
         this.lblVolChapSecPage.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
         this.lblVolChapSecPage.Visible = false;
         this.Controls.Add(lblVolChapSecPage);
         // 
         // lblScripture
         // 
         this.lblScripture.BackColor = System.Drawing.Color.Transparent;
         this.lblScripture.Font = new System.Drawing.Font(ShareMem.oMainFrmAlias.lblScripture.Font.Name,
                                                     ShareMem.oMainFrmAlias.lblScripture.Font.Size * ShareMem.Factor,
                                                     ShareMem.oMainFrmAlias.lblScripture.Font.Style,
                                                     System.Drawing.GraphicsUnit.Point,
                                                     ((byte)(136)));
         //this.lblScripture.Location = new System.Drawing.Point((int)(ShareMem.oMainFrmAlias.lblScripture.Location.X * ShareMem.Factor) + 16,
         //                                                 (int)(ShareMem.oMainFrmAlias.lblScripture.Location.Y * ShareMem.Factor));
         SetScriptureLocation(location);
         this.lblScripture.Name = "lblScripture";
         //this.lblScripture.Size = new System.Drawing.Size((int)(ShareMem.oMainFrmAlias.lblScripture.Size.Width * ShareMem.Factor),
         //                                            (int)(ShareMem.oMainFrmAlias.lblScripture.Size.Height * ShareMem.Factor));
         SetScriptureSize(location);
         this.lblScripture.TabIndex = 38;
         this.lblScripture.Visible = false;
         this.Controls.Add(lblScripture);
         // 
         // lblNewPoem
         // 
         this.lblNewPoem.BackColor = System.Drawing.Color.Transparent;
         this.lblNewPoem.Font = new System.Drawing.Font(ShareMem.oMainFrmAlias.lblNewPoem.Font.Name,
                                                     ShareMem.oMainFrmAlias.lblNewPoem.Font.Size * ShareMem.Factor,
                                                     ShareMem.oMainFrmAlias.lblNewPoem.Font.Style,
                                                     System.Drawing.GraphicsUnit.Point,
                                                     ((byte)(136)));
         this.lblNewPoem.Location = new System.Drawing.Point((int)(ShareMem.oMainFrmAlias.lblNewPoem.Location.X * ShareMem.Factor),
                                                          (int)(ShareMem.oMainFrmAlias.lblNewPoem.Location.Y * ShareMem.Factor));
         this.lblNewPoem.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
         this.lblNewPoem.Name = "lblNewPoem";
         this.lblNewPoem.Size = new System.Drawing.Size((int)(ShareMem.oMainFrmAlias.lblNewPoem.Size.Width * ShareMem.Factor),
                                                     (int)(ShareMem.oMainFrmAlias.lblNewPoem.Size.Height * ShareMem.Factor));
         //this.lblNewPoem.TabIndex = 5;
         this.lblNewPoem.Text = "";
         this.lblNewPoem.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
         this.lblNewPoem.Visible = false;
         this.Controls.Add(lblNewPoem);
         // 
         // lblOldPoem
         // 
         this.lblOldPoem.BackColor = System.Drawing.Color.Transparent;
         this.lblOldPoem.Font = new System.Drawing.Font(ShareMem.oMainFrmAlias.lblOldPoem.Font.Name,
                                                     ShareMem.oMainFrmAlias.lblOldPoem.Font.Size * ShareMem.Factor,
                                                     ShareMem.oMainFrmAlias.lblOldPoem.Font.Style,
                                                     System.Drawing.GraphicsUnit.Point,
                                                     ((byte)(136)));
         this.lblOldPoem.Location = new System.Drawing.Point((int)(ShareMem.oMainFrmAlias.lblOldPoem.Location.X * ShareMem.Factor),
                                                          (int)(ShareMem.oMainFrmAlias.lblOldPoem.Location.Y * ShareMem.Factor));
         this.lblOldPoem.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
         this.lblOldPoem.Name = "lblOldPoem";
         this.lblOldPoem.Size = new System.Drawing.Size((int)(ShareMem.oMainFrmAlias.lblOldPoem.Size.Width * ShareMem.Factor),
                                                     (int)(ShareMem.oMainFrmAlias.lblOldPoem.Size.Height * ShareMem.Factor));
         //this.lblOldPoem.TabIndex = 5;
         this.lblOldPoem.Text = "";
         this.lblOldPoem.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
         this.lblOldPoem.Visible = false;
         this.Controls.Add(lblOldPoem);
         // 
         // lblName2
         // 
         this.lblName2.BackColor = System.Drawing.Color.Transparent;
         this.lblName2.Font = new System.Drawing.Font(ShareMem.oMainFrmAlias.lblName2.Font.Name,
                                                     ShareMem.oMainFrmAlias.lblName2.Font.Size * ShareMem.Factor * (float)0.8,
                                                     ShareMem.oMainFrmAlias.lblName2.Font.Style,
                                                     System.Drawing.GraphicsUnit.Point,
                                                     ((byte)(136)));
         this.lblName2.Location = new System.Drawing.Point((int)(ShareMem.oMainFrmAlias.lblName2.Location.X * ShareMem.Factor),
                                                          (int)(ShareMem.oMainFrmAlias.lblName2.Location.Y * ShareMem.Factor));
         this.lblName2.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
         this.lblName2.Name = "lblOldPoem";
         this.lblName2.Size = new System.Drawing.Size((int)(ShareMem.oMainFrmAlias.lblName2.Size.Width * ShareMem.Factor),
                                                     (int)(ShareMem.oMainFrmAlias.lblName2.Size.Height * ShareMem.Factor));
         //this.lblName2.TabIndex = 5;
         this.lblName2.Text = "";
         this.lblName2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
         this.lblName2.Visible = false;
         this.Controls.Add(lblName2);
         // 
         // lblName3
         // 
         this.lblName3.BackColor = System.Drawing.Color.Transparent;
         this.lblName3.Font = new System.Drawing.Font(ShareMem.oMainFrmAlias.lblName3.Font.Name,
                                                     ShareMem.oMainFrmAlias.lblName3.Font.Size * ShareMem.Factor * (float)0.8,
                                                     ShareMem.oMainFrmAlias.lblName3.Font.Style,
                                                     System.Drawing.GraphicsUnit.Point,
                                                     ((byte)(136)));
         this.lblName3.Location = new System.Drawing.Point((int)(ShareMem.oMainFrmAlias.lblName3.Location.X * ShareMem.Factor),
                                                          (int)(ShareMem.oMainFrmAlias.lblName3.Location.Y * ShareMem.Factor));
         this.lblName3.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
         this.lblName3.Name = "lblOldPoem";
         this.lblName3.Size = new System.Drawing.Size((int)(ShareMem.oMainFrmAlias.lblName3.Size.Width * ShareMem.Factor),
                                                     (int)(ShareMem.oMainFrmAlias.lblName3.Size.Height * ShareMem.Factor));
         //this.lblName2.TabIndex = 5;
         this.lblName3.Text = "";
         this.lblName3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
         this.lblName3.Visible = false;
         this.Controls.Add(lblName3);
      }
      #endregion

      private void DisplayForm1024_Shown(object sender, EventArgs e)
      {
      }

      private void DisplayForm1024_Paint(object sender, PaintEventArgs e)
      {
         DrawRectangle(lblSpeaker.ForeColor,
                       new Point((int)(6 * ShareMem.Factor), (int)(52 * ShareMem.Factor)),
                       new Size((int)(384 * ShareMem.Factor) + 4, (int)(35 * ShareMem.Factor) + 4));
      }

      private void DrawRectangle(Color color, Point pt, Size size)
      {
         //Draw a rectangle
         //Sets g to a graphics object
         Graphics g = this.CreateGraphics();
         //Create pen
         Pen p = new Pen(color, (int)(1 * ShareMem.Factor));
         //Draw outer rectangle to form
         g.DrawRectangle(p, pt.X + 4, pt.Y, size.Width, size.Height);
         //Destroy a object
         g.Dispose();
         p.Dispose();
      }

      public void SetTextOfSuject(string strSubject)
      {
         if (!lblSubject.Text.Equals(strSubject))
            lblSubject.Text = strSubject;
      }

      public void SetTextOfSpeakerSongsTranslator(string strSpeaker, string strSongsTranslator)
      {
         if (!lblSpeaker.Text.Equals(strSpeaker))
            lblSpeaker.Text = strSpeaker;
         if (!lblSongsTranslator.Text.Equals(strSongsTranslator))
         {
            if (strSongsTranslator.StartsWith("詩"))
               lblSongsTranslator.Font =  new System.Drawing.Font(ShareMem.oMainFrmAlias.lblPoem1.Font.Name,
                                                     ShareMem.oMainFrmAlias.lblPoem1.Font.Size * ShareMem.Factor,
                                                     ShareMem.oMainFrmAlias.lblPoem1.Font.Style,
                                                     System.Drawing.GraphicsUnit.Point,
                                                     ((byte)(136)));
            else
               lblSongsTranslator.Font =  new System.Drawing.Font(ShareMem.oMainFrmAlias.lblPoem1.Font.Name,
                                                     ShareMem.oMainFrmAlias.lblPoem1.Font.Size * ShareMem.Factor * (float)0.8,
                                                     ShareMem.oMainFrmAlias.lblPoem1.Font.Style,
                                                     System.Drawing.GraphicsUnit.Point,
                                                     ((byte)(136)));
            lblSongsTranslator.Text = strSongsTranslator;
         }
      }

      public void SetShowStatus(bool showFlag)
      {
          if (showFlag == true)
          {
              lblSpeaker.Visible = true;
              lblSongsTranslator.Visible = true;
          }
          else
          {
              lblSpeaker.Visible = false;
              lblSongsTranslator.Visible = false;
          }
      }

      public void SetTextOfVolChapSecPageScripture(string strVolChapSecPage,
                                                   string strScripture)
      {
         lblVolChapSecPage.Hide();    //隱藏經節
         lblScripture.Show();   //顯示打字
         lblOldPoem.Hide();
         lblNewPoem.Hide();
         lblName2.Hide();
         lblName3.Hide();

         if (strVolChapSecPage.Equals("HIDE")) lblVolChapSecPage.Hide();    //隱藏經節
         else 
         {
             lblVolChapSecPage.Show(); //顯示經節
             if (!lblVolChapSecPage.Text.Equals(strVolChapSecPage))
                lblVolChapSecPage.Text = strVolChapSecPage;
         }
         if (!lblScripture.Text.Equals(strScripture))
             lblScripture.Text = strScripture;
      }

      public void SetTextOfPoemName2(string strNewPoem, string strOldPoem,
                                     string strName2, string strName3)
      {
         lblScripture.Hide();   //隱藏打字
         lblVolChapSecPage.Hide();    //隱藏經節
         lblOldPoem.Show();
         lblNewPoem.Show();
         lblName2.Show();
         lblName3.Show();

         if (!lblNewPoem.Text.Equals(strNewPoem))
            lblNewPoem.Text = strNewPoem;
         if (!lblOldPoem.Text.Equals(strOldPoem))
            lblOldPoem.Text = strOldPoem;
         if (!lblName2.Text.Equals(strName2))
            lblName2.Text = strName2;
         if (!lblName3.Text.Equals(strName3))
            lblName3.Text = strName3;

      }

      public void SetFontOfSubject(Font font)
      {
         if (!lblSubject.Font.Equals(font))
            lblSubject.Font = new Font(font.Name,
                                       (float)font.Size * (float)ShareMem.Factor,
                                       font.Style,
                                       System.Drawing.GraphicsUnit.Point,
                                       ((byte)(136)));
      }

      public void SetColorOfSuject(Color color)
      {
         if (!lblSubject.ForeColor.Equals(color))
            lblSubject.ForeColor = color;
      }

      public void SetFontOfOthers(Font font)
      {
         if (!lblScripture.Font.Equals(font))
            lblScripture.Font = new Font(font.Name,
                                         (float)font.Size * (float)ShareMem.Factor,
                                         font.Style,
                                         System.Drawing.GraphicsUnit.Point,
                                         ((byte)(136)));
         //lblSong1.Font = new Font("Arial",
         //                         (float)font.Size * (float)ShareMem.Factor,
         //                         font.Style,
         //                         System.Drawing.GraphicsUnit.Point,
         //                         ((byte)(136)));
      }

      public void SetColorOfTitle(Color color)
      {
         if (!lblSpeaker.ForeColor.Equals(color))
         {
            lblSpeaker.ForeColor = color;
            lblSongsTranslator.ForeColor = lblSpeaker.ForeColor;
         }
      }

      public void SetColorOfOthers(Color color)
      {
         if (!lblVolChapSecPage.ForeColor.Equals(color))
         {
            lblVolChapSecPage.ForeColor = color;
            lblScripture.ForeColor = lblVolChapSecPage.ForeColor;

            lblNewPoem.ForeColor = lblVolChapSecPage.ForeColor;
            lblOldPoem.ForeColor = lblVolChapSecPage.ForeColor;
            lblName2.ForeColor = lblVolChapSecPage.ForeColor;
            lblName3.ForeColor = lblVolChapSecPage.ForeColor;
         }
      }

      public void SwitchProc()
      {
         switch (ShareMem.preDisplayMode)
         {
            case ShareMem.DisplayMode.Scripture:
               if (lblVolChapSecPage.Visible)
                  lblVolChapSecPage.Visible = false;
               if (lblScripture.Visible)
                  lblScripture.Visible = false;
               break;
            case ShareMem.DisplayMode.LoadText:
            case ShareMem.DisplayMode.Text:
               if (lblScripture.Visible)
                  lblScripture.Visible = false;
               break;
            case ShareMem.DisplayMode.Poem:
               if (lblNewPoem.Visible)
                  lblNewPoem.Visible = false;
               if (lblOldPoem.Visible)
                  lblOldPoem.Visible = false;
               if (lblName2.Visible)
                  lblName2.Visible = false;
               if (lblName3.Visible)
                  lblName3.Visible = false;
               break;
            default:
               break;
         }

         switch (ShareMem.curDisplayMode)
         {
            case ShareMem.DisplayMode.Scripture:
               if (!lblVolChapSecPage.Visible) 
                  lblVolChapSecPage.Visible = true;
               if (!lblScripture.Visible) 
                  lblScripture.Visible = true;
               break;
            case ShareMem.DisplayMode.LoadText:
            case ShareMem.DisplayMode.Text:
               if (!lblScripture.Visible)
                  lblScripture.Visible = true;
               break;
            case ShareMem.DisplayMode.Poem:
               if (!lblNewPoem.Visible)
                  lblNewPoem.Visible = true;
               if (!lblOldPoem.Visible)
                  lblOldPoem.Visible = true;
               if (!lblName2.Visible)
                  lblName2.Visible = true;
               if (!lblName3.Visible)
                  lblName3.Visible = true;
               break;
            default:
               break;
         }
      }

      public void ClearProc(ShareMem.DisplayMode clearDisplayMode)
      {
         switch (clearDisplayMode)
         {
            case ShareMem.DisplayMode.Scripture:
               if (lblVolChapSecPage.Visible)
                  lblVolChapSecPage.Text = "";
               if (lblScripture.Visible && ShareMem.curDisplayMode != ShareMem.DisplayMode.LoadText
                                        && ShareMem.curDisplayMode != ShareMem.DisplayMode.Text)
                  lblScripture.Text = "";
               break;
            case ShareMem.DisplayMode.LoadText:
               if (lblScripture.Visible && ShareMem.curDisplayMode != ShareMem.DisplayMode.Scripture
                                        && ShareMem.curDisplayMode != ShareMem.DisplayMode.Text)
                  lblScripture.Text = "";
               break;
            case ShareMem.DisplayMode.Text:
               if (lblScripture.Visible && ShareMem.curDisplayMode != ShareMem.DisplayMode.Scripture
                                        && ShareMem.curDisplayMode != ShareMem.DisplayMode.LoadText)
                  lblScripture.Text = "";
               break;
            case ShareMem.DisplayMode.Poem:
               if (lblNewPoem.Visible)
                  lblNewPoem.Text = "";
               if (lblOldPoem.Visible)
                  lblOldPoem.Text = "";
               if (lblName2.Visible)
                  lblName2.Text = "";
               if (lblName3.Visible)
                  lblName3.Text = "";
               break;
            default:
               break;
         }
      }

      //套用
      public void SetScriptureLocation(string location)
      {
          if (String.Compare(location, "L", true) == 0)
              this.lblScripture.Location = new System.Drawing.Point((int)(ShareMem.oMainFrmAlias.lblScripture.Location.X * ShareMem.Factor) + 16,
                                                                    (int)(ShareMem.oMainFrmAlias.lblScripture.Location.Y * ShareMem.Factor));

          else if (String.Compare(location, "R", true) == 0)
              this.lblScripture.Location = new System.Drawing.Point((int)(ShareMem.oMainFrmAlias.lblScripture1.Location.X * ShareMem.Factor) + 16,
                                                                    (int)(ShareMem.oMainFrmAlias.lblScripture1.Location.Y * ShareMem.Factor));
      }

      public void SetScriptureSize(string location)
      {
          if (String.Compare(location, "L", true) == 0)
              this.lblScripture.Size = new System.Drawing.Size((int)(ShareMem.oMainFrmAlias.lblScripture.Size.Width * ShareMem.Factor),
                                                               (int)(ShareMem.oMainFrmAlias.lblScripture.Size.Height * ShareMem.Factor));

          else if (String.Compare(location, "R", true) == 0)
              this.lblScripture.Size = new System.Drawing.Size((int)(ShareMem.oMainFrmAlias.lblScripture1.Size.Width * ShareMem.Factor),
                                                               (int)(ShareMem.oMainFrmAlias.lblScripture1.Size.Height * ShareMem.Factor));
      }

      public void SetSubjectLocation(string location)
      {
          if (String.Compare(location, "L", true) == 0)
              lblSubject.Location = new System.Drawing.Point((int)(ShareMem.subjectX * ShareMem.Factor),
                                                             (int)((ShareMem.subjectY + 2) * ShareMem.Factor));
          else if (String.Compare(location, "R", true) == 0)
              lblSubject.Location = new System.Drawing.Point((int)(ShareMem.subjectX1 * ShareMem.Factor),
                                                             (int)((ShareMem.subjectY1 + 2) * ShareMem.Factor));
      }
   }
}