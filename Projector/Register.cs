using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Win32;
using System.IO;
using System.Drawing;

namespace Projector
{
   public class Register
   {
      RegistryKey SoftKey;
      RegistryKey STCChurchKey;
      RegistryKey ProjectorKey;

      public Register()
      {
      }

      public Size GetSizeOfHostKey()
      {
         Size SizeOfHost = new Size(0, 0);

         SoftKey = Registry.LocalMachine.CreateSubKey("Software");
         STCChurchKey = SoftKey.CreateSubKey("STCChurch");
         ProjectorKey = STCChurchKey.CreateSubKey("Projector");

         SizeOfHost.Width = (int)ProjectorKey.GetValue("SizeOfHostW", 800);
         SizeOfHost.Height = (int)ProjectorKey.GetValue("SIzeOfHostH", 600);

         SoftKey.Close();
         STCChurchKey.Close();
         ProjectorKey.Close();

         return SizeOfHost;
      }

      public void SetSizeOfHostKey(Size SizeOfHost)
      {
         SoftKey = Registry.LocalMachine.CreateSubKey("Software");
         STCChurchKey = SoftKey.CreateSubKey("STCChurch");
         ProjectorKey = STCChurchKey.CreateSubKey("Projector");

         ProjectorKey.SetValue("SizeOfHostW", SizeOfHost.Width, RegistryValueKind.DWord);
         ProjectorKey.SetValue("SizeOfHostH", SizeOfHost.Height, RegistryValueKind.DWord);

         SoftKey.Close();
         STCChurchKey.Close();
         ProjectorKey.Close();
      }

      //取得投射的Size
      public Size GetSizeOfClientKey()
      {
         Size SizeOfClient = new Size(0, 0);

         SoftKey = Registry.LocalMachine.CreateSubKey("Software");
         STCChurchKey = SoftKey.CreateSubKey("STCChurch");
         ProjectorKey = STCChurchKey.CreateSubKey("Projector");

         SizeOfClient.Width = (int)ProjectorKey.GetValue("SizeOfClientW", 800);
         SizeOfClient.Height = (int)ProjectorKey.GetValue("SIzeOfClientH", 600);

         SoftKey.Close();
         STCChurchKey.Close();
         ProjectorKey.Close();

         return SizeOfClient;
      }

      //設定投射的Size
      public void SetSizeOfClientKey(Size SizeOfClient)
      {
         SoftKey = Registry.LocalMachine.CreateSubKey("Software");
         STCChurchKey = SoftKey.CreateSubKey("STCChurch");
         ProjectorKey = STCChurchKey.CreateSubKey("Projector");

         ProjectorKey.SetValue("SizeOfClientW", SizeOfClient.Width, RegistryValueKind.DWord);
         ProjectorKey.SetValue("SizeOfClientH", SizeOfClient.Height, RegistryValueKind.DWord);

         SoftKey.Close();
         STCChurchKey.Close();
         ProjectorKey.Close();
      }

      //取得投射的方向
      public string GetDisplayOfLocation()
      {
          string strName;

          SoftKey = Registry.LocalMachine.CreateSubKey("Software");
          STCChurchKey = SoftKey.CreateSubKey("STCChurch");
          ProjectorKey = STCChurchKey.CreateSubKey("Projector");

          strName = (string)ProjectorKey.GetValue("DisplayOfLocation", ShareMem.curDisplayLocation);

          SoftKey.Close();
          STCChurchKey.Close();
          ProjectorKey.Close();

          return strName;
      }

      //設定投射的方向
      public void SetDisplayOfLocation(string strName)
      {
          SoftKey = Registry.LocalMachine.CreateSubKey("Software");
          STCChurchKey = SoftKey.CreateSubKey("STCChurch");
          ProjectorKey = STCChurchKey.CreateSubKey("Projector");

          ProjectorKey.SetValue("DisplayOfLocation", strName, RegistryValueKind.String);

          SoftKey.Close();
          STCChurchKey.Close();
          ProjectorKey.Close();
      }

      //取得聽打起始座標-X軸(左投射)
      public int GetCurrentX()
      {
          int iCurrentX;

          SoftKey = Registry.LocalMachine.CreateSubKey("Software");
          STCChurchKey = SoftKey.CreateSubKey("STCChurch");
          ProjectorKey = STCChurchKey.CreateSubKey("Projector");

          iCurrentX = (int)ProjectorKey.GetValue("CurrentX", ShareMem.currentX);

          SoftKey.Close();
          STCChurchKey.Close();
          ProjectorKey.Close();

          return iCurrentX;
      }

      //設定聽打起始座標-X軸(左投射)
      public void SetCurrentX(int iCurrentX)
      {
          SoftKey = Registry.LocalMachine.CreateSubKey("Software");
          STCChurchKey = SoftKey.CreateSubKey("STCChurch");
          ProjectorKey = STCChurchKey.CreateSubKey("Projector");

          ProjectorKey.SetValue("CurrentX", iCurrentX, RegistryValueKind.DWord);

          SoftKey.Close();
          STCChurchKey.Close();
          ProjectorKey.Close();
      }

      //取得聽打起始座標-Y軸(左投射)
      public int GetCurrentY()
      {
          int iCurrentY;

          SoftKey = Registry.LocalMachine.CreateSubKey("Software");
          STCChurchKey = SoftKey.CreateSubKey("STCChurch");
          ProjectorKey = STCChurchKey.CreateSubKey("Projector");

          iCurrentY = (int)ProjectorKey.GetValue("CurrentY", ShareMem.currentY);

          SoftKey.Close();
          STCChurchKey.Close();
          ProjectorKey.Close();

          return iCurrentY;
      }

      //設定聽打起始座標-Y軸(左投射)
      public void SetCurrentY(int iCurrentY)
      {
          SoftKey = Registry.LocalMachine.CreateSubKey("Software");
          STCChurchKey = SoftKey.CreateSubKey("STCChurch");
          ProjectorKey = STCChurchKey.CreateSubKey("Projector");

          ProjectorKey.SetValue("CurrentY", iCurrentY, RegistryValueKind.DWord);

          SoftKey.Close();
          STCChurchKey.Close();
          ProjectorKey.Close();
      }

      //取得聽打可視大小-寬度(左投射)
      public int GetContentWidth()
      {
          int iContentWidth;

          SoftKey = Registry.LocalMachine.CreateSubKey("Software");
          STCChurchKey = SoftKey.CreateSubKey("STCChurch");
          ProjectorKey = STCChurchKey.CreateSubKey("Projector");

          iContentWidth = (int)ProjectorKey.GetValue("ContentWidth", ShareMem.contentWidth);

          SoftKey.Close();
          STCChurchKey.Close();
          ProjectorKey.Close();

          return iContentWidth;
      }

      //設定聽打可視大小-寬度(左投射)
      public void SetContentWidth(int iContentWidth)
      {
          SoftKey = Registry.LocalMachine.CreateSubKey("Software");
          STCChurchKey = SoftKey.CreateSubKey("STCChurch");
          ProjectorKey = STCChurchKey.CreateSubKey("Projector");

          ProjectorKey.SetValue("ContentWidth", iContentWidth, RegistryValueKind.DWord);

          SoftKey.Close();
          STCChurchKey.Close();
          ProjectorKey.Close();
      }

      //取得聽打可視大小-高度(左投射)
      public int GetContentHeight()
      {
          int iContentHeight;

          SoftKey = Registry.LocalMachine.CreateSubKey("Software");
          STCChurchKey = SoftKey.CreateSubKey("STCChurch");
          ProjectorKey = STCChurchKey.CreateSubKey("Projector");

          iContentHeight = (int)ProjectorKey.GetValue("ContentHeight", ShareMem.contentHeight);

          SoftKey.Close();
          STCChurchKey.Close();
          ProjectorKey.Close();

          return iContentHeight;
      }

      //設定聽打可視大小-高度(左投射)
      public void SetContentHeight(int iContentHeight)
      {
          SoftKey = Registry.LocalMachine.CreateSubKey("Software");
          STCChurchKey = SoftKey.CreateSubKey("STCChurch");
          ProjectorKey = STCChurchKey.CreateSubKey("Projector");

          ProjectorKey.SetValue("ContentHeight", iContentHeight, RegistryValueKind.DWord);

          SoftKey.Close();
          STCChurchKey.Close();
          ProjectorKey.Close();
      }

/*
      //取得可視字體大小
      public int GetContentFontSize()
      {
          int iContentFontSize;

          SoftKey = Registry.LocalMachine.CreateSubKey("Software");
          STCChurchKey = SoftKey.CreateSubKey("STCChurch");
          ProjectorKey = STCChurchKey.CreateSubKey("Projector");

          iContentFontSize = (int)ProjectorKey.GetValue("ContentFontSize", ShareMem.contentFontSize);

          SoftKey.Close();
          STCChurchKey.Close();
          ProjectorKey.Close();

          return iContentFontSize;
      }

      //設定可視字體大小
      public void SetContentFontSize(int iContentFontSize)
      {
          SoftKey = Registry.LocalMachine.CreateSubKey("Software");
          STCChurchKey = SoftKey.CreateSubKey("STCChurch");
          ProjectorKey = STCChurchKey.CreateSubKey("Projector");

          ProjectorKey.SetValue("ContentFontSize", iContentFontSize, RegistryValueKind.DWord);

          SoftKey.Close();
          STCChurchKey.Close();
          ProjectorKey.Close();
      }
*/

      //取得講題起始座標-X軸(左投射)
      public int GetSubjectX()
      {
          int iSubjectX;

          SoftKey = Registry.LocalMachine.CreateSubKey("Software");
          STCChurchKey = SoftKey.CreateSubKey("STCChurch");
          ProjectorKey = STCChurchKey.CreateSubKey("Projector");

          iSubjectX = (int)ProjectorKey.GetValue("SubjectX", ShareMem.subjectX);

          SoftKey.Close();
          STCChurchKey.Close();
          ProjectorKey.Close();

          return iSubjectX;
      }

      //設定講題起始座標-X軸(左投射)
      public void SetSubjectX(int iSubjectX)
      {
          SoftKey = Registry.LocalMachine.CreateSubKey("Software");
          STCChurchKey = SoftKey.CreateSubKey("STCChurch");
          ProjectorKey = STCChurchKey.CreateSubKey("Projector");

          ProjectorKey.SetValue("SubjectX", iSubjectX, RegistryValueKind.DWord);

          SoftKey.Close();
          STCChurchKey.Close();
          ProjectorKey.Close();
      }

      //取得講題起始座標-Y軸(左投射)
      public int GetSubjectY()
      {
          int iSubjectY;

          SoftKey = Registry.LocalMachine.CreateSubKey("Software");
          STCChurchKey = SoftKey.CreateSubKey("STCChurch");
          ProjectorKey = STCChurchKey.CreateSubKey("Projector");

          iSubjectY = (int)ProjectorKey.GetValue("SubjectY", ShareMem.subjectY);

          SoftKey.Close();
          STCChurchKey.Close();
          ProjectorKey.Close();

          return iSubjectY;
      }

      //設定講題起始座標-Y軸(左投射)
      public void SetSubjectY(int iSubjectY)
      {
          SoftKey = Registry.LocalMachine.CreateSubKey("Software");
          STCChurchKey = SoftKey.CreateSubKey("STCChurch");
          ProjectorKey = STCChurchKey.CreateSubKey("Projector");

          ProjectorKey.SetValue("SubjectY", iSubjectY, RegistryValueKind.DWord);

          SoftKey.Close();
          STCChurchKey.Close();
          ProjectorKey.Close();
      }

      //取得講題可視大小-寬度(左投射)
      public int GetSubjectWidth()
      {
          int iSubjectWidth;

          SoftKey = Registry.LocalMachine.CreateSubKey("Software");
          STCChurchKey = SoftKey.CreateSubKey("STCChurch");
          ProjectorKey = STCChurchKey.CreateSubKey("Projector");

          iSubjectWidth = (int)ProjectorKey.GetValue("SubjectWidth", ShareMem.subjectWidth);

          SoftKey.Close();
          STCChurchKey.Close();
          ProjectorKey.Close();

          return iSubjectWidth;
      }

      //設定講題可視大小-寬度(左投射)
      public void SetSubjectWidth(int iSubjectWidth)
      {
          SoftKey = Registry.LocalMachine.CreateSubKey("Software");
          STCChurchKey = SoftKey.CreateSubKey("STCChurch");
          ProjectorKey = STCChurchKey.CreateSubKey("Projector");

          ProjectorKey.SetValue("SubjectWidth", iSubjectWidth, RegistryValueKind.DWord);

          SoftKey.Close();
          STCChurchKey.Close();
          ProjectorKey.Close();
      }

      //取得講題可視大小-高度(左投射)
      public int GetSubjectHeight()
      {
          int iSubjectHeight;

          SoftKey = Registry.LocalMachine.CreateSubKey("Software");
          STCChurchKey = SoftKey.CreateSubKey("STCChurch");
          ProjectorKey = STCChurchKey.CreateSubKey("Projector");

          iSubjectHeight = (int)ProjectorKey.GetValue("SubjectHeight", ShareMem.subjectHeight);

          SoftKey.Close();
          STCChurchKey.Close();
          ProjectorKey.Close();

          return iSubjectHeight;
      }

      //設定講題可視大小-高度(左投射)
      public void SetSubjectHeight(int iSubjectHeight)
      {
          SoftKey = Registry.LocalMachine.CreateSubKey("Software");
          STCChurchKey = SoftKey.CreateSubKey("STCChurch");
          ProjectorKey = STCChurchKey.CreateSubKey("Projector");

          ProjectorKey.SetValue("SubjectHeight", iSubjectHeight, RegistryValueKind.DWord);

          SoftKey.Close();
          STCChurchKey.Close();
          ProjectorKey.Close();
      }

      //取得聽打可視行數(左投射)
      public int GetContentLines()
      {
          int iContentLines;

          SoftKey = Registry.LocalMachine.CreateSubKey("Software");
          STCChurchKey = SoftKey.CreateSubKey("STCChurch");
          ProjectorKey = STCChurchKey.CreateSubKey("Projector");

          iContentLines = (int)ProjectorKey.GetValue("ContentLines", ShareMem.contentLines);

          SoftKey.Close();
          STCChurchKey.Close();
          ProjectorKey.Close();

          return iContentLines;
      }

      //設定聽打可視行數(左投射)
      public void SetContentLines(int iContentLines)
      {
          SoftKey = Registry.LocalMachine.CreateSubKey("Software");
          STCChurchKey = SoftKey.CreateSubKey("STCChurch");
          ProjectorKey = STCChurchKey.CreateSubKey("Projector");

          ProjectorKey.SetValue("ContentLines", iContentLines, RegistryValueKind.DWord);

          SoftKey.Close();
          STCChurchKey.Close();
          ProjectorKey.Close();
      }

      //取得聽打每行Max字數(左投射)
      public int GetContentLength()
      {
          int iContentLength;

          SoftKey = Registry.LocalMachine.CreateSubKey("Software");
          STCChurchKey = SoftKey.CreateSubKey("STCChurch");
          ProjectorKey = STCChurchKey.CreateSubKey("Projector");

          iContentLength = (int)ProjectorKey.GetValue("ContentLength", ShareMem.contentLength);

          SoftKey.Close();
          STCChurchKey.Close();
          ProjectorKey.Close();

          return iContentLength;
      }

      //設定聽打每行Max字數(左投射)
      public void SetContentLength(int iContentLength)
      {
          SoftKey = Registry.LocalMachine.CreateSubKey("Software");
          STCChurchKey = SoftKey.CreateSubKey("STCChurch");
          ProjectorKey = STCChurchKey.CreateSubKey("Projector");

          ProjectorKey.SetValue("ContentLength", iContentLength, RegistryValueKind.DWord);

          SoftKey.Close();
          STCChurchKey.Close();
          ProjectorKey.Close();
      }

      //取得註解(左投射)
      public string GetMemo()
      {
          string strMemo;

          SoftKey = Registry.LocalMachine.CreateSubKey("Software");
          STCChurchKey = SoftKey.CreateSubKey("STCChurch");
          ProjectorKey = STCChurchKey.CreateSubKey("Projector");

          strMemo = (string)ProjectorKey.GetValue("Memo", ShareMem.memo);

          SoftKey.Close();
          STCChurchKey.Close();
          ProjectorKey.Close();

          return strMemo;
      }

      //設定註解(左投射)
      public void SetMemo(string strMemo)
      {
          SoftKey = Registry.LocalMachine.CreateSubKey("Software");
          STCChurchKey = SoftKey.CreateSubKey("STCChurch");
          ProjectorKey = STCChurchKey.CreateSubKey("Projector");

          ProjectorKey.SetValue("Memo", strMemo, RegistryValueKind.String);

          SoftKey.Close();
          STCChurchKey.Close();
          ProjectorKey.Close();
      }

/* */
      //取得聽打起始座標-X軸(右投射)
      public int GetCurrentX1()
      {
          int iCurrentX1;

          SoftKey = Registry.LocalMachine.CreateSubKey("Software");
          STCChurchKey = SoftKey.CreateSubKey("STCChurch");
          ProjectorKey = STCChurchKey.CreateSubKey("Projector");

          iCurrentX1 = (int)ProjectorKey.GetValue("CurrentX1", ShareMem.currentX1);

          SoftKey.Close();
          STCChurchKey.Close();
          ProjectorKey.Close();

          return iCurrentX1;
      }

      //設定聽打起始座標-X軸(右投射)
      public void SetCurrentX1(int iCurrentX1)
      {
          SoftKey = Registry.LocalMachine.CreateSubKey("Software");
          STCChurchKey = SoftKey.CreateSubKey("STCChurch");
          ProjectorKey = STCChurchKey.CreateSubKey("Projector");

          ProjectorKey.SetValue("CurrentX1", iCurrentX1, RegistryValueKind.DWord);

          SoftKey.Close();
          STCChurchKey.Close();
          ProjectorKey.Close();
      }

      //取得聽打起始座標-Y軸(右投射)
      public int GetCurrentY1()
      {
          int iCurrentY1;

          SoftKey = Registry.LocalMachine.CreateSubKey("Software");
          STCChurchKey = SoftKey.CreateSubKey("STCChurch");
          ProjectorKey = STCChurchKey.CreateSubKey("Projector");

          iCurrentY1 = (int)ProjectorKey.GetValue("CurrentY1", ShareMem.currentY1);

          SoftKey.Close();
          STCChurchKey.Close();
          ProjectorKey.Close();

          return iCurrentY1;
      }

      //設定聽打起始座標-Y軸(右投射)
      public void SetCurrentY1(int iCurrentY1)
      {
          SoftKey = Registry.LocalMachine.CreateSubKey("Software");
          STCChurchKey = SoftKey.CreateSubKey("STCChurch");
          ProjectorKey = STCChurchKey.CreateSubKey("Projector");

          ProjectorKey.SetValue("CurrentY1", iCurrentY1, RegistryValueKind.DWord);

          SoftKey.Close();
          STCChurchKey.Close();
          ProjectorKey.Close();
      }

      //取得聽打可視大小-寬度(右投射)
      public int GetContentWidth1()
      {
          int iContentWidth1;

          SoftKey = Registry.LocalMachine.CreateSubKey("Software");
          STCChurchKey = SoftKey.CreateSubKey("STCChurch");
          ProjectorKey = STCChurchKey.CreateSubKey("Projector");

          iContentWidth1 = (int)ProjectorKey.GetValue("ContentWidth1", ShareMem.contentWidth1);

          SoftKey.Close();
          STCChurchKey.Close();
          ProjectorKey.Close();

          return iContentWidth1;
      }

      //設定聽打可視大小-寬度(右投射)
      public void SetContentWidth1(int iContentWidth1)
      {
          SoftKey = Registry.LocalMachine.CreateSubKey("Software");
          STCChurchKey = SoftKey.CreateSubKey("STCChurch");
          ProjectorKey = STCChurchKey.CreateSubKey("Projector");

          ProjectorKey.SetValue("ContentWidth1", iContentWidth1, RegistryValueKind.DWord);

          SoftKey.Close();
          STCChurchKey.Close();
          ProjectorKey.Close();
      }

      //取得聽打可視大小-高度(右投射)
      public int GetContentHeight1()
      {
          int iContentHeight1;

          SoftKey = Registry.LocalMachine.CreateSubKey("Software");
          STCChurchKey = SoftKey.CreateSubKey("STCChurch");
          ProjectorKey = STCChurchKey.CreateSubKey("Projector");

          iContentHeight1 = (int)ProjectorKey.GetValue("ContentHeight1", ShareMem.contentHeight1);

          SoftKey.Close();
          STCChurchKey.Close();
          ProjectorKey.Close();

          return iContentHeight1;
      }

      //設定聽打可視大小-高度(右投射)
      public void SetContentHeight1(int iContentHeight1)
      {
          SoftKey = Registry.LocalMachine.CreateSubKey("Software");
          STCChurchKey = SoftKey.CreateSubKey("STCChurch");
          ProjectorKey = STCChurchKey.CreateSubKey("Projector");

          ProjectorKey.SetValue("ContentHeight1", iContentHeight1, RegistryValueKind.DWord);

          SoftKey.Close();
          STCChurchKey.Close();
          ProjectorKey.Close();
      }

      //取得講題起始座標-X軸(右投射)
      public int GetSubjectX1()
      {
          int iSubjectX1;

          SoftKey = Registry.LocalMachine.CreateSubKey("Software");
          STCChurchKey = SoftKey.CreateSubKey("STCChurch");
          ProjectorKey = STCChurchKey.CreateSubKey("Projector");

          iSubjectX1 = (int)ProjectorKey.GetValue("SubjectX1", ShareMem.subjectX1);

          SoftKey.Close();
          STCChurchKey.Close();
          ProjectorKey.Close();

          return iSubjectX1;
      }

      //設定講題起始座標-X軸(右投射)
      public void SetSubjectX1(int iSubjectX1)
      {
          SoftKey = Registry.LocalMachine.CreateSubKey("Software");
          STCChurchKey = SoftKey.CreateSubKey("STCChurch");
          ProjectorKey = STCChurchKey.CreateSubKey("Projector");

          ProjectorKey.SetValue("SubjectX1", iSubjectX1, RegistryValueKind.DWord);

          SoftKey.Close();
          STCChurchKey.Close();
          ProjectorKey.Close();
      }

      //取得講題起始座標-Y軸(右投射)
      public int GetSubjectY1()
      {
          int iSubjectY1;

          SoftKey = Registry.LocalMachine.CreateSubKey("Software");
          STCChurchKey = SoftKey.CreateSubKey("STCChurch");
          ProjectorKey = STCChurchKey.CreateSubKey("Projector");

          iSubjectY1 = (int)ProjectorKey.GetValue("SubjectY1", ShareMem.subjectY1);

          SoftKey.Close();
          STCChurchKey.Close();
          ProjectorKey.Close();

          return iSubjectY1;
      }

      //設定講題起始座標-Y軸(右投射)
      public void SetSubjectY1(int iSubjectY1)
      {
          SoftKey = Registry.LocalMachine.CreateSubKey("Software");
          STCChurchKey = SoftKey.CreateSubKey("STCChurch");
          ProjectorKey = STCChurchKey.CreateSubKey("Projector");

          ProjectorKey.SetValue("SubjectY1", iSubjectY1, RegistryValueKind.DWord);

          SoftKey.Close();
          STCChurchKey.Close();
          ProjectorKey.Close();
      }

      //取得講題可視大小-寬度(右投射)
      public int GetSubjectWidth1()
      {
          int iSubjectWidth1;

          SoftKey = Registry.LocalMachine.CreateSubKey("Software");
          STCChurchKey = SoftKey.CreateSubKey("STCChurch");
          ProjectorKey = STCChurchKey.CreateSubKey("Projector");

          iSubjectWidth1 = (int)ProjectorKey.GetValue("SubjectWidth1", ShareMem.subjectWidth1);

          SoftKey.Close();
          STCChurchKey.Close();
          ProjectorKey.Close();

          return iSubjectWidth1;
      }

      //設定講題可視大小-寬度(右投射)
      public void SetSubjectWidth1(int iSubjectWidth1)
      {
          SoftKey = Registry.LocalMachine.CreateSubKey("Software");
          STCChurchKey = SoftKey.CreateSubKey("STCChurch");
          ProjectorKey = STCChurchKey.CreateSubKey("Projector");

          ProjectorKey.SetValue("SubjectWidth1", iSubjectWidth1, RegistryValueKind.DWord);

          SoftKey.Close();
          STCChurchKey.Close();
          ProjectorKey.Close();
      }

      //取得講題可視大小-高度(右投射)
      public int GetSubjectHeight1()
      {
          int iSubjectHeight1;

          SoftKey = Registry.LocalMachine.CreateSubKey("Software");
          STCChurchKey = SoftKey.CreateSubKey("STCChurch");
          ProjectorKey = STCChurchKey.CreateSubKey("Projector");

          iSubjectHeight1 = (int)ProjectorKey.GetValue("SubjectHeight1", ShareMem.subjectHeight1);

          SoftKey.Close();
          STCChurchKey.Close();
          ProjectorKey.Close();

          return iSubjectHeight1;
      }

      //設定講題可視大小-高度(右投射)
      public void SetSubjectHeight1(int iSubjectHeight1)
      {
          SoftKey = Registry.LocalMachine.CreateSubKey("Software");
          STCChurchKey = SoftKey.CreateSubKey("STCChurch");
          ProjectorKey = STCChurchKey.CreateSubKey("Projector");

          ProjectorKey.SetValue("SubjectHeight1", iSubjectHeight1, RegistryValueKind.DWord);

          SoftKey.Close();
          STCChurchKey.Close();
          ProjectorKey.Close();
      }

      //取得聽打可視行數(右投射)
      public int GetContentLines1()
      {
          int iContentLines1;

          SoftKey = Registry.LocalMachine.CreateSubKey("Software");
          STCChurchKey = SoftKey.CreateSubKey("STCChurch");
          ProjectorKey = STCChurchKey.CreateSubKey("Projector");

          iContentLines1 = (int)ProjectorKey.GetValue("ContentLines1", ShareMem.contentLines1);

          SoftKey.Close();
          STCChurchKey.Close();
          ProjectorKey.Close();

          return iContentLines1;
      }

      //設定聽打可視行數(右投射)
      public void SetContentLines1(int iContentLines1)
      {
          SoftKey = Registry.LocalMachine.CreateSubKey("Software");
          STCChurchKey = SoftKey.CreateSubKey("STCChurch");
          ProjectorKey = STCChurchKey.CreateSubKey("Projector");

          ProjectorKey.SetValue("ContentLines1", iContentLines1, RegistryValueKind.DWord);

          SoftKey.Close();
          STCChurchKey.Close();
          ProjectorKey.Close();
      }

      //取得聽打每行Max字數(右投射)
      public int GetContentLength1()
      {
          int iContentLength1;

          SoftKey = Registry.LocalMachine.CreateSubKey("Software");
          STCChurchKey = SoftKey.CreateSubKey("STCChurch");
          ProjectorKey = STCChurchKey.CreateSubKey("Projector");

          iContentLength1 = (int)ProjectorKey.GetValue("ContentLength1", ShareMem.contentLength1);

          SoftKey.Close();
          STCChurchKey.Close();
          ProjectorKey.Close();

          return iContentLength1;
      }

      //設定聽打每行Max字數(右投射)
      public void SetContentLength1(int iContentLength1)
      {
          SoftKey = Registry.LocalMachine.CreateSubKey("Software");
          STCChurchKey = SoftKey.CreateSubKey("STCChurch");
          ProjectorKey = STCChurchKey.CreateSubKey("Projector");

          ProjectorKey.SetValue("ContentLength1", iContentLength1, RegistryValueKind.DWord);

          SoftKey.Close();
          STCChurchKey.Close();
          ProjectorKey.Close();
      }

      //取得註解(右投射)
      public string GetMemo1()
      {
          string strMemo1;

          SoftKey = Registry.LocalMachine.CreateSubKey("Software");
          STCChurchKey = SoftKey.CreateSubKey("STCChurch");
          ProjectorKey = STCChurchKey.CreateSubKey("Projector");

          strMemo1 = (string)ProjectorKey.GetValue("Memo1", ShareMem.memo1);

          SoftKey.Close();
          STCChurchKey.Close();
          ProjectorKey.Close();

          return strMemo1;
      }

      //設定註解(右投射)
      public void SetMemo1(string strMemo1)
      {
          SoftKey = Registry.LocalMachine.CreateSubKey("Software");
          STCChurchKey = SoftKey.CreateSubKey("STCChurch");
          ProjectorKey = STCChurchKey.CreateSubKey("Projector");

          ProjectorKey.SetValue("Memo1", strMemo1, RegistryValueKind.String);

          SoftKey.Close();
          STCChurchKey.Close();
          ProjectorKey.Close();
      }

/* */
      public string GetFontNameOfSubject()
      {
         string strName;

         SoftKey = Registry.LocalMachine.CreateSubKey("Software");
         STCChurchKey = SoftKey.CreateSubKey("STCChurch");
         ProjectorKey = STCChurchKey.CreateSubKey("Projector");

         strName = (string)ProjectorKey.GetValue("FontNameOfSubject", "標楷體");

         SoftKey.Close();
         STCChurchKey.Close();
         ProjectorKey.Close();

         return strName;
      }

      public void SetFontNameOfSubject(string strName)
      {
         SoftKey = Registry.LocalMachine.CreateSubKey("Software");
         STCChurchKey = SoftKey.CreateSubKey("STCChurch");
         ProjectorKey = STCChurchKey.CreateSubKey("Projector");

         ProjectorKey.SetValue("FontNameOfSubject", strName, RegistryValueKind.String);

         SoftKey.Close();
         STCChurchKey.Close();
         ProjectorKey.Close();
      }

      public float GetFontSizeOfSubject()
      {
         float fSize;

         SoftKey = Registry.LocalMachine.CreateSubKey("Software");
         STCChurchKey = SoftKey.CreateSubKey("STCChurch");
         ProjectorKey = STCChurchKey.CreateSubKey("Projector");

         fSize = float.Parse((string)ProjectorKey.GetValue("FontSizeOfSubject", "20.00"));

         SoftKey.Close();
         STCChurchKey.Close();
         ProjectorKey.Close();

         return fSize;
      }

      public void SetFontSizeOfSubject(float fSize)
      {
         SoftKey = Registry.LocalMachine.CreateSubKey("Software");
         STCChurchKey = SoftKey.CreateSubKey("STCChurch");
         ProjectorKey = STCChurchKey.CreateSubKey("Projector");

         ProjectorKey.SetValue("FontSizeOfSubject", fSize.ToString(), RegistryValueKind.String);

         SoftKey.Close();
         STCChurchKey.Close();
         ProjectorKey.Close();
      }

      public int GetFontStyleOfSubject()
      {
         int iStyle;

         SoftKey = Registry.LocalMachine.CreateSubKey("Software");
         STCChurchKey = SoftKey.CreateSubKey("STCChurch");
         ProjectorKey = STCChurchKey.CreateSubKey("Projector");

         iStyle = (int)ProjectorKey.GetValue("FontStyleOfSubject", 0);

         SoftKey.Close();
         STCChurchKey.Close();
         ProjectorKey.Close();

         return iStyle;
      }

      public void SetFontStyleOfSubject(int iStyle)
      {
         SoftKey = Registry.LocalMachine.CreateSubKey("Software");
         STCChurchKey = SoftKey.CreateSubKey("STCChurch");
         ProjectorKey = STCChurchKey.CreateSubKey("Projector");

         ProjectorKey.SetValue("FontStyleOfSubject", iStyle, RegistryValueKind.DWord);

         SoftKey.Close();
         STCChurchKey.Close();
         ProjectorKey.Close();
      }

      //public string GetColorOfSubject()
      public int GetColorOfSubject()
      {
         //string strColor;
         int iColor;

         SoftKey = Registry.LocalMachine.CreateSubKey("Software");
         STCChurchKey = SoftKey.CreateSubKey("STCChurch");
         ProjectorKey = STCChurchKey.CreateSubKey("Projector");

         //strColor = (string)ProjectorKey.GetValue("ColorOfSubject", "ControlText");
         iColor = (int)ProjectorKey.GetValue("ColorOfSubject", 0xff000000);
         //iColor = (int)ProjectorKey.GetValue("ColorOfSubject", 0);
         

         SoftKey.Close();
         STCChurchKey.Close();
         ProjectorKey.Close();

         return iColor;
      }

      //public void SetColorOfSubject(string strColor)
      public void SetColorOfSubject(int color)
      {
         SoftKey = Registry.LocalMachine.CreateSubKey("Software");
         STCChurchKey = SoftKey.CreateSubKey("STCChurch");
         ProjectorKey = STCChurchKey.CreateSubKey("Projector");

         //ProjectorKey.SetValue("ColorOfSubject", strColor, RegistryValueKind.String);
         ProjectorKey.SetValue("ColorOfSubject", color, RegistryValueKind.DWord);

         SoftKey.Close();
         STCChurchKey.Close();
         ProjectorKey.Close();
      }

      //public string GetColorOfTitle()
      public int GetColorOfTitle()
      {
         //string strColor;
         int color;

         SoftKey = Registry.LocalMachine.CreateSubKey("Software");
         STCChurchKey = SoftKey.CreateSubKey("STCChurch");
         ProjectorKey = STCChurchKey.CreateSubKey("Projector");

         //strColor = (string)ProjectorKey.GetValue("ColorOfTitle", "ControlText");
         color = (int)ProjectorKey.GetValue("ColorOfTitle", 0xff000000);
         //color = (int)ProjectorKey.GetValue("ColorOfTitle", 0);

         SoftKey.Close();
         STCChurchKey.Close();
         ProjectorKey.Close();

         return color;
      }

      //public void SetColorOfTitle(string strColor)
      public void SetColorOfTitle(int color)
      {
         SoftKey = Registry.LocalMachine.CreateSubKey("Software");
         STCChurchKey = SoftKey.CreateSubKey("STCChurch");
         ProjectorKey = STCChurchKey.CreateSubKey("Projector");

         //ProjectorKey.SetValue("ColorOfTitle", color, RegistryValueKind.String);
         ProjectorKey.SetValue("ColorOfTitle", color, RegistryValueKind.DWord);

         SoftKey.Close();
         STCChurchKey.Close();
         ProjectorKey.Close();
      }

      public string GetFontNameOfOthers()
      {
         string strName;

         SoftKey = Registry.LocalMachine.CreateSubKey("Software");
         STCChurchKey = SoftKey.CreateSubKey("STCChurch");
         ProjectorKey = STCChurchKey.CreateSubKey("Projector");

         strName = (string)ProjectorKey.GetValue("FontNameOfOther", "標楷體");

         SoftKey.Close();
         STCChurchKey.Close();
         ProjectorKey.Close();

         return strName;
      }

      public void SetFontNameOfOthers(string strName)
      {
         SoftKey = Registry.LocalMachine.CreateSubKey("Software");
         STCChurchKey = SoftKey.CreateSubKey("STCChurch");
         ProjectorKey = STCChurchKey.CreateSubKey("Projector");

         ProjectorKey.SetValue("FontNameOfOther", strName, RegistryValueKind.String);

         SoftKey.Close();
         STCChurchKey.Close();
         ProjectorKey.Close();
      }

      public float GetFontSizeOfOthers()
      {
         float fSize;

         SoftKey = Registry.LocalMachine.CreateSubKey("Software");
         STCChurchKey = SoftKey.CreateSubKey("STCChurch");
         ProjectorKey = STCChurchKey.CreateSubKey("Projector");

         fSize = float.Parse((string)ProjectorKey.GetValue("FontSizeOfOther", "14.00"));

         SoftKey.Close();
         STCChurchKey.Close();
         ProjectorKey.Close();

         return fSize;
      }

      public void SetFontSizeOfOthers(float fSize)
      {
         SoftKey = Registry.LocalMachine.CreateSubKey("Software");
         STCChurchKey = SoftKey.CreateSubKey("STCChurch");
         ProjectorKey = STCChurchKey.CreateSubKey("Projector");

         ProjectorKey.SetValue("FontSizeOfOther", fSize.ToString(), RegistryValueKind.String);

         SoftKey.Close();
         STCChurchKey.Close();
         ProjectorKey.Close();
      }

      public int GetFontStyleOfOthers()
      {
         int iStyle;

         SoftKey = Registry.LocalMachine.CreateSubKey("Software");
         STCChurchKey = SoftKey.CreateSubKey("STCChurch");
         ProjectorKey = STCChurchKey.CreateSubKey("Projector");

         iStyle = (int)ProjectorKey.GetValue("FontStyleOfOther", 0);

         SoftKey.Close();
         STCChurchKey.Close();
         ProjectorKey.Close();

         return iStyle;
      }

      public void SetFontStyleOfOthers(int iStyle)
      {
         SoftKey = Registry.LocalMachine.CreateSubKey("Software");
         STCChurchKey = SoftKey.CreateSubKey("STCChurch");
         ProjectorKey = STCChurchKey.CreateSubKey("Projector");

         ProjectorKey.SetValue("FontStyleOfOther", iStyle, RegistryValueKind.DWord);

         SoftKey.Close();
         STCChurchKey.Close();
         ProjectorKey.Close();
      }

      //public string GetColorOfOthers()
      public int GetColorOfOthers()
      {
         //string strColor;
         int color;

         SoftKey = Registry.LocalMachine.CreateSubKey("Software");
         STCChurchKey = SoftKey.CreateSubKey("STCChurch");
         ProjectorKey = STCChurchKey.CreateSubKey("Projector");

         //strColor = (string)ProjectorKey.GetValue("ColorOfOther", "ControlText");
         color = (int)ProjectorKey.GetValue("ColorOfOther", 0xff000000);//default is black
         //color = (int)ProjectorKey.GetValue("ColorOfOther", 0);//default is black

         SoftKey.Close();
         STCChurchKey.Close();
         ProjectorKey.Close();

         return color;
      }

      //public void SetColorOfOthers(string strColor)
      public void SetColorOfOthers(int color)
      {
         SoftKey = Registry.LocalMachine.CreateSubKey("Software");
         STCChurchKey = SoftKey.CreateSubKey("STCChurch");
         ProjectorKey = STCChurchKey.CreateSubKey("Projector");

         //ProjectorKey.SetValue("ColorOfOther", color, RegistryValueKind.String);
         ProjectorKey.SetValue("ColorOfOther", color, RegistryValueKind.DWord);

         SoftKey.Close();
         STCChurchKey.Close();
         ProjectorKey.Close();
      }

      public string GetPathOfBackgroundImage()
      {
         string strPath;

         SoftKey = Registry.LocalMachine.CreateSubKey("Software");
         STCChurchKey = SoftKey.CreateSubKey("STCChurch");
         ProjectorKey = STCChurchKey.CreateSubKey("Projector");

         strPath = (string)ProjectorKey.GetValue("PathOfBackgroundImager", ".\\Resources\\Background-01.bmp");

         SoftKey.Close();
         STCChurchKey.Close();
         ProjectorKey.Close();

         return strPath;
      }

      public void SetPathOfBackgroundImage(string strPath)
      {
         SoftKey = Registry.LocalMachine.CreateSubKey("Software");
         STCChurchKey = SoftKey.CreateSubKey("STCChurch");
         ProjectorKey = STCChurchKey.CreateSubKey("Projector");

         ProjectorKey.SetValue("PathOfBackgroundImager", strPath, RegistryValueKind.String);

         SoftKey.Close();
         STCChurchKey.Close();
         ProjectorKey.Close();
      }

      //多節經文
      public bool GetCheckScripture()
      {
         int iCheck;

         SoftKey = Registry.LocalMachine.CreateSubKey("Software");
         STCChurchKey = SoftKey.CreateSubKey("STCChurch");
         ProjectorKey = STCChurchKey.CreateSubKey("Projector");

         iCheck = (int)ProjectorKey.GetValue("CheckScripture", 0);

         SoftKey.Close();
         STCChurchKey.Close();
         ProjectorKey.Close();

         return (iCheck > 0)?true:false;
      }

      public void SetCheckScripture(bool bCheck)
      {
         SoftKey = Registry.LocalMachine.CreateSubKey("Software");
         STCChurchKey = SoftKey.CreateSubKey("STCChurch");
         ProjectorKey = STCChurchKey.CreateSubKey("Projector");

         ProjectorKey.SetValue("CheckScripture", (bCheck) ? 1 : 0, RegistryValueKind.DWord);

         SoftKey.Close();
         STCChurchKey.Close();
         ProjectorKey.Close();
      }


      //複製到聽打
      public bool GetCheckToTextBox1()
      {
          int iCheck;

          SoftKey = Registry.LocalMachine.CreateSubKey("Software");
          STCChurchKey = SoftKey.CreateSubKey("STCChurch");
          ProjectorKey = STCChurchKey.CreateSubKey("Projector");

          iCheck = (int)ProjectorKey.GetValue("CheckToTextBox1", 1);

          SoftKey.Close();
          STCChurchKey.Close();
          ProjectorKey.Close();

          return (iCheck > 0) ? true : false;
      }

      public void SetCheckToTextBox1(bool bCheck)
      {
          SoftKey = Registry.LocalMachine.CreateSubKey("Software");
          STCChurchKey = SoftKey.CreateSubKey("STCChurch");
          ProjectorKey = STCChurchKey.CreateSubKey("Projector");

          ProjectorKey.SetValue("CheckToTextBox1", (bCheck) ? 1 : 0, RegistryValueKind.DWord);

          SoftKey.Close();
          STCChurchKey.Close();
          ProjectorKey.Close();
      }

      //存檔錄徑
      public string GetFilePath()
      {
          string strName;

          SoftKey = Registry.LocalMachine.CreateSubKey("Software");
          STCChurchKey = SoftKey.CreateSubKey("STCChurch");
          ProjectorKey = STCChurchKey.CreateSubKey("Projector");

          strName = (string)ProjectorKey.GetValue("FilePath", "C:\\shipai");

          SoftKey.Close();
          STCChurchKey.Close();
          ProjectorKey.Close();

          return strName;
      }

      public void SetFilePath(string strName)
      {
          SoftKey = Registry.LocalMachine.CreateSubKey("Software");
          STCChurchKey = SoftKey.CreateSubKey("STCChurch");
          ProjectorKey = STCChurchKey.CreateSubKey("Projector");

          ProjectorKey.SetValue("FilePath", strName, RegistryValueKind.String);

          SoftKey.Close();
          STCChurchKey.Close();
          ProjectorKey.Close();
      }
   }
}
