﻿namespace Projector
{
   partial class ResolutionDlg
   {
      /// <summary>
      /// 設計工具所需的變數。
      /// </summary>
      private System.ComponentModel.IContainer components = null;

      /// <summary>
      /// 清除任何使用中的資源。
      /// </summary>
      /// <param name="disposing">如果應該公開 Managed 資源則為 true，否則為 false。</param>
      protected override void Dispose(bool disposing)
      {
         if (disposing && (components != null))
         {
            components.Dispose();
         }
         base.Dispose(disposing);
      }

      #region Windows Form 設計工具產生的程式碼

      /// <summary>
      /// 此為設計工具支援所需的方法 - 請勿使用程式碼編輯器修改這個方法的內容。
      ///
      /// </summary>
      private void InitializeComponent()
      {
          this.btnCancel = new System.Windows.Forms.Button();
          this.btnApply = new System.Windows.Forms.Button();
          this.label1 = new System.Windows.Forms.Label();
          this.trackBarHost = new System.Windows.Forms.TrackBar();
          this.label2 = new System.Windows.Forms.Label();
          this.label3 = new System.Windows.Forms.Label();
          this.label4 = new System.Windows.Forms.Label();
          this.btnOK = new System.Windows.Forms.Button();
          this.trackBarClient = new System.Windows.Forms.TrackBar();
          this.label5 = new System.Windows.Forms.Label();
          this.label6 = new System.Windows.Forms.Label();
          this.label7 = new System.Windows.Forms.Label();
          this.label8 = new System.Windows.Forms.Label();
          this.pictureBox2 = new System.Windows.Forms.PictureBox();
          this.pictureBox1 = new System.Windows.Forms.PictureBox();
          this.groupBox1 = new System.Windows.Forms.GroupBox();
          this.txtBoxMemo = new System.Windows.Forms.TextBox();
          this.label30 = new System.Windows.Forms.Label();
          this.btnLeftRest = new System.Windows.Forms.Button();
          this.groupBox3 = new System.Windows.Forms.GroupBox();
          this.txtBoxSubjectHeight = new System.Windows.Forms.NumericUpDown();
          this.txtBoxSubjectWidth = new System.Windows.Forms.NumericUpDown();
          this.txtBoxSubjectY = new System.Windows.Forms.NumericUpDown();
          this.txtBoxSubjectX = new System.Windows.Forms.NumericUpDown();
          this.label19 = new System.Windows.Forms.Label();
          this.label18 = new System.Windows.Forms.Label();
          this.label17 = new System.Windows.Forms.Label();
          this.label16 = new System.Windows.Forms.Label();
          this.chkBoxLeftDisplay = new System.Windows.Forms.CheckBox();
          this.groupBox2 = new System.Windows.Forms.GroupBox();
          this.txtBoxContentLength = new System.Windows.Forms.NumericUpDown();
          this.txtBoxContentLines = new System.Windows.Forms.NumericUpDown();
          this.txtBoxContentHeight = new System.Windows.Forms.NumericUpDown();
          this.txtBoxCurrentY = new System.Windows.Forms.NumericUpDown();
          this.txtBoxContentWidth = new System.Windows.Forms.NumericUpDown();
          this.txtBoxCurrentX = new System.Windows.Forms.NumericUpDown();
          this.label15 = new System.Windows.Forms.Label();
          this.label14 = new System.Windows.Forms.Label();
          this.label12 = new System.Windows.Forms.Label();
          this.label11 = new System.Windows.Forms.Label();
          this.label10 = new System.Windows.Forms.Label();
          this.label9 = new System.Windows.Forms.Label();
          this.label13 = new System.Windows.Forms.Label();
          this.txtBoxFilePath = new System.Windows.Forms.TextBox();
          this.groupBox4 = new System.Windows.Forms.GroupBox();
          this.txtBoxMemo1 = new System.Windows.Forms.TextBox();
          this.label31 = new System.Windows.Forms.Label();
          this.btnRightRest = new System.Windows.Forms.Button();
          this.groupBox5 = new System.Windows.Forms.GroupBox();
          this.txtBoxSubjectHeight1 = new System.Windows.Forms.NumericUpDown();
          this.txtBoxSubjectWidth1 = new System.Windows.Forms.NumericUpDown();
          this.txtBoxSubjectY1 = new System.Windows.Forms.NumericUpDown();
          this.txtBoxSubjectX1 = new System.Windows.Forms.NumericUpDown();
          this.label20 = new System.Windows.Forms.Label();
          this.label21 = new System.Windows.Forms.Label();
          this.label22 = new System.Windows.Forms.Label();
          this.label23 = new System.Windows.Forms.Label();
          this.chkBoxRightDisplay = new System.Windows.Forms.CheckBox();
          this.groupBox6 = new System.Windows.Forms.GroupBox();
          this.txtBoxContentLength1 = new System.Windows.Forms.NumericUpDown();
          this.txtBoxContentLines1 = new System.Windows.Forms.NumericUpDown();
          this.txtBoxContentHeight1 = new System.Windows.Forms.NumericUpDown();
          this.txtBoxCurrentY1 = new System.Windows.Forms.NumericUpDown();
          this.txtBoxContentWidth1 = new System.Windows.Forms.NumericUpDown();
          this.txtBoxCurrentX1 = new System.Windows.Forms.NumericUpDown();
          this.label24 = new System.Windows.Forms.Label();
          this.label25 = new System.Windows.Forms.Label();
          this.label26 = new System.Windows.Forms.Label();
          this.label27 = new System.Windows.Forms.Label();
          this.label28 = new System.Windows.Forms.Label();
          this.label29 = new System.Windows.Forms.Label();
          this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
          this.btnSelectDir = new System.Windows.Forms.Button();
          ((System.ComponentModel.ISupportInitialize)(this.trackBarHost)).BeginInit();
          ((System.ComponentModel.ISupportInitialize)(this.trackBarClient)).BeginInit();
          ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
          ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
          this.groupBox1.SuspendLayout();
          this.groupBox3.SuspendLayout();
          ((System.ComponentModel.ISupportInitialize)(this.txtBoxSubjectHeight)).BeginInit();
          ((System.ComponentModel.ISupportInitialize)(this.txtBoxSubjectWidth)).BeginInit();
          ((System.ComponentModel.ISupportInitialize)(this.txtBoxSubjectY)).BeginInit();
          ((System.ComponentModel.ISupportInitialize)(this.txtBoxSubjectX)).BeginInit();
          this.groupBox2.SuspendLayout();
          ((System.ComponentModel.ISupportInitialize)(this.txtBoxContentLength)).BeginInit();
          ((System.ComponentModel.ISupportInitialize)(this.txtBoxContentLines)).BeginInit();
          ((System.ComponentModel.ISupportInitialize)(this.txtBoxContentHeight)).BeginInit();
          ((System.ComponentModel.ISupportInitialize)(this.txtBoxCurrentY)).BeginInit();
          ((System.ComponentModel.ISupportInitialize)(this.txtBoxContentWidth)).BeginInit();
          ((System.ComponentModel.ISupportInitialize)(this.txtBoxCurrentX)).BeginInit();
          this.groupBox4.SuspendLayout();
          this.groupBox5.SuspendLayout();
          ((System.ComponentModel.ISupportInitialize)(this.txtBoxSubjectHeight1)).BeginInit();
          ((System.ComponentModel.ISupportInitialize)(this.txtBoxSubjectWidth1)).BeginInit();
          ((System.ComponentModel.ISupportInitialize)(this.txtBoxSubjectY1)).BeginInit();
          ((System.ComponentModel.ISupportInitialize)(this.txtBoxSubjectX1)).BeginInit();
          this.groupBox6.SuspendLayout();
          ((System.ComponentModel.ISupportInitialize)(this.txtBoxContentLength1)).BeginInit();
          ((System.ComponentModel.ISupportInitialize)(this.txtBoxContentLines1)).BeginInit();
          ((System.ComponentModel.ISupportInitialize)(this.txtBoxContentHeight1)).BeginInit();
          ((System.ComponentModel.ISupportInitialize)(this.txtBoxCurrentY1)).BeginInit();
          ((System.ComponentModel.ISupportInitialize)(this.txtBoxContentWidth1)).BeginInit();
          ((System.ComponentModel.ISupportInitialize)(this.txtBoxCurrentX1)).BeginInit();
          this.SuspendLayout();
          // 
          // btnCancel
          // 
          this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
          this.btnCancel.Font = new System.Drawing.Font("新細明體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
          this.btnCancel.Location = new System.Drawing.Point(299, 569);
          this.btnCancel.Name = "btnCancel";
          this.btnCancel.Size = new System.Drawing.Size(74, 40);
          this.btnCancel.TabIndex = 1;
          this.btnCancel.Text = "取 消";
          this.btnCancel.UseVisualStyleBackColor = true;
          // 
          // btnApply
          // 
          this.btnApply.Enabled = false;
          this.btnApply.Font = new System.Drawing.Font("新細明體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
          this.btnApply.Location = new System.Drawing.Point(379, 569);
          this.btnApply.Name = "btnApply";
          this.btnApply.Size = new System.Drawing.Size(74, 40);
          this.btnApply.TabIndex = 2;
          this.btnApply.Text = "套 用";
          this.btnApply.UseVisualStyleBackColor = true;
          this.btnApply.Click += new System.EventHandler(this.btnApply_Click);
          // 
          // label1
          // 
          this.label1.AutoSize = true;
          this.label1.Location = new System.Drawing.Point(66, 269);
          this.label1.Name = "label1";
          this.label1.Size = new System.Drawing.Size(63, 14);
          this.label1.TabIndex = 4;
          this.label1.Text = "主控螢幕";
          // 
          // trackBarHost
          // 
          this.trackBarHost.AutoSize = false;
          this.trackBarHost.Enabled = false;
          this.trackBarHost.Location = new System.Drawing.Point(60, 470);
          this.trackBarHost.Maximum = 1;
          this.trackBarHost.Name = "trackBarHost";
          this.trackBarHost.Size = new System.Drawing.Size(91, 46);
          this.trackBarHost.TabIndex = 3;
          this.trackBarHost.Value = 1;
          this.trackBarHost.Scroll += new System.EventHandler(this.trackBar1_Scroll);
          // 
          // label2
          // 
          this.label2.AutoSize = true;
          this.label2.Enabled = false;
          this.label2.Location = new System.Drawing.Point(12, 479);
          this.label2.Name = "label2";
          this.label2.Size = new System.Drawing.Size(35, 14);
          this.label2.TabIndex = 5;
          this.label2.Text = "較少";
          // 
          // label3
          // 
          this.label3.AutoSize = true;
          this.label3.Enabled = false;
          this.label3.Location = new System.Drawing.Point(157, 479);
          this.label3.Name = "label3";
          this.label3.Size = new System.Drawing.Size(35, 14);
          this.label3.TabIndex = 6;
          this.label3.Text = "較多";
          // 
          // label4
          // 
          this.label4.Location = new System.Drawing.Point(12, 519);
          this.label4.Name = "label4";
          this.label4.Size = new System.Drawing.Size(187, 19);
          this.label4.TabIndex = 7;
          this.label4.Text = "800 x 600 個像數";
          this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
          // 
          // btnOK
          // 
          this.btnOK.DialogResult = System.Windows.Forms.DialogResult.OK;
          this.btnOK.Font = new System.Drawing.Font("新細明體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
          this.btnOK.Location = new System.Drawing.Point(219, 569);
          this.btnOK.Name = "btnOK";
          this.btnOK.Size = new System.Drawing.Size(74, 40);
          this.btnOK.TabIndex = 0;
          this.btnOK.Text = "確 定";
          this.btnOK.UseVisualStyleBackColor = true;
          this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
          // 
          // trackBarClient
          // 
          this.trackBarClient.AutoSize = false;
          this.trackBarClient.Location = new System.Drawing.Point(60, 181);
          this.trackBarClient.Maximum = 1;
          this.trackBarClient.Name = "trackBarClient";
          this.trackBarClient.Size = new System.Drawing.Size(91, 46);
          this.trackBarClient.TabIndex = 3;
          this.trackBarClient.Scroll += new System.EventHandler(this.trackBarClient_Scroll);
          // 
          // label5
          // 
          this.label5.AutoSize = true;
          this.label5.Location = new System.Drawing.Point(12, 190);
          this.label5.Name = "label5";
          this.label5.Size = new System.Drawing.Size(35, 14);
          this.label5.TabIndex = 5;
          this.label5.Text = "較少";
          // 
          // label6
          // 
          this.label6.AutoSize = true;
          this.label6.Location = new System.Drawing.Point(157, 190);
          this.label6.Name = "label6";
          this.label6.Size = new System.Drawing.Size(35, 14);
          this.label6.TabIndex = 6;
          this.label6.Text = "較多";
          // 
          // label7
          // 
          this.label7.Location = new System.Drawing.Point(12, 230);
          this.label7.Name = "label7";
          this.label7.Size = new System.Drawing.Size(187, 19);
          this.label7.TabIndex = 7;
          this.label7.Text = "800 x 600 個像數";
          this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
          // 
          // label8
          // 
          this.label8.AutoSize = true;
          this.label8.Location = new System.Drawing.Point(53, 9);
          this.label8.Name = "label8";
          this.label8.Size = new System.Drawing.Size(99, 14);
          this.label8.TabIndex = 4;
          this.label8.Text = "投射布幕(全部)";
          // 
          // pictureBox2
          // 
          this.pictureBox2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
          this.pictureBox2.Image = global::Projector.Properties.Resources.Slide;
          this.pictureBox2.Location = new System.Drawing.Point(12, 31);
          this.pictureBox2.Name = "pictureBox2";
          this.pictureBox2.Size = new System.Drawing.Size(187, 140);
          this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
          this.pictureBox2.TabIndex = 3;
          this.pictureBox2.TabStop = false;
          // 
          // pictureBox1
          // 
          this.pictureBox1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
          this.pictureBox1.Image = global::Projector.Properties.Resources._17LCD;
          this.pictureBox1.Location = new System.Drawing.Point(12, 291);
          this.pictureBox1.Name = "pictureBox1";
          this.pictureBox1.Size = new System.Drawing.Size(187, 173);
          this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
          this.pictureBox1.TabIndex = 3;
          this.pictureBox1.TabStop = false;
          // 
          // groupBox1
          // 
          this.groupBox1.Controls.Add(this.txtBoxMemo);
          this.groupBox1.Controls.Add(this.label30);
          this.groupBox1.Controls.Add(this.btnLeftRest);
          this.groupBox1.Controls.Add(this.groupBox3);
          this.groupBox1.Controls.Add(this.chkBoxLeftDisplay);
          this.groupBox1.Controls.Add(this.groupBox2);
          this.groupBox1.ForeColor = System.Drawing.Color.Blue;
          this.groupBox1.Location = new System.Drawing.Point(255, 9);
          this.groupBox1.Name = "groupBox1";
          this.groupBox1.Size = new System.Drawing.Size(439, 240);
          this.groupBox1.TabIndex = 10;
          this.groupBox1.TabStop = false;
          this.groupBox1.Text = "左投射";
          // 
          // txtBoxMemo
          // 
          this.txtBoxMemo.Location = new System.Drawing.Point(160, 21);
          this.txtBoxMemo.MaxLength = 10;
          this.txtBoxMemo.Name = "txtBoxMemo";
          this.txtBoxMemo.Size = new System.Drawing.Size(100, 24);
          this.txtBoxMemo.TabIndex = 16;
          this.txtBoxMemo.TextChanged += new System.EventHandler(this.change_Click);
          // 
          // label30
          // 
          this.label30.AutoSize = true;
          this.label30.Location = new System.Drawing.Point(122, 23);
          this.label30.Name = "label30";
          this.label30.Size = new System.Drawing.Size(35, 14);
          this.label30.TabIndex = 15;
          this.label30.Text = "註解";
          // 
          // btnLeftRest
          // 
          this.btnLeftRest.ForeColor = System.Drawing.SystemColors.ControlText;
          this.btnLeftRest.Location = new System.Drawing.Point(339, 17);
          this.btnLeftRest.Name = "btnLeftRest";
          this.btnLeftRest.Size = new System.Drawing.Size(65, 28);
          this.btnLeftRest.TabIndex = 8;
          this.btnLeftRest.Text = "重設";
          this.btnLeftRest.UseVisualStyleBackColor = true;
          this.btnLeftRest.Click += new System.EventHandler(this.btnLeftRest_Click);
          // 
          // groupBox3
          // 
          this.groupBox3.Controls.Add(this.txtBoxSubjectHeight);
          this.groupBox3.Controls.Add(this.txtBoxSubjectWidth);
          this.groupBox3.Controls.Add(this.txtBoxSubjectY);
          this.groupBox3.Controls.Add(this.txtBoxSubjectX);
          this.groupBox3.Controls.Add(this.label19);
          this.groupBox3.Controls.Add(this.label18);
          this.groupBox3.Controls.Add(this.label17);
          this.groupBox3.Controls.Add(this.label16);
          this.groupBox3.Location = new System.Drawing.Point(15, 48);
          this.groupBox3.Name = "groupBox3";
          this.groupBox3.Size = new System.Drawing.Size(409, 76);
          this.groupBox3.TabIndex = 14;
          this.groupBox3.TabStop = false;
          this.groupBox3.Text = "講題位置調整";
          // 
          // txtBoxSubjectHeight
          // 
          this.txtBoxSubjectHeight.Location = new System.Drawing.Point(323, 46);
          this.txtBoxSubjectHeight.Maximum = new decimal(new int[] {
            768,
            0,
            0,
            0});
          this.txtBoxSubjectHeight.Name = "txtBoxSubjectHeight";
          this.txtBoxSubjectHeight.Size = new System.Drawing.Size(65, 24);
          this.txtBoxSubjectHeight.TabIndex = 11;
          this.txtBoxSubjectHeight.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
          this.txtBoxSubjectHeight.ValueChanged += new System.EventHandler(this.change_Click);
          // 
          // txtBoxSubjectWidth
          // 
          this.txtBoxSubjectWidth.Location = new System.Drawing.Point(109, 46);
          this.txtBoxSubjectWidth.Maximum = new decimal(new int[] {
            1024,
            0,
            0,
            0});
          this.txtBoxSubjectWidth.Name = "txtBoxSubjectWidth";
          this.txtBoxSubjectWidth.Size = new System.Drawing.Size(65, 24);
          this.txtBoxSubjectWidth.TabIndex = 10;
          this.txtBoxSubjectWidth.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
          this.txtBoxSubjectWidth.ValueChanged += new System.EventHandler(this.change_Click);
          // 
          // txtBoxSubjectY
          // 
          this.txtBoxSubjectY.Location = new System.Drawing.Point(323, 23);
          this.txtBoxSubjectY.Maximum = new decimal(new int[] {
            768,
            0,
            0,
            0});
          this.txtBoxSubjectY.Minimum = new decimal(new int[] {
            10,
            0,
            0,
            -2147483648});
          this.txtBoxSubjectY.Name = "txtBoxSubjectY";
          this.txtBoxSubjectY.Size = new System.Drawing.Size(65, 24);
          this.txtBoxSubjectY.TabIndex = 9;
          this.txtBoxSubjectY.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
          this.txtBoxSubjectY.ValueChanged += new System.EventHandler(this.change_Click);
          // 
          // txtBoxSubjectX
          // 
          this.txtBoxSubjectX.Location = new System.Drawing.Point(109, 23);
          this.txtBoxSubjectX.Maximum = new decimal(new int[] {
            1024,
            0,
            0,
            0});
          this.txtBoxSubjectX.Minimum = new decimal(new int[] {
            10,
            0,
            0,
            -2147483648});
          this.txtBoxSubjectX.Name = "txtBoxSubjectX";
          this.txtBoxSubjectX.Size = new System.Drawing.Size(65, 24);
          this.txtBoxSubjectX.TabIndex = 8;
          this.txtBoxSubjectX.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
          this.txtBoxSubjectX.ValueChanged += new System.EventHandler(this.change_Click);
          // 
          // label19
          // 
          this.label19.AutoSize = true;
          this.label19.Location = new System.Drawing.Point(206, 49);
          this.label19.Name = "label19";
          this.label19.Size = new System.Drawing.Size(89, 14);
          this.label19.TabIndex = 5;
          this.label19.Text = "可視大小 [高]";
          // 
          // label18
          // 
          this.label18.AutoSize = true;
          this.label18.Location = new System.Drawing.Point(7, 49);
          this.label18.Name = "label18";
          this.label18.Size = new System.Drawing.Size(89, 14);
          this.label18.TabIndex = 4;
          this.label18.Text = "可視大小 [寬]";
          // 
          // label17
          // 
          this.label17.AutoSize = true;
          this.label17.Location = new System.Drawing.Point(206, 26);
          this.label17.Name = "label17";
          this.label17.Size = new System.Drawing.Size(84, 14);
          this.label17.TabIndex = 1;
          this.label17.Text = "起始座標 [Y]";
          // 
          // label16
          // 
          this.label16.AutoSize = true;
          this.label16.Location = new System.Drawing.Point(6, 26);
          this.label16.Name = "label16";
          this.label16.Size = new System.Drawing.Size(84, 14);
          this.label16.TabIndex = 0;
          this.label16.Text = "起始座標 [X]";
          // 
          // chkBoxLeftDisplay
          // 
          this.chkBoxLeftDisplay.AutoSize = true;
          this.chkBoxLeftDisplay.Location = new System.Drawing.Point(25, 23);
          this.chkBoxLeftDisplay.Name = "chkBoxLeftDisplay";
          this.chkBoxLeftDisplay.Size = new System.Drawing.Size(54, 18);
          this.chkBoxLeftDisplay.TabIndex = 10;
          this.chkBoxLeftDisplay.Text = "啟用";
          this.chkBoxLeftDisplay.UseVisualStyleBackColor = true;
          this.chkBoxLeftDisplay.CheckedChanged += new System.EventHandler(this.chkBoxDisplay_Click);
          // 
          // groupBox2
          // 
          this.groupBox2.Controls.Add(this.txtBoxContentLength);
          this.groupBox2.Controls.Add(this.txtBoxContentLines);
          this.groupBox2.Controls.Add(this.txtBoxContentHeight);
          this.groupBox2.Controls.Add(this.txtBoxCurrentY);
          this.groupBox2.Controls.Add(this.txtBoxContentWidth);
          this.groupBox2.Controls.Add(this.txtBoxCurrentX);
          this.groupBox2.Controls.Add(this.label15);
          this.groupBox2.Controls.Add(this.label14);
          this.groupBox2.Controls.Add(this.label12);
          this.groupBox2.Controls.Add(this.label11);
          this.groupBox2.Controls.Add(this.label10);
          this.groupBox2.Controls.Add(this.label9);
          this.groupBox2.Location = new System.Drawing.Point(15, 130);
          this.groupBox2.Name = "groupBox2";
          this.groupBox2.Size = new System.Drawing.Size(409, 106);
          this.groupBox2.TabIndex = 11;
          this.groupBox2.TabStop = false;
          this.groupBox2.Text = "視訊文字細部調整";
          // 
          // txtBoxContentLength
          // 
          this.txtBoxContentLength.Location = new System.Drawing.Point(323, 73);
          this.txtBoxContentLength.Maximum = new decimal(new int[] {
            20,
            0,
            0,
            0});
          this.txtBoxContentLength.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
          this.txtBoxContentLength.Name = "txtBoxContentLength";
          this.txtBoxContentLength.Size = new System.Drawing.Size(65, 24);
          this.txtBoxContentLength.TabIndex = 19;
          this.txtBoxContentLength.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
          this.txtBoxContentLength.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
          this.txtBoxContentLength.ValueChanged += new System.EventHandler(this.change_Click);
          // 
          // txtBoxContentLines
          // 
          this.txtBoxContentLines.Location = new System.Drawing.Point(109, 73);
          this.txtBoxContentLines.Maximum = new decimal(new int[] {
            20,
            0,
            0,
            0});
          this.txtBoxContentLines.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
          this.txtBoxContentLines.Name = "txtBoxContentLines";
          this.txtBoxContentLines.Size = new System.Drawing.Size(65, 24);
          this.txtBoxContentLines.TabIndex = 18;
          this.txtBoxContentLines.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
          this.txtBoxContentLines.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
          this.txtBoxContentLines.ValueChanged += new System.EventHandler(this.change_Click);
          // 
          // txtBoxContentHeight
          // 
          this.txtBoxContentHeight.Location = new System.Drawing.Point(323, 50);
          this.txtBoxContentHeight.Maximum = new decimal(new int[] {
            768,
            0,
            0,
            0});
          this.txtBoxContentHeight.Name = "txtBoxContentHeight";
          this.txtBoxContentHeight.Size = new System.Drawing.Size(65, 24);
          this.txtBoxContentHeight.TabIndex = 17;
          this.txtBoxContentHeight.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
          this.txtBoxContentHeight.ValueChanged += new System.EventHandler(this.change_Click);
          // 
          // txtBoxCurrentY
          // 
          this.txtBoxCurrentY.Location = new System.Drawing.Point(323, 27);
          this.txtBoxCurrentY.Maximum = new decimal(new int[] {
            768,
            0,
            0,
            0});
          this.txtBoxCurrentY.Minimum = new decimal(new int[] {
            10,
            0,
            0,
            -2147483648});
          this.txtBoxCurrentY.Name = "txtBoxCurrentY";
          this.txtBoxCurrentY.Size = new System.Drawing.Size(65, 24);
          this.txtBoxCurrentY.TabIndex = 16;
          this.txtBoxCurrentY.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
          this.txtBoxCurrentY.ValueChanged += new System.EventHandler(this.change_Click);
          // 
          // txtBoxContentWidth
          // 
          this.txtBoxContentWidth.Location = new System.Drawing.Point(109, 50);
          this.txtBoxContentWidth.Maximum = new decimal(new int[] {
            1024,
            0,
            0,
            0});
          this.txtBoxContentWidth.Name = "txtBoxContentWidth";
          this.txtBoxContentWidth.Size = new System.Drawing.Size(65, 24);
          this.txtBoxContentWidth.TabIndex = 15;
          this.txtBoxContentWidth.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
          this.txtBoxContentWidth.ValueChanged += new System.EventHandler(this.change_Click);
          // 
          // txtBoxCurrentX
          // 
          this.txtBoxCurrentX.Location = new System.Drawing.Point(109, 27);
          this.txtBoxCurrentX.Maximum = new decimal(new int[] {
            1024,
            0,
            0,
            0});
          this.txtBoxCurrentX.Minimum = new decimal(new int[] {
            10,
            0,
            0,
            -2147483648});
          this.txtBoxCurrentX.Name = "txtBoxCurrentX";
          this.txtBoxCurrentX.Size = new System.Drawing.Size(65, 24);
          this.txtBoxCurrentX.TabIndex = 14;
          this.txtBoxCurrentX.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
          this.txtBoxCurrentX.ValueChanged += new System.EventHandler(this.change_Click);
          // 
          // label15
          // 
          this.label15.AutoSize = true;
          this.label15.Location = new System.Drawing.Point(206, 80);
          this.label15.Name = "label15";
          this.label15.Size = new System.Drawing.Size(63, 14);
          this.label15.TabIndex = 12;
          this.label15.Text = "每行字數";
          // 
          // label14
          // 
          this.label14.AutoSize = true;
          this.label14.Location = new System.Drawing.Point(7, 80);
          this.label14.Name = "label14";
          this.label14.Size = new System.Drawing.Size(63, 14);
          this.label14.TabIndex = 5;
          this.label14.Text = "顯示行數";
          // 
          // label12
          // 
          this.label12.AutoSize = true;
          this.label12.Location = new System.Drawing.Point(206, 58);
          this.label12.Name = "label12";
          this.label12.Size = new System.Drawing.Size(89, 14);
          this.label12.TabIndex = 3;
          this.label12.Text = "可視大小 [高]";
          // 
          // label11
          // 
          this.label11.AutoSize = true;
          this.label11.Location = new System.Drawing.Point(7, 58);
          this.label11.Name = "label11";
          this.label11.Size = new System.Drawing.Size(89, 14);
          this.label11.TabIndex = 2;
          this.label11.Text = "可視大小 [寬]";
          // 
          // label10
          // 
          this.label10.AutoSize = true;
          this.label10.Location = new System.Drawing.Point(206, 35);
          this.label10.Name = "label10";
          this.label10.Size = new System.Drawing.Size(84, 14);
          this.label10.TabIndex = 1;
          this.label10.Text = "起始座標 [Y]";
          // 
          // label9
          // 
          this.label9.AutoSize = true;
          this.label9.Location = new System.Drawing.Point(7, 35);
          this.label9.Name = "label9";
          this.label9.Size = new System.Drawing.Size(84, 14);
          this.label9.TabIndex = 0;
          this.label9.Text = "起始座標 [X]";
          // 
          // label13
          // 
          this.label13.AutoSize = true;
          this.label13.Location = new System.Drawing.Point(263, 522);
          this.label13.Name = "label13";
          this.label13.Size = new System.Drawing.Size(63, 14);
          this.label13.TabIndex = 12;
          this.label13.Text = "存檔路徑";
          // 
          // txtBoxFilePath
          // 
          this.txtBoxFilePath.Enabled = false;
          this.txtBoxFilePath.Location = new System.Drawing.Point(332, 519);
          this.txtBoxFilePath.Name = "txtBoxFilePath";
          this.txtBoxFilePath.Size = new System.Drawing.Size(256, 24);
          this.txtBoxFilePath.TabIndex = 13;
          this.txtBoxFilePath.TextChanged += new System.EventHandler(this.change_Click);
          // 
          // groupBox4
          // 
          this.groupBox4.Controls.Add(this.txtBoxMemo1);
          this.groupBox4.Controls.Add(this.label31);
          this.groupBox4.Controls.Add(this.btnRightRest);
          this.groupBox4.Controls.Add(this.groupBox5);
          this.groupBox4.Controls.Add(this.chkBoxRightDisplay);
          this.groupBox4.Controls.Add(this.groupBox6);
          this.groupBox4.ForeColor = System.Drawing.Color.Blue;
          this.groupBox4.Location = new System.Drawing.Point(255, 269);
          this.groupBox4.Name = "groupBox4";
          this.groupBox4.Size = new System.Drawing.Size(439, 240);
          this.groupBox4.TabIndex = 15;
          this.groupBox4.TabStop = false;
          this.groupBox4.Text = "右投射";
          // 
          // txtBoxMemo1
          // 
          this.txtBoxMemo1.Location = new System.Drawing.Point(160, 20);
          this.txtBoxMemo1.MaxLength = 10;
          this.txtBoxMemo1.Name = "txtBoxMemo1";
          this.txtBoxMemo1.Size = new System.Drawing.Size(100, 24);
          this.txtBoxMemo1.TabIndex = 16;
          this.txtBoxMemo1.TextChanged += new System.EventHandler(this.change_Click);
          // 
          // label31
          // 
          this.label31.AutoSize = true;
          this.label31.Location = new System.Drawing.Point(122, 23);
          this.label31.Name = "label31";
          this.label31.Size = new System.Drawing.Size(35, 14);
          this.label31.TabIndex = 15;
          this.label31.Text = "註解";
          // 
          // btnRightRest
          // 
          this.btnRightRest.ForeColor = System.Drawing.SystemColors.ControlText;
          this.btnRightRest.Location = new System.Drawing.Point(339, 17);
          this.btnRightRest.Name = "btnRightRest";
          this.btnRightRest.Size = new System.Drawing.Size(65, 28);
          this.btnRightRest.TabIndex = 8;
          this.btnRightRest.Text = "重設";
          this.btnRightRest.UseVisualStyleBackColor = true;
          // 
          // groupBox5
          // 
          this.groupBox5.Controls.Add(this.txtBoxSubjectHeight1);
          this.groupBox5.Controls.Add(this.txtBoxSubjectWidth1);
          this.groupBox5.Controls.Add(this.txtBoxSubjectY1);
          this.groupBox5.Controls.Add(this.txtBoxSubjectX1);
          this.groupBox5.Controls.Add(this.label20);
          this.groupBox5.Controls.Add(this.label21);
          this.groupBox5.Controls.Add(this.label22);
          this.groupBox5.Controls.Add(this.label23);
          this.groupBox5.Location = new System.Drawing.Point(15, 48);
          this.groupBox5.Name = "groupBox5";
          this.groupBox5.Size = new System.Drawing.Size(409, 76);
          this.groupBox5.TabIndex = 14;
          this.groupBox5.TabStop = false;
          this.groupBox5.Text = "講題位置調整";
          // 
          // txtBoxSubjectHeight1
          // 
          this.txtBoxSubjectHeight1.Location = new System.Drawing.Point(323, 43);
          this.txtBoxSubjectHeight1.Maximum = new decimal(new int[] {
            768,
            0,
            0,
            0});
          this.txtBoxSubjectHeight1.Name = "txtBoxSubjectHeight1";
          this.txtBoxSubjectHeight1.Size = new System.Drawing.Size(66, 24);
          this.txtBoxSubjectHeight1.TabIndex = 11;
          this.txtBoxSubjectHeight1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
          this.txtBoxSubjectHeight1.ValueChanged += new System.EventHandler(this.change_Click);
          // 
          // txtBoxSubjectWidth1
          // 
          this.txtBoxSubjectWidth1.Location = new System.Drawing.Point(109, 43);
          this.txtBoxSubjectWidth1.Maximum = new decimal(new int[] {
            1024,
            0,
            0,
            0});
          this.txtBoxSubjectWidth1.Name = "txtBoxSubjectWidth1";
          this.txtBoxSubjectWidth1.Size = new System.Drawing.Size(65, 24);
          this.txtBoxSubjectWidth1.TabIndex = 10;
          this.txtBoxSubjectWidth1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
          this.txtBoxSubjectWidth1.ValueChanged += new System.EventHandler(this.change_Click);
          // 
          // txtBoxSubjectY1
          // 
          this.txtBoxSubjectY1.Location = new System.Drawing.Point(323, 22);
          this.txtBoxSubjectY1.Maximum = new decimal(new int[] {
            768,
            0,
            0,
            0});
          this.txtBoxSubjectY1.Minimum = new decimal(new int[] {
            10,
            0,
            0,
            -2147483648});
          this.txtBoxSubjectY1.Name = "txtBoxSubjectY1";
          this.txtBoxSubjectY1.Size = new System.Drawing.Size(66, 24);
          this.txtBoxSubjectY1.TabIndex = 9;
          this.txtBoxSubjectY1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
          this.txtBoxSubjectY1.ValueChanged += new System.EventHandler(this.change_Click);
          // 
          // txtBoxSubjectX1
          // 
          this.txtBoxSubjectX1.Location = new System.Drawing.Point(109, 22);
          this.txtBoxSubjectX1.Maximum = new decimal(new int[] {
            1024,
            0,
            0,
            0});
          this.txtBoxSubjectX1.Minimum = new decimal(new int[] {
            10,
            0,
            0,
            -2147483648});
          this.txtBoxSubjectX1.Name = "txtBoxSubjectX1";
          this.txtBoxSubjectX1.Size = new System.Drawing.Size(65, 24);
          this.txtBoxSubjectX1.TabIndex = 8;
          this.txtBoxSubjectX1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
          this.txtBoxSubjectX1.ValueChanged += new System.EventHandler(this.change_Click);
          // 
          // label20
          // 
          this.label20.AutoSize = true;
          this.label20.Location = new System.Drawing.Point(206, 49);
          this.label20.Name = "label20";
          this.label20.Size = new System.Drawing.Size(89, 14);
          this.label20.TabIndex = 5;
          this.label20.Text = "可視大小 [高]";
          // 
          // label21
          // 
          this.label21.AutoSize = true;
          this.label21.Location = new System.Drawing.Point(7, 49);
          this.label21.Name = "label21";
          this.label21.Size = new System.Drawing.Size(89, 14);
          this.label21.TabIndex = 4;
          this.label21.Text = "可視大小 [寬]";
          // 
          // label22
          // 
          this.label22.AutoSize = true;
          this.label22.Location = new System.Drawing.Point(206, 26);
          this.label22.Name = "label22";
          this.label22.Size = new System.Drawing.Size(84, 14);
          this.label22.TabIndex = 1;
          this.label22.Text = "起始座標 [Y]";
          // 
          // label23
          // 
          this.label23.AutoSize = true;
          this.label23.Location = new System.Drawing.Point(6, 26);
          this.label23.Name = "label23";
          this.label23.Size = new System.Drawing.Size(84, 14);
          this.label23.TabIndex = 0;
          this.label23.Text = "起始座標 [X]";
          // 
          // chkBoxRightDisplay
          // 
          this.chkBoxRightDisplay.AutoSize = true;
          this.chkBoxRightDisplay.Location = new System.Drawing.Point(25, 23);
          this.chkBoxRightDisplay.Name = "chkBoxRightDisplay";
          this.chkBoxRightDisplay.Size = new System.Drawing.Size(54, 18);
          this.chkBoxRightDisplay.TabIndex = 10;
          this.chkBoxRightDisplay.Text = "啟用";
          this.chkBoxRightDisplay.UseVisualStyleBackColor = true;
          this.chkBoxRightDisplay.CheckedChanged += new System.EventHandler(this.chkBoxDisplay_Click);
          // 
          // groupBox6
          // 
          this.groupBox6.Controls.Add(this.txtBoxContentLength1);
          this.groupBox6.Controls.Add(this.txtBoxContentLines1);
          this.groupBox6.Controls.Add(this.txtBoxContentHeight1);
          this.groupBox6.Controls.Add(this.txtBoxCurrentY1);
          this.groupBox6.Controls.Add(this.txtBoxContentWidth1);
          this.groupBox6.Controls.Add(this.txtBoxCurrentX1);
          this.groupBox6.Controls.Add(this.label24);
          this.groupBox6.Controls.Add(this.label25);
          this.groupBox6.Controls.Add(this.label26);
          this.groupBox6.Controls.Add(this.label27);
          this.groupBox6.Controls.Add(this.label28);
          this.groupBox6.Controls.Add(this.label29);
          this.groupBox6.Location = new System.Drawing.Point(15, 130);
          this.groupBox6.Name = "groupBox6";
          this.groupBox6.Size = new System.Drawing.Size(409, 106);
          this.groupBox6.TabIndex = 11;
          this.groupBox6.TabStop = false;
          this.groupBox6.Text = "視訊文字細部調整";
          // 
          // txtBoxContentLength1
          // 
          this.txtBoxContentLength1.Location = new System.Drawing.Point(324, 75);
          this.txtBoxContentLength1.Maximum = new decimal(new int[] {
            20,
            0,
            0,
            0});
          this.txtBoxContentLength1.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
          this.txtBoxContentLength1.Name = "txtBoxContentLength1";
          this.txtBoxContentLength1.Size = new System.Drawing.Size(65, 24);
          this.txtBoxContentLength1.TabIndex = 19;
          this.txtBoxContentLength1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
          this.txtBoxContentLength1.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
          this.txtBoxContentLength1.ValueChanged += new System.EventHandler(this.change_Click);
          // 
          // txtBoxContentLines1
          // 
          this.txtBoxContentLines1.Location = new System.Drawing.Point(110, 75);
          this.txtBoxContentLines1.Maximum = new decimal(new int[] {
            20,
            0,
            0,
            0});
          this.txtBoxContentLines1.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
          this.txtBoxContentLines1.Name = "txtBoxContentLines1";
          this.txtBoxContentLines1.Size = new System.Drawing.Size(64, 24);
          this.txtBoxContentLines1.TabIndex = 18;
          this.txtBoxContentLines1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
          this.txtBoxContentLines1.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
          this.txtBoxContentLines1.ValueChanged += new System.EventHandler(this.change_Click);
          // 
          // txtBoxContentHeight1
          // 
          this.txtBoxContentHeight1.Location = new System.Drawing.Point(324, 53);
          this.txtBoxContentHeight1.Maximum = new decimal(new int[] {
            768,
            0,
            0,
            0});
          this.txtBoxContentHeight1.Name = "txtBoxContentHeight1";
          this.txtBoxContentHeight1.Size = new System.Drawing.Size(65, 24);
          this.txtBoxContentHeight1.TabIndex = 17;
          this.txtBoxContentHeight1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
          this.txtBoxContentHeight1.ValueChanged += new System.EventHandler(this.change_Click);
          // 
          // txtBoxCurrentY1
          // 
          this.txtBoxCurrentY1.Location = new System.Drawing.Point(324, 31);
          this.txtBoxCurrentY1.Maximum = new decimal(new int[] {
            768,
            0,
            0,
            0});
          this.txtBoxCurrentY1.Minimum = new decimal(new int[] {
            10,
            0,
            0,
            -2147483648});
          this.txtBoxCurrentY1.Name = "txtBoxCurrentY1";
          this.txtBoxCurrentY1.Size = new System.Drawing.Size(65, 24);
          this.txtBoxCurrentY1.TabIndex = 16;
          this.txtBoxCurrentY1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
          this.txtBoxCurrentY1.ValueChanged += new System.EventHandler(this.change_Click);
          // 
          // txtBoxContentWidth1
          // 
          this.txtBoxContentWidth1.Location = new System.Drawing.Point(110, 53);
          this.txtBoxContentWidth1.Maximum = new decimal(new int[] {
            1024,
            0,
            0,
            0});
          this.txtBoxContentWidth1.Name = "txtBoxContentWidth1";
          this.txtBoxContentWidth1.Size = new System.Drawing.Size(64, 24);
          this.txtBoxContentWidth1.TabIndex = 15;
          this.txtBoxContentWidth1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
          this.txtBoxContentWidth1.ValueChanged += new System.EventHandler(this.change_Click);
          // 
          // txtBoxCurrentX1
          // 
          this.txtBoxCurrentX1.Location = new System.Drawing.Point(110, 31);
          this.txtBoxCurrentX1.Maximum = new decimal(new int[] {
            1024,
            0,
            0,
            0});
          this.txtBoxCurrentX1.Minimum = new decimal(new int[] {
            10,
            0,
            0,
            -2147483648});
          this.txtBoxCurrentX1.Name = "txtBoxCurrentX1";
          this.txtBoxCurrentX1.Size = new System.Drawing.Size(64, 24);
          this.txtBoxCurrentX1.TabIndex = 14;
          this.txtBoxCurrentX1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
          this.txtBoxCurrentX1.ValueChanged += new System.EventHandler(this.change_Click);
          // 
          // label24
          // 
          this.label24.AutoSize = true;
          this.label24.Location = new System.Drawing.Point(206, 80);
          this.label24.Name = "label24";
          this.label24.Size = new System.Drawing.Size(63, 14);
          this.label24.TabIndex = 12;
          this.label24.Text = "每行字數";
          // 
          // label25
          // 
          this.label25.AutoSize = true;
          this.label25.Location = new System.Drawing.Point(7, 80);
          this.label25.Name = "label25";
          this.label25.Size = new System.Drawing.Size(63, 14);
          this.label25.TabIndex = 5;
          this.label25.Text = "顯示行數";
          // 
          // label26
          // 
          this.label26.AutoSize = true;
          this.label26.Location = new System.Drawing.Point(206, 58);
          this.label26.Name = "label26";
          this.label26.Size = new System.Drawing.Size(89, 14);
          this.label26.TabIndex = 3;
          this.label26.Text = "可視大小 [高]";
          // 
          // label27
          // 
          this.label27.AutoSize = true;
          this.label27.Location = new System.Drawing.Point(7, 58);
          this.label27.Name = "label27";
          this.label27.Size = new System.Drawing.Size(89, 14);
          this.label27.TabIndex = 2;
          this.label27.Text = "可視大小 [寬]";
          // 
          // label28
          // 
          this.label28.AutoSize = true;
          this.label28.Location = new System.Drawing.Point(206, 35);
          this.label28.Name = "label28";
          this.label28.Size = new System.Drawing.Size(84, 14);
          this.label28.TabIndex = 1;
          this.label28.Text = "起始座標 [Y]";
          // 
          // label29
          // 
          this.label29.AutoSize = true;
          this.label29.Location = new System.Drawing.Point(7, 35);
          this.label29.Name = "label29";
          this.label29.Size = new System.Drawing.Size(84, 14);
          this.label29.TabIndex = 0;
          this.label29.Text = "起始座標 [X]";
          // 
          // btnSelectDir
          // 
          this.btnSelectDir.ForeColor = System.Drawing.SystemColors.ControlText;
          this.btnSelectDir.Location = new System.Drawing.Point(594, 515);
          this.btnSelectDir.Name = "btnSelectDir";
          this.btnSelectDir.Size = new System.Drawing.Size(65, 28);
          this.btnSelectDir.TabIndex = 16;
          this.btnSelectDir.Text = "選取";
          this.btnSelectDir.UseVisualStyleBackColor = true;
          this.btnSelectDir.Click += new System.EventHandler(this.btnSelectDir_Click);
          // 
          // ResolutionDlg
          // 
          this.AcceptButton = this.btnOK;
          this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
          this.CancelButton = this.btnCancel;
          this.ClientSize = new System.Drawing.Size(707, 621);
          this.Controls.Add(this.btnSelectDir);
          this.Controls.Add(this.groupBox4);
          this.Controls.Add(this.txtBoxFilePath);
          this.Controls.Add(this.label13);
          this.Controls.Add(this.groupBox1);
          this.Controls.Add(this.btnOK);
          this.Controls.Add(this.label7);
          this.Controls.Add(this.label4);
          this.Controls.Add(this.label6);
          this.Controls.Add(this.label5);
          this.Controls.Add(this.label3);
          this.Controls.Add(this.trackBarClient);
          this.Controls.Add(this.label2);
          this.Controls.Add(this.trackBarHost);
          this.Controls.Add(this.label8);
          this.Controls.Add(this.label1);
          this.Controls.Add(this.pictureBox2);
          this.Controls.Add(this.pictureBox1);
          this.Controls.Add(this.btnApply);
          this.Controls.Add(this.btnCancel);
          this.Font = new System.Drawing.Font("新細明體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
          this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
          this.MaximizeBox = false;
          this.Name = "ResolutionDlg";
          this.ShowInTaskbar = false;
          this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
          this.Text = "進階設定";
          this.Load += new System.EventHandler(this.ResolutionDlg_Load);
          ((System.ComponentModel.ISupportInitialize)(this.trackBarHost)).EndInit();
          ((System.ComponentModel.ISupportInitialize)(this.trackBarClient)).EndInit();
          ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
          ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
          this.groupBox1.ResumeLayout(false);
          this.groupBox1.PerformLayout();
          this.groupBox3.ResumeLayout(false);
          this.groupBox3.PerformLayout();
          ((System.ComponentModel.ISupportInitialize)(this.txtBoxSubjectHeight)).EndInit();
          ((System.ComponentModel.ISupportInitialize)(this.txtBoxSubjectWidth)).EndInit();
          ((System.ComponentModel.ISupportInitialize)(this.txtBoxSubjectY)).EndInit();
          ((System.ComponentModel.ISupportInitialize)(this.txtBoxSubjectX)).EndInit();
          this.groupBox2.ResumeLayout(false);
          this.groupBox2.PerformLayout();
          ((System.ComponentModel.ISupportInitialize)(this.txtBoxContentLength)).EndInit();
          ((System.ComponentModel.ISupportInitialize)(this.txtBoxContentLines)).EndInit();
          ((System.ComponentModel.ISupportInitialize)(this.txtBoxContentHeight)).EndInit();
          ((System.ComponentModel.ISupportInitialize)(this.txtBoxCurrentY)).EndInit();
          ((System.ComponentModel.ISupportInitialize)(this.txtBoxContentWidth)).EndInit();
          ((System.ComponentModel.ISupportInitialize)(this.txtBoxCurrentX)).EndInit();
          this.groupBox4.ResumeLayout(false);
          this.groupBox4.PerformLayout();
          this.groupBox5.ResumeLayout(false);
          this.groupBox5.PerformLayout();
          ((System.ComponentModel.ISupportInitialize)(this.txtBoxSubjectHeight1)).EndInit();
          ((System.ComponentModel.ISupportInitialize)(this.txtBoxSubjectWidth1)).EndInit();
          ((System.ComponentModel.ISupportInitialize)(this.txtBoxSubjectY1)).EndInit();
          ((System.ComponentModel.ISupportInitialize)(this.txtBoxSubjectX1)).EndInit();
          this.groupBox6.ResumeLayout(false);
          this.groupBox6.PerformLayout();
          ((System.ComponentModel.ISupportInitialize)(this.txtBoxContentLength1)).EndInit();
          ((System.ComponentModel.ISupportInitialize)(this.txtBoxContentLines1)).EndInit();
          ((System.ComponentModel.ISupportInitialize)(this.txtBoxContentHeight1)).EndInit();
          ((System.ComponentModel.ISupportInitialize)(this.txtBoxCurrentY1)).EndInit();
          ((System.ComponentModel.ISupportInitialize)(this.txtBoxContentWidth1)).EndInit();
          ((System.ComponentModel.ISupportInitialize)(this.txtBoxCurrentX1)).EndInit();
          this.ResumeLayout(false);
          this.PerformLayout();

      }

      #endregion

      private System.Windows.Forms.Button btnCancel;
      private System.Windows.Forms.Button btnApply;
      private System.Windows.Forms.PictureBox pictureBox1;
      private System.Windows.Forms.Label label1;
      private System.Windows.Forms.TrackBar trackBarHost;
      private System.Windows.Forms.Label label2;
      private System.Windows.Forms.Label label3;
      private System.Windows.Forms.Label label4;
      private System.Windows.Forms.Button btnOK;
      private System.Windows.Forms.TrackBar trackBarClient;
      private System.Windows.Forms.Label label5;
      private System.Windows.Forms.Label label6;
      private System.Windows.Forms.Label label7;
      private System.Windows.Forms.PictureBox pictureBox2;
      private System.Windows.Forms.Label label8;
      private System.Windows.Forms.GroupBox groupBox1;
      private System.Windows.Forms.GroupBox groupBox2;
      private System.Windows.Forms.Label label11;
      private System.Windows.Forms.Label label10;
      private System.Windows.Forms.Label label9;
      private System.Windows.Forms.Label label12;
      private System.Windows.Forms.Label label14;
      private System.Windows.Forms.Label label15;
      private System.Windows.Forms.Label label13;
      private System.Windows.Forms.TextBox txtBoxFilePath;
      private System.Windows.Forms.GroupBox groupBox3;
      private System.Windows.Forms.Label label17;
      private System.Windows.Forms.Label label16;
      private System.Windows.Forms.Button btnLeftRest;
      private System.Windows.Forms.Label label19;
      private System.Windows.Forms.Label label18;
      private System.Windows.Forms.CheckBox chkBoxLeftDisplay;
      private System.Windows.Forms.GroupBox groupBox4;
      private System.Windows.Forms.Button btnRightRest;
      private System.Windows.Forms.GroupBox groupBox5;
      private System.Windows.Forms.Label label20;
      private System.Windows.Forms.Label label21;
      private System.Windows.Forms.Label label22;
      private System.Windows.Forms.Label label23;
      private System.Windows.Forms.CheckBox chkBoxRightDisplay;
      private System.Windows.Forms.GroupBox groupBox6;
      private System.Windows.Forms.Label label24;
      private System.Windows.Forms.Label label25;
      private System.Windows.Forms.Label label26;
      private System.Windows.Forms.Label label27;
      private System.Windows.Forms.Label label28;
      private System.Windows.Forms.Label label29;
      private System.Windows.Forms.Label label30;
      private System.Windows.Forms.Label label31;
      private System.Windows.Forms.TextBox txtBoxMemo;
      private System.Windows.Forms.TextBox txtBoxMemo1;
      private System.Windows.Forms.NumericUpDown txtBoxSubjectX;
      private System.Windows.Forms.NumericUpDown txtBoxSubjectY;
      private System.Windows.Forms.NumericUpDown txtBoxSubjectWidth;
      private System.Windows.Forms.NumericUpDown txtBoxSubjectHeight;
      private System.Windows.Forms.NumericUpDown txtBoxCurrentX;
      private System.Windows.Forms.NumericUpDown txtBoxContentWidth;
      private System.Windows.Forms.NumericUpDown txtBoxCurrentY;
      private System.Windows.Forms.NumericUpDown txtBoxContentHeight;
      private System.Windows.Forms.NumericUpDown txtBoxContentLines;
      private System.Windows.Forms.NumericUpDown txtBoxContentLength;
      private System.Windows.Forms.NumericUpDown txtBoxSubjectX1;
      private System.Windows.Forms.NumericUpDown txtBoxSubjectY1;
      private System.Windows.Forms.NumericUpDown txtBoxSubjectWidth1;
      private System.Windows.Forms.NumericUpDown txtBoxSubjectHeight1;
      private System.Windows.Forms.NumericUpDown txtBoxCurrentX1;
      private System.Windows.Forms.NumericUpDown txtBoxContentWidth1;
      private System.Windows.Forms.NumericUpDown txtBoxCurrentY1;
      private System.Windows.Forms.NumericUpDown txtBoxContentHeight1;
      private System.Windows.Forms.NumericUpDown txtBoxContentLines1;
      private System.Windows.Forms.NumericUpDown txtBoxContentLength1;
      private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog1;
      private System.Windows.Forms.Button btnSelectDir;
   }
}