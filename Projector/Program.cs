﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace Projector
{
   static class Program
   {
      /// <summary>
      /// 應用程式的主要進入點。
      /// </summary>
      [STAThread]
      static void Main()
      {
         Register reg = new Register();
         ShareMem.SizeOfHost = SystemInformation.PrimaryMonitorSize;
         //if (SystemInformation.MonitorCount > 1)//Num of Monitor = 2
         //   ShareMem.SizeOfClient = new System.Drawing.Size(SystemInformation.VirtualScreen.Width - SystemInformation.PrimaryMonitorSize.Width,
         //                                                   SystemInformation.PrimaryMonitorSize.Height);
         //ShareMem.SizeOfHost = reg.GetSizeOfHostKey();
         ShareMem.SizeOfClient = reg.GetSizeOfClientKey();
         ShareMem.curDisplayLocation = reg.GetDisplayOfLocation();

         Application.EnableVisualStyles();
         Application.SetCompatibleTextRenderingDefault(false);
         Application.Run(new MainFrame());
      }
   }
}