using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Projector
{
   public partial class ResolutionDlg : Form
   {
      public ResolutionDlg()
      {
         InitializeComponent();
      }

      private void ResolutionDlg_Load(object sender, EventArgs e)
      {
         this.TransparencyKey = Color.Red;
         //this.label1.BackColor = Color.Red;

         //if (ShareMem.SizeOfHost.Width == 800)
         //{
            //trackBarHost.Value = 0;
            //label4.Text = "800 x 600 個像數";
            trackBarHost.Value = 1;
            label4.Text = ShareMem.SizeOfHost.Width.ToString() + " x " +
                          ShareMem.SizeOfHost.Height.ToString() + " 個像數"; 
         //}
         //else
         //{
         //   trackBarHost.Value = 1;
         //   label4.Text = "1024 x 768 個像數";
         //}

         if (ShareMem.SizeOfClient.Width == 800)
         {
            trackBarClient.Value = 0;
            label7.Text = "800 x 600 個像數";
         }
         else
         {
            trackBarClient.Value = 1;
            label7.Text = "1024 x 768 個像數";
         }

         //投射方向
         if (String.Compare(ShareMem.curDisplayLocation, "LR", true) == 0)
         {
             chkBoxLeftDisplay.Checked = true;
             chkBoxRightDisplay.Checked = true;
         }
         else if (String.Compare(ShareMem.curDisplayLocation, "L", true) == 0)
         {
             chkBoxLeftDisplay.Checked = true;
             chkBoxRightDisplay.Checked = false;
             groupBox5.Enabled = false;
             groupBox6.Enabled = false;
             btnRightRest.Enabled = false;
             label31.Enabled = false;
             txtBoxMemo1.Enabled = false;
         }
         else if (String.Compare(ShareMem.curDisplayLocation, "R", true) == 0)
         {
             chkBoxLeftDisplay.Checked = false;
             chkBoxRightDisplay.Checked = true;
             groupBox2.Enabled = false;
             groupBox3.Enabled = false;
             btnLeftRest.Enabled = false;
             label30.Enabled = false;
             txtBoxMemo.Enabled = false;
         }
         else
         {
             chkBoxLeftDisplay.Checked = false;
             chkBoxRightDisplay.Checked = false;
             groupBox2.Enabled = false;
             groupBox3.Enabled = false;
             groupBox5.Enabled = false;
             groupBox6.Enabled = false;
             btnRightRest.Enabled = false;
             btnLeftRest.Enabled = false;
             label30.Enabled = false;
             label31.Enabled = false;
             txtBoxMemo.Enabled = false;
             txtBoxMemo1.Enabled = false;
         }

         Register reg = new Register();

         //細部調整
         txtBoxFilePath.Text = reg.GetFilePath();
         //左投射
         txtBoxCurrentX.Value = ShareMem.currentX;
         txtBoxCurrentY.Value = ShareMem.currentY;
         txtBoxContentWidth.Value = ShareMem.contentWidth;
         txtBoxContentHeight.Value = ShareMem.contentHeight;
         txtBoxContentLines.Value = ShareMem.contentLines;
         txtBoxContentLength.Value = ShareMem.contentLength;
         txtBoxSubjectX.Value = ShareMem.subjectX;
         txtBoxSubjectY.Value = ShareMem.subjectY;
         txtBoxSubjectWidth.Value = ShareMem.subjectWidth;
         txtBoxSubjectHeight.Value = ShareMem.subjectHeight;
         txtBoxMemo.Text = ShareMem.memo;
         //右投射
         txtBoxCurrentX1.Value = ShareMem.currentX1;
         txtBoxCurrentY1.Value = ShareMem.currentY1;
         txtBoxContentWidth1.Value = ShareMem.contentWidth1;
         txtBoxContentHeight1.Value = ShareMem.contentHeight1;
         txtBoxContentLines1.Value = ShareMem.contentLines1;
         txtBoxContentLength1.Value = ShareMem.contentLength1;
         txtBoxSubjectX1.Value = ShareMem.subjectX1;
         txtBoxSubjectY1.Value = ShareMem.subjectY1;
         txtBoxSubjectWidth1.Value = ShareMem.subjectWidth1;
         txtBoxSubjectHeight1.Value = ShareMem.subjectHeight1;
         txtBoxMemo1.Text = ShareMem.memo1;

         btnApply.Enabled = false;
      }

      private void trackBar1_Scroll(object sender, EventArgs e)
      {
         //if (btnApply.Enabled == false)
         //   btnApply.Enabled = true;

         //if (trackBarHost.Value == 0)
         //   label4.Text = "800 x 600 個像數";
         //else
         //   label4.Text = "1024 x 768 個像數";
      }

      private void trackBarClient_Scroll(object sender, EventArgs e)
      {
         if (btnApply.Enabled == false)
            btnApply.Enabled = true;

         if (trackBarClient.Value == 0)
            label7.Text = "800 x 600 個像數";
         else
            label7.Text = "1024 x 768 個像數";
      }

      private void change_Click(object sender, EventArgs e)
      {
          if (btnApply.Enabled == false)
              btnApply.Enabled = true;
      }

      private void btnApply_Click(object sender, EventArgs e)
      {
         Register reg = new Register();

         //if (trackBarHost.Value == 0)
         //{
         //   ShareMem.SizeOfHost = new Size(800, 600);
         //   reg.SetSizeOfHostKey(ShareMem.SizeOfHost);
         //}
         //else
         //{
         //   ShareMem.SizeOfHost = new Size(1024, 768);
         //   reg.SetSizeOfHostKey(ShareMem.SizeOfHost);
         //}
         reg.SetSizeOfHostKey(ShareMem.SizeOfHost);

         if (trackBarClient.Value == 0)
         {
            ShareMem.SizeOfClient = new Size(800, 600);
            reg.SetSizeOfClientKey(ShareMem.SizeOfClient);
         }
         else
         {
            ShareMem.SizeOfClient = new Size(1024, 768);
            reg.SetSizeOfClientKey(ShareMem.SizeOfClient);
         }

         //投射方向
         if (chkBoxLeftDisplay.Checked == true && chkBoxRightDisplay.Checked == true)
         {
             ShareMem.curDisplayLocation = "LR";
             reg.SetDisplayOfLocation("LR");
         }
         else if (chkBoxLeftDisplay.Checked == true)
         {
             ShareMem.curDisplayLocation = "L";
             reg.SetDisplayOfLocation("L");
         }
         else if (chkBoxRightDisplay.Checked == true)
         {
             ShareMem.curDisplayLocation = "R";
             reg.SetDisplayOfLocation("R");
         }
         else
         {
             ShareMem.curDisplayLocation = "";
             reg.SetDisplayOfLocation("");
         }

         if (chkBoxLeftDisplay.Checked == true)
         {
             //左投射
             ShareMem.currentX = (int) txtBoxCurrentX.Value;
             reg.SetCurrentX((int) txtBoxCurrentX.Value);
             ShareMem.currentY = (int) txtBoxCurrentY.Value;
             reg.SetCurrentY((int) txtBoxCurrentY.Value);
             ShareMem.contentWidth = (int) txtBoxContentWidth.Value;
             reg.SetContentWidth((int) txtBoxContentWidth.Value);
             ShareMem.contentHeight = (int) txtBoxContentHeight.Value;
             reg.SetContentHeight((int) txtBoxContentHeight.Value);
             ShareMem.contentLines = (int) txtBoxContentLines.Value;
             reg.SetContentLines((int) txtBoxContentLines.Value);
             ShareMem.contentLength = (int) txtBoxContentLength.Value;
             reg.SetContentLength((int) txtBoxContentLength.Value);
             ShareMem.subjectX = (int) txtBoxSubjectX.Value;
             reg.SetSubjectX((int) txtBoxSubjectX.Value);
             ShareMem.subjectY = (int) txtBoxSubjectY.Value;
             reg.SetSubjectY((int) txtBoxSubjectY.Value);
             ShareMem.subjectWidth = (int) txtBoxSubjectWidth.Value;
             reg.SetSubjectWidth((int) txtBoxSubjectWidth.Value);
             ShareMem.subjectHeight = (int) txtBoxSubjectHeight.Value;
             reg.SetSubjectHeight((int) txtBoxSubjectHeight.Value);
             ShareMem.memo = txtBoxMemo.Text;
             reg.SetMemo(txtBoxMemo.Text);
         }
         if (chkBoxRightDisplay.Checked == true)
         {
             //右投射
             ShareMem.currentX1 = (int) txtBoxCurrentX1.Value;
             reg.SetCurrentX1((int) txtBoxCurrentX1.Value);
             ShareMem.currentY1 = (int) txtBoxCurrentY1.Value;
             reg.SetCurrentY1((int) txtBoxCurrentY1.Value);
             ShareMem.contentWidth1 = (int) txtBoxContentWidth1.Value;
             reg.SetContentWidth1((int) txtBoxContentWidth1.Value);
             ShareMem.contentHeight1 = (int) txtBoxContentHeight1.Value;
             reg.SetContentHeight1((int) txtBoxContentHeight1.Value);
             ShareMem.contentLines1 = (int) txtBoxContentLines1.Value;
             reg.SetContentLines1((int) txtBoxContentLines1.Value);
             ShareMem.contentLength1 = (int) txtBoxContentLength1.Value;
             reg.SetContentLength1((int) txtBoxContentLength1.Value);
             ShareMem.subjectX1 = (int) txtBoxSubjectX1.Value;
             reg.SetSubjectX1((int) txtBoxSubjectX1.Value);
             ShareMem.subjectY1 = (int) txtBoxSubjectY1.Value;
             reg.SetSubjectY1((int) txtBoxSubjectY1.Value);
             ShareMem.subjectWidth1 = (int) txtBoxSubjectWidth1.Value;
             reg.SetSubjectWidth1((int) txtBoxSubjectWidth1.Value);
             ShareMem.subjectHeight1 = (int) txtBoxSubjectHeight1.Value;
             reg.SetSubjectHeight1((int) txtBoxSubjectHeight1.Value);
             ShareMem.memo1 = txtBoxMemo1.Text;
             reg.SetMemo1(txtBoxMemo1.Text);
         }

         //細部調整
         reg.SetFilePath(txtBoxFilePath.Text);

         btnApply.Enabled = false;
      }

      private void btnOK_Click(object sender, EventArgs e)
      {
         btnApply.PerformClick();
      }

      //投影機設定
      private void chkBoxDisplay_Click(object sender, EventArgs e)
      {
          //左投射
          if (chkBoxLeftDisplay.Checked == true)
          {
              groupBox2.Enabled = true;
              groupBox3.Enabled = true;
              btnLeftRest.Enabled = true;
              label30.Enabled = true;
              txtBoxMemo.Enabled = true;
          }
          else
          {
              groupBox2.Enabled = false;
              groupBox3.Enabled = false;
              btnLeftRest.Enabled = false;
              label30.Enabled = false;
              txtBoxMemo.Enabled = false;
          }

          //右投射
          if (chkBoxRightDisplay.Checked == true)
          {
              groupBox5.Enabled = true;
              groupBox6.Enabled = true;
              btnRightRest.Enabled = true;
              label31.Enabled = true;
              txtBoxMemo1.Enabled = true;
          }
          else
          {
              groupBox5.Enabled = false;
              groupBox6.Enabled = false;
              btnRightRest.Enabled = false;
              label31.Enabled = false;
              txtBoxMemo1.Enabled = false;
          }

          if (btnApply.Enabled == false)
              btnApply.Enabled = true;
      }

      //重設-左投射
      private void btnLeftRest_Click(object sender, EventArgs e)
      {
          txtBoxSubjectX.Value = 0;
          txtBoxSubjectY.Value = -4;
          txtBoxSubjectWidth.Value = 396;
          txtBoxSubjectHeight.Value = 50;
          txtBoxCurrentX.Value = 2;
          txtBoxCurrentY.Value = 127;
          txtBoxContentWidth.Value = 396;
          txtBoxContentHeight.Value = 160;
          txtBoxContentLines.Value = 7;
          txtBoxContentLength.Value = 15;

          if (btnApply.Enabled == false)
              btnApply.Enabled = true;
      }

      //重設-右投射
      private void btnRightRest_Click(object sender, EventArgs e)
      {
          txtBoxSubjectX1.Value = 0;
          txtBoxSubjectY1.Value = -4;
          txtBoxSubjectWidth1.Value = 396;
          txtBoxSubjectHeight1.Value = 50;
          txtBoxCurrentX1.Value = 2;
          txtBoxCurrentY1.Value = 127;
          txtBoxContentWidth1.Value = 396;
          txtBoxContentHeight1.Value = 160;
          txtBoxContentLines1.Value = 7;
          txtBoxContentLength1.Value = 15;

          if (btnApply.Enabled == false)
              btnApply.Enabled = true;
      }

      //修改存檔路徑
      private void btnSelectDir_Click(object sender, EventArgs e)
      {
          folderBrowserDialog1.SelectedPath = @"C:\";
          DialogResult result = folderBrowserDialog1.ShowDialog();
          if (result == DialogResult.OK)
          {
              txtBoxFilePath.Text = folderBrowserDialog1.SelectedPath;
          }
      }
   }
}