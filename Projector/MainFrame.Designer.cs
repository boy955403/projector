﻿namespace Projector
{
   partial class MainFrame
   {
      /// <summary>
      /// 設計工具所需的變數。
      /// </summary>
      private System.ComponentModel.IContainer components = null;

      /// <summary>
      /// 清除任何使用中的資源。
      /// </summary>
      /// <param name="disposing">如果應該公開 Managed 資源則為 true，否則為 false。</param>
      protected override void Dispose(bool disposing)
      {
         if (disposing && (components != null))
         {
            components.Dispose();
         }
         base.Dispose(disposing);
      }

      #region Windows Form 設計工具產生的程式碼

      /// <summary>
      /// 此為設計工具支援所需的方法 - 請勿使用程式碼編輯器修改這個方法的內容。
      ///
      /// </summary>
      private void InitializeComponent()
      {
          this.components = new System.ComponentModel.Container();
          System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainFrame));
          this.MdiMenuStrip = new System.Windows.Forms.MenuStrip();
          this.SettingsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
          this.ResolutionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
          this.HelpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
          this.OnlineHelpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
          this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
          this.AboutToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
          this.btnHide = new System.Windows.Forms.Button();
          this.btnShow = new System.Windows.Forms.Button();
          this.comboBoxTitle1 = new System.Windows.Forms.ComboBox();
          this.pnlPreview = new System.Windows.Forms.Panel();
          this.lblVolChapSecPage = new System.Windows.Forms.Label();
          this.lblName3 = new System.Windows.Forms.Label();
          this.lblName2 = new System.Windows.Forms.Label();
          this.lblOldPoem = new System.Windows.Forms.Label();
          this.lblNewPoem = new System.Windows.Forms.Label();
          this.txtBoxSong2 = new System.Windows.Forms.TextBox();
          this.txtBoxSong1 = new System.Windows.Forms.TextBox();
          this.txtBoxName1 = new System.Windows.Forms.TextBox();
          this.txtBoxSubject = new System.Windows.Forms.TextBox();
          this.lblPoem2 = new System.Windows.Forms.Label();
          this.lblComma = new System.Windows.Forms.Label();
          this.lblPoem1 = new System.Windows.Forms.Label();
          this.grpBoxPreview = new System.Windows.Forms.GroupBox();
          this.fontDialog1 = new System.Windows.Forms.FontDialog();
          this.colorDialog1 = new System.Windows.Forms.ColorDialog();
          this.btnFontOfSubject = new System.Windows.Forms.Button();
          this.btnColorOfSubject = new System.Windows.Forms.Button();
          this.grpBoxSubject = new System.Windows.Forms.GroupBox();
          this.btnCallPPS = new System.Windows.Forms.Button();
          this.grpBoxSubItem = new System.Windows.Forms.GroupBox();
          this.btnHideName1Song1 = new System.Windows.Forms.Button();
          this.chkBoxTranslator = new System.Windows.Forms.CheckBox();
          this.btnColorOfTitle = new System.Windows.Forms.Button();
          this.btnSendName1Song1 = new System.Windows.Forms.Button();
          this.comboBoxTitle2 = new System.Windows.Forms.ComboBox();
          this.txtBoxName2 = new System.Windows.Forms.TextBox();
          this.btnColorOfOther = new System.Windows.Forms.Button();
          this.btnFontOfOther = new System.Windows.Forms.Button();
          this.groupBox1 = new System.Windows.Forms.GroupBox();
          this.chkBoxToTextBox1 = new System.Windows.Forms.CheckBox();
          this.btnLast = new System.Windows.Forms.Button();
          this.txtBoxQuery = new System.Windows.Forms.TextBox();
          this.btnNext = new System.Windows.Forms.Button();
          this.chkBoxScripture = new System.Windows.Forms.CheckBox();
          this.groupBox2 = new System.Windows.Forms.GroupBox();
          this.label4 = new System.Windows.Forms.Label();
          this.comboBox1 = new System.Windows.Forms.ComboBox();
          this.label3 = new System.Windows.Forms.Label();
          this.txtBoxName4 = new System.Windows.Forms.TextBox();
          this.comboBoxTitle4 = new System.Windows.Forms.ComboBox();
          this.btnSendName2Song2 = new System.Windows.Forms.Button();
          this.txtBoxSong3 = new System.Windows.Forms.TextBox();
          this.label2 = new System.Windows.Forms.Label();
          this.label1 = new System.Windows.Forms.Label();
          this.txtBoxName3 = new System.Windows.Forms.TextBox();
          this.comboBoxTitle3 = new System.Windows.Forms.ComboBox();
          this.btnClear = new System.Windows.Forms.Button();
          this.fontDialog2 = new System.Windows.Forms.FontDialog();
          this.colorDialog2 = new System.Windows.Forms.ColorDialog();
          this.btnBrowse = new System.Windows.Forms.Button();
          this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
          this.groupBox3 = new System.Windows.Forms.GroupBox();
          this.btnSendBackground = new System.Windows.Forms.Button();
          this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
          this.groupBox4 = new System.Windows.Forms.GroupBox();
          this.listBox2 = new System.Windows.Forms.ListBox();
          this.listBox1 = new System.Windows.Forms.ListBox();
          this.groupBox5 = new System.Windows.Forms.GroupBox();
          this.btnEditText = new System.Windows.Forms.Button();
          this.btnReloadText = new System.Windows.Forms.Button();
          this.btnSendLoadedText = new System.Windows.Forms.Button();
          this.listBox6 = new System.Windows.Forms.ListBox();
          this.btnSaveDoc = new System.Windows.Forms.Button();
          this.textBox1 = new System.Windows.Forms.TextBox();
          this.colorDialog3 = new System.Windows.Forms.ColorDialog();
          this.groupBox6 = new System.Windows.Forms.GroupBox();
          this.lblRightScripture = new System.Windows.Forms.Label();
          this.lblLeftScripture = new System.Windows.Forms.Label();
          this.radBtnRightDisplay = new System.Windows.Forms.RadioButton();
          this.radBtnLeftDisplay = new System.Windows.Forms.RadioButton();
          this.radBtnDisplay = new System.Windows.Forms.RadioButton();
          this.label6 = new System.Windows.Forms.Label();
          this.btnNewDoc = new System.Windows.Forms.Button();
          this.label5 = new System.Windows.Forms.Label();
          this.btnCleanText = new System.Windows.Forms.Button();
          this.MdiMenuStrip.SuspendLayout();
          this.pnlPreview.SuspendLayout();
          this.grpBoxPreview.SuspendLayout();
          this.grpBoxSubject.SuspendLayout();
          this.grpBoxSubItem.SuspendLayout();
          this.groupBox1.SuspendLayout();
          this.groupBox2.SuspendLayout();
          this.groupBox3.SuspendLayout();
          this.groupBox4.SuspendLayout();
          this.groupBox5.SuspendLayout();
          this.groupBox6.SuspendLayout();
          this.SuspendLayout();
          // 
          // MdiMenuStrip
          // 
          this.MdiMenuStrip.Font = new System.Drawing.Font("新細明體", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
          this.MdiMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.SettingsToolStripMenuItem,
            this.HelpToolStripMenuItem});
          this.MdiMenuStrip.Location = new System.Drawing.Point(0, 0);
          this.MdiMenuStrip.Name = "MdiMenuStrip";
          this.MdiMenuStrip.Padding = new System.Windows.Forms.Padding(7, 2, 0, 2);
          this.MdiMenuStrip.Size = new System.Drawing.Size(1236, 24);
          this.MdiMenuStrip.TabIndex = 21;
          // 
          // SettingsToolStripMenuItem
          // 
          this.SettingsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ResolutionToolStripMenuItem});
          this.SettingsToolStripMenuItem.Font = new System.Drawing.Font("新細明體", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
          this.SettingsToolStripMenuItem.Name = "SettingsToolStripMenuItem";
          this.SettingsToolStripMenuItem.Size = new System.Drawing.Size(67, 20);
          this.SettingsToolStripMenuItem.Text = "設定(&S)";
          // 
          // ResolutionToolStripMenuItem
          // 
          this.ResolutionToolStripMenuItem.Name = "ResolutionToolStripMenuItem";
          this.ResolutionToolStripMenuItem.Size = new System.Drawing.Size(153, 22);
          this.ResolutionToolStripMenuItem.Text = "進階設定(&R)";
          this.ResolutionToolStripMenuItem.Click += new System.EventHandler(this.ResolutionToolStripMenuItem_Click);
          // 
          // HelpToolStripMenuItem
          // 
          this.HelpToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.OnlineHelpToolStripMenuItem,
            this.toolStripSeparator1,
            this.AboutToolStripMenuItem1});
          this.HelpToolStripMenuItem.Font = new System.Drawing.Font("新細明體", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
          this.HelpToolStripMenuItem.Name = "HelpToolStripMenuItem";
          this.HelpToolStripMenuItem.Size = new System.Drawing.Size(69, 20);
          this.HelpToolStripMenuItem.Text = "說明(&H)";
          // 
          // OnlineHelpToolStripMenuItem
          // 
          this.OnlineHelpToolStripMenuItem.Name = "OnlineHelpToolStripMenuItem";
          this.OnlineHelpToolStripMenuItem.Size = new System.Drawing.Size(214, 22);
          this.OnlineHelpToolStripMenuItem.Text = "線上說明(&H)";
          this.OnlineHelpToolStripMenuItem.Click += new System.EventHandler(this.OnlineHelpToolStripMenuItem_Click);
          // 
          // toolStripSeparator1
          // 
          this.toolStripSeparator1.Name = "toolStripSeparator1";
          this.toolStripSeparator1.Size = new System.Drawing.Size(211, 6);
          // 
          // AboutToolStripMenuItem1
          // 
          this.AboutToolStripMenuItem1.Name = "AboutToolStripMenuItem1";
          this.AboutToolStripMenuItem1.Size = new System.Drawing.Size(214, 22);
          this.AboutToolStripMenuItem1.Text = "關於教會視訊系統(&A)";
          this.AboutToolStripMenuItem1.Click += new System.EventHandler(this.AboutToolStripMenuItem1_Click);
          // 
          // btnHide
          // 
          this.btnHide.Font = new System.Drawing.Font("新細明體", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
          this.btnHide.Location = new System.Drawing.Point(523, 438);
          this.btnHide.Name = "btnHide";
          this.btnHide.Size = new System.Drawing.Size(104, 30);
          this.btnHide.TabIndex = 49;
          this.btnHide.Text = "隱藏畫面";
          this.btnHide.UseVisualStyleBackColor = true;
          this.btnHide.Click += new System.EventHandler(this.btnHide_Click);
          // 
          // btnShow
          // 
          this.btnShow.Font = new System.Drawing.Font("新細明體", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
          this.btnShow.Location = new System.Drawing.Point(303, 438);
          this.btnShow.Name = "btnShow";
          this.btnShow.Size = new System.Drawing.Size(104, 30);
          this.btnShow.TabIndex = 48;
          this.btnShow.Text = "顯示畫面";
          this.btnShow.UseVisualStyleBackColor = true;
          this.btnShow.Click += new System.EventHandler(this.btnShow_Click);
          // 
          // comboBoxTitle1
          // 
          this.comboBoxTitle1.FlatStyle = System.Windows.Forms.FlatStyle.System;
          this.comboBoxTitle1.Font = new System.Drawing.Font("標楷體", 17.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
          this.comboBoxTitle1.FormattingEnabled = true;
          this.comboBoxTitle1.ImeMode = System.Windows.Forms.ImeMode.On;
          this.comboBoxTitle1.Items.AddRange(new object[] {
            "",
            "弟兄",
            "傳道",
            "執事",
            "長老",
            "神學生",
            "姊妹",
            "傳道娘",
            "執事娘",
            "長老娘"});
          this.comboBoxTitle1.Location = new System.Drawing.Point(118, 55);
          this.comboBoxTitle1.Name = "comboBoxTitle1";
          this.comboBoxTitle1.Size = new System.Drawing.Size(86, 31);
          this.comboBoxTitle1.TabIndex = 2;
          // 
          // pnlPreview
          // 
          this.pnlPreview.BackColor = System.Drawing.SystemColors.Control;
          this.pnlPreview.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
          this.pnlPreview.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
          this.pnlPreview.Controls.Add(this.lblVolChapSecPage);
          this.pnlPreview.Controls.Add(this.lblName3);
          this.pnlPreview.Controls.Add(this.lblName2);
          this.pnlPreview.Controls.Add(this.lblOldPoem);
          this.pnlPreview.Controls.Add(this.lblNewPoem);
          this.pnlPreview.Controls.Add(this.txtBoxSong2);
          this.pnlPreview.Controls.Add(this.txtBoxSong1);
          this.pnlPreview.Controls.Add(this.txtBoxName1);
          this.pnlPreview.Controls.Add(this.txtBoxSubject);
          this.pnlPreview.Controls.Add(this.lblPoem2);
          this.pnlPreview.Controls.Add(this.comboBoxTitle1);
          this.pnlPreview.Controls.Add(this.lblComma);
          this.pnlPreview.Controls.Add(this.lblPoem1);
          this.pnlPreview.Location = new System.Drawing.Point(6, 20);
          this.pnlPreview.Name = "pnlPreview";
          this.pnlPreview.Size = new System.Drawing.Size(400, 300);
          this.pnlPreview.TabIndex = 45;
          // 
          // lblVolChapSecPage
          // 
          this.lblVolChapSecPage.BackColor = System.Drawing.Color.Transparent;
          this.lblVolChapSecPage.Font = new System.Drawing.Font("標楷體", 17.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
          this.lblVolChapSecPage.Location = new System.Drawing.Point(4, 90);
          this.lblVolChapSecPage.Name = "lblVolChapSecPage";
          this.lblVolChapSecPage.Size = new System.Drawing.Size(388, 32);
          this.lblVolChapSecPage.TabIndex = 61;
          this.lblVolChapSecPage.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
          this.lblVolChapSecPage.Visible = false;
          // 
          // lblName3
          // 
          this.lblName3.BackColor = System.Drawing.Color.Transparent;
          this.lblName3.Font = new System.Drawing.Font("標楷體", 17.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
          this.lblName3.Location = new System.Drawing.Point(38, 220);
          this.lblName3.Name = "lblName3";
          this.lblName3.Size = new System.Drawing.Size(320, 32);
          this.lblName3.TabIndex = 64;
          this.lblName3.Tag = "";
          this.lblName3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
          this.lblName3.Visible = false;
          // 
          // lblName2
          // 
          this.lblName2.BackColor = System.Drawing.Color.Transparent;
          this.lblName2.Font = new System.Drawing.Font("標楷體", 17.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
          this.lblName2.Location = new System.Drawing.Point(38, 191);
          this.lblName2.Name = "lblName2";
          this.lblName2.Size = new System.Drawing.Size(320, 32);
          this.lblName2.TabIndex = 63;
          this.lblName2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
          this.lblName2.Visible = false;
          // 
          // lblOldPoem
          // 
          this.lblOldPoem.BackColor = System.Drawing.Color.Transparent;
          this.lblOldPoem.Font = new System.Drawing.Font("標楷體", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
          this.lblOldPoem.Location = new System.Drawing.Point(30, 150);
          this.lblOldPoem.Name = "lblOldPoem";
          this.lblOldPoem.Size = new System.Drawing.Size(340, 40);
          this.lblOldPoem.TabIndex = 62;
          this.lblOldPoem.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
          this.lblOldPoem.Visible = false;
          // 
          // lblNewPoem
          // 
          this.lblNewPoem.BackColor = System.Drawing.Color.Transparent;
          this.lblNewPoem.Font = new System.Drawing.Font("標楷體", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
          this.lblNewPoem.Location = new System.Drawing.Point(30, 110);
          this.lblNewPoem.Name = "lblNewPoem";
          this.lblNewPoem.Size = new System.Drawing.Size(340, 40);
          this.lblNewPoem.TabIndex = 61;
          this.lblNewPoem.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
          this.lblNewPoem.Visible = false;
          // 
          // txtBoxSong2
          // 
          this.txtBoxSong2.Font = new System.Drawing.Font("標楷體", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
          this.txtBoxSong2.ImeMode = System.Windows.Forms.ImeMode.On;
          this.txtBoxSong2.Location = new System.Drawing.Point(308, 54);
          this.txtBoxSong2.MaxLength = 3;
          this.txtBoxSong2.Name = "txtBoxSong2";
          this.txtBoxSong2.Size = new System.Drawing.Size(50, 33);
          this.txtBoxSong2.TabIndex = 4;
          this.txtBoxSong2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
          // 
          // txtBoxSong1
          // 
          this.txtBoxSong1.Font = new System.Drawing.Font("標楷體", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
          this.txtBoxSong1.ImeMode = System.Windows.Forms.ImeMode.On;
          this.txtBoxSong1.Location = new System.Drawing.Point(252, 54);
          this.txtBoxSong1.MaxLength = 3;
          this.txtBoxSong1.Name = "txtBoxSong1";
          this.txtBoxSong1.Size = new System.Drawing.Size(50, 33);
          this.txtBoxSong1.TabIndex = 3;
          this.txtBoxSong1.Tag = "";
          this.txtBoxSong1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
          // 
          // txtBoxName1
          // 
          this.txtBoxName1.Font = new System.Drawing.Font("標楷體", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
          this.txtBoxName1.ImeMode = System.Windows.Forms.ImeMode.On;
          this.txtBoxName1.Location = new System.Drawing.Point(8, 54);
          this.txtBoxName1.MaxLength = 6;
          this.txtBoxName1.Multiline = true;
          this.txtBoxName1.Name = "txtBoxName1";
          this.txtBoxName1.Size = new System.Drawing.Size(107, 32);
          this.txtBoxName1.TabIndex = 1;
          // 
          // txtBoxSubject
          // 
          this.txtBoxSubject.Cursor = System.Windows.Forms.Cursors.IBeam;
          this.txtBoxSubject.Font = new System.Drawing.Font("標楷體", 32.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
          this.txtBoxSubject.ImeMode = System.Windows.Forms.ImeMode.On;
          this.txtBoxSubject.Location = new System.Drawing.Point(0, -4);
          this.txtBoxSubject.MaxLength = 32;
          this.txtBoxSubject.Multiline = true;
          this.txtBoxSubject.Name = "txtBoxSubject";
          this.txtBoxSubject.Size = new System.Drawing.Size(396, 50);
          this.txtBoxSubject.TabIndex = 0;
          this.txtBoxSubject.Text = "請輸入講題";
          this.txtBoxSubject.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
          this.txtBoxSubject.TextChanged += new System.EventHandler(this.btnSendSubject_Click);
          this.txtBoxSubject.Click += new System.EventHandler(this.txtBoxSubject_Click);
          // 
          // lblPoem2
          // 
          this.lblPoem2.BackColor = System.Drawing.Color.Transparent;
          this.lblPoem2.Font = new System.Drawing.Font("標楷體", 17.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
          this.lblPoem2.Location = new System.Drawing.Point(354, 53);
          this.lblPoem2.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
          this.lblPoem2.Name = "lblPoem2";
          this.lblPoem2.Size = new System.Drawing.Size(32, 32);
          this.lblPoem2.TabIndex = 9;
          this.lblPoem2.Text = "首";
          this.lblPoem2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
          // 
          // lblComma
          // 
          this.lblComma.BackColor = System.Drawing.Color.Transparent;
          this.lblComma.Font = new System.Drawing.Font("標楷體", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
          this.lblComma.Location = new System.Drawing.Point(298, 53);
          this.lblComma.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
          this.lblComma.Name = "lblComma";
          this.lblComma.Size = new System.Drawing.Size(16, 32);
          this.lblComma.TabIndex = 8;
          this.lblComma.Text = "、";
          this.lblComma.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
          // 
          // lblPoem1
          // 
          this.lblPoem1.BackColor = System.Drawing.Color.Transparent;
          this.lblPoem1.Font = new System.Drawing.Font("標楷體", 17.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
          this.lblPoem1.Location = new System.Drawing.Point(224, 53);
          this.lblPoem1.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
          this.lblPoem1.Name = "lblPoem1";
          this.lblPoem1.Size = new System.Drawing.Size(32, 32);
          this.lblPoem1.TabIndex = 5;
          this.lblPoem1.Text = "詩";
          this.lblPoem1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
          // 
          // grpBoxPreview
          // 
          this.grpBoxPreview.Controls.Add(this.pnlPreview);
          this.grpBoxPreview.Font = new System.Drawing.Font("新細明體", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
          this.grpBoxPreview.Location = new System.Drawing.Point(303, 27);
          this.grpBoxPreview.Name = "grpBoxPreview";
          this.grpBoxPreview.Size = new System.Drawing.Size(412, 326);
          this.grpBoxPreview.TabIndex = 52;
          this.grpBoxPreview.TabStop = false;
          this.grpBoxPreview.Text = "編輯畫面";
          // 
          // fontDialog1
          // 
          this.fontDialog1.Font = new System.Drawing.Font("標楷體", 19.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
          // 
          // colorDialog1
          // 
          this.colorDialog1.AllowFullOpen = false;
          // 
          // btnFontOfSubject
          // 
          this.btnFontOfSubject.Font = new System.Drawing.Font("新細明體", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
          this.btnFontOfSubject.Location = new System.Drawing.Point(6, 27);
          this.btnFontOfSubject.Name = "btnFontOfSubject";
          this.btnFontOfSubject.Size = new System.Drawing.Size(62, 30);
          this.btnFontOfSubject.TabIndex = 53;
          this.btnFontOfSubject.Text = "字型";
          this.btnFontOfSubject.UseVisualStyleBackColor = true;
          this.btnFontOfSubject.Click += new System.EventHandler(this.btnFontOfSubject_Click);
          // 
          // btnColorOfSubject
          // 
          this.btnColorOfSubject.Font = new System.Drawing.Font("新細明體", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
          this.btnColorOfSubject.Location = new System.Drawing.Point(74, 27);
          this.btnColorOfSubject.Name = "btnColorOfSubject";
          this.btnColorOfSubject.Size = new System.Drawing.Size(62, 30);
          this.btnColorOfSubject.TabIndex = 53;
          this.btnColorOfSubject.Text = "顏色";
          this.btnColorOfSubject.UseVisualStyleBackColor = true;
          this.btnColorOfSubject.Click += new System.EventHandler(this.btnColorOfSubject_Click);
          // 
          // grpBoxSubject
          // 
          this.grpBoxSubject.Controls.Add(this.btnColorOfSubject);
          this.grpBoxSubject.Controls.Add(this.btnFontOfSubject);
          this.grpBoxSubject.Font = new System.Drawing.Font("新細明體", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
          this.grpBoxSubject.Location = new System.Drawing.Point(721, 27);
          this.grpBoxSubject.Name = "grpBoxSubject";
          this.grpBoxSubject.Size = new System.Drawing.Size(143, 62);
          this.grpBoxSubject.TabIndex = 55;
          this.grpBoxSubject.TabStop = false;
          this.grpBoxSubject.Text = "講題";
          // 
          // btnCallPPS
          // 
          this.btnCallPPS.Enabled = false;
          this.btnCallPPS.ForeColor = System.Drawing.SystemColors.ControlText;
          this.btnCallPPS.Location = new System.Drawing.Point(398, 28);
          this.btnCallPPS.Name = "btnCallPPS";
          this.btnCallPPS.Size = new System.Drawing.Size(94, 30);
          this.btnCallPPS.TabIndex = 63;
          this.btnCallPPS.Text = "顯示歌詞";
          this.btnCallPPS.UseVisualStyleBackColor = true;
          this.btnCallPPS.Click += new System.EventHandler(this.btnCallPPS_Click);
          // 
          // grpBoxSubItem
          // 
          this.grpBoxSubItem.BackColor = System.Drawing.SystemColors.Control;
          this.grpBoxSubItem.Controls.Add(this.btnHideName1Song1);
          this.grpBoxSubItem.Controls.Add(this.chkBoxTranslator);
          this.grpBoxSubItem.Controls.Add(this.btnColorOfTitle);
          this.grpBoxSubItem.Controls.Add(this.btnSendName1Song1);
          this.grpBoxSubItem.Controls.Add(this.comboBoxTitle2);
          this.grpBoxSubItem.Controls.Add(this.txtBoxName2);
          this.grpBoxSubItem.Font = new System.Drawing.Font("新細明體", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
          this.grpBoxSubItem.ForeColor = System.Drawing.SystemColors.MenuHighlight;
          this.grpBoxSubItem.Location = new System.Drawing.Point(721, 95);
          this.grpBoxSubItem.Name = "grpBoxSubItem";
          this.grpBoxSubItem.Size = new System.Drawing.Size(498, 62);
          this.grpBoxSubItem.TabIndex = 55;
          this.grpBoxSubItem.TabStop = false;
          this.grpBoxSubItem.Text = "主領、唱詩、翻譯";
          // 
          // btnHideName1Song1
          // 
          this.btnHideName1Song1.ForeColor = System.Drawing.SystemColors.ControlText;
          this.btnHideName1Song1.Location = new System.Drawing.Point(142, 27);
          this.btnHideName1Song1.Name = "btnHideName1Song1";
          this.btnHideName1Song1.Size = new System.Drawing.Size(62, 30);
          this.btnHideName1Song1.TabIndex = 63;
          this.btnHideName1Song1.Text = "隱藏";
          this.btnHideName1Song1.UseVisualStyleBackColor = true;
          this.btnHideName1Song1.Click += new System.EventHandler(this.btnHideName1Song1_Click);
          // 
          // chkBoxTranslator
          // 
          this.chkBoxTranslator.AutoSize = true;
          this.chkBoxTranslator.Font = new System.Drawing.Font("新細明體", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
          this.chkBoxTranslator.ForeColor = System.Drawing.SystemColors.MenuHighlight;
          this.chkBoxTranslator.Location = new System.Drawing.Point(210, 25);
          this.chkBoxTranslator.Name = "chkBoxTranslator";
          this.chkBoxTranslator.Size = new System.Drawing.Size(120, 19);
          this.chkBoxTranslator.TabIndex = 62;
          this.chkBoxTranslator.Text = "顯示翻譯人員:";
          this.chkBoxTranslator.UseVisualStyleBackColor = true;
          // 
          // btnColorOfTitle
          // 
          this.btnColorOfTitle.Font = new System.Drawing.Font("新細明體", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
          this.btnColorOfTitle.ForeColor = System.Drawing.SystemColors.ControlText;
          this.btnColorOfTitle.Location = new System.Drawing.Point(74, 27);
          this.btnColorOfTitle.Name = "btnColorOfTitle";
          this.btnColorOfTitle.Size = new System.Drawing.Size(62, 30);
          this.btnColorOfTitle.TabIndex = 53;
          this.btnColorOfTitle.Text = "顏色";
          this.btnColorOfTitle.UseVisualStyleBackColor = true;
          this.btnColorOfTitle.Click += new System.EventHandler(this.btnColorOfTitle_Click);
          // 
          // btnSendName1Song1
          // 
          this.btnSendName1Song1.Font = new System.Drawing.Font("新細明體", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
          this.btnSendName1Song1.Location = new System.Drawing.Point(6, 27);
          this.btnSendName1Song1.Name = "btnSendName1Song1";
          this.btnSendName1Song1.Size = new System.Drawing.Size(62, 30);
          this.btnSendName1Song1.TabIndex = 61;
          this.btnSendName1Song1.Text = "送出";
          this.btnSendName1Song1.UseVisualStyleBackColor = true;
          this.btnSendName1Song1.Click += new System.EventHandler(this.btnSendName1Song1_Click);
          // 
          // comboBoxTitle2
          // 
          this.comboBoxTitle2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
          this.comboBoxTitle2.FlatStyle = System.Windows.Forms.FlatStyle.System;
          this.comboBoxTitle2.Font = new System.Drawing.Font("標楷體", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
          this.comboBoxTitle2.FormattingEnabled = true;
          this.comboBoxTitle2.ImeMode = System.Windows.Forms.ImeMode.On;
          this.comboBoxTitle2.Items.AddRange(new object[] {
            "",
            "弟兄",
            "姊妹"});
          this.comboBoxTitle2.Location = new System.Drawing.Point(425, 24);
          this.comboBoxTitle2.Name = "comboBoxTitle2";
          this.comboBoxTitle2.Size = new System.Drawing.Size(67, 27);
          this.comboBoxTitle2.TabIndex = 4;
          // 
          // txtBoxName2
          // 
          this.txtBoxName2.Font = new System.Drawing.Font("新細明體", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
          this.txtBoxName2.ImeMode = System.Windows.Forms.ImeMode.On;
          this.txtBoxName2.Location = new System.Drawing.Point(330, 23);
          this.txtBoxName2.MaxLength = 4;
          this.txtBoxName2.Multiline = true;
          this.txtBoxName2.Name = "txtBoxName2";
          this.txtBoxName2.Size = new System.Drawing.Size(89, 32);
          this.txtBoxName2.TabIndex = 3;
          // 
          // btnColorOfOther
          // 
          this.btnColorOfOther.Font = new System.Drawing.Font("新細明體", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
          this.btnColorOfOther.ForeColor = System.Drawing.SystemColors.ControlText;
          this.btnColorOfOther.Location = new System.Drawing.Point(331, 64);
          this.btnColorOfOther.Name = "btnColorOfOther";
          this.btnColorOfOther.Size = new System.Drawing.Size(61, 30);
          this.btnColorOfOther.TabIndex = 60;
          this.btnColorOfOther.Text = "顏色";
          this.btnColorOfOther.UseVisualStyleBackColor = true;
          this.btnColorOfOther.Click += new System.EventHandler(this.btnColorOfOthers_Click);
          // 
          // btnFontOfOther
          // 
          this.btnFontOfOther.Font = new System.Drawing.Font("新細明體", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
          this.btnFontOfOther.ForeColor = System.Drawing.SystemColors.ControlText;
          this.btnFontOfOther.Location = new System.Drawing.Point(399, 64);
          this.btnFontOfOther.Name = "btnFontOfOther";
          this.btnFontOfOther.Size = new System.Drawing.Size(93, 30);
          this.btnFontOfOther.TabIndex = 59;
          this.btnFontOfOther.Text = "經文字型";
          this.btnFontOfOther.UseVisualStyleBackColor = true;
          this.btnFontOfOther.Click += new System.EventHandler(this.btnFontOfOthers_Click);
          // 
          // groupBox1
          // 
          this.groupBox1.Controls.Add(this.chkBoxToTextBox1);
          this.groupBox1.Controls.Add(this.btnLast);
          this.groupBox1.Controls.Add(this.txtBoxQuery);
          this.groupBox1.Controls.Add(this.btnNext);
          this.groupBox1.Controls.Add(this.chkBoxScripture);
          this.groupBox1.Font = new System.Drawing.Font("新細明體", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
          this.groupBox1.Location = new System.Drawing.Point(303, 359);
          this.groupBox1.Name = "groupBox1";
          this.groupBox1.Size = new System.Drawing.Size(412, 62);
          this.groupBox1.TabIndex = 56;
          this.groupBox1.TabStop = false;
          this.groupBox1.Text = "經文查詢";
          // 
          // chkBoxToTextBox1
          // 
          this.chkBoxToTextBox1.AutoSize = true;
          this.chkBoxToTextBox1.Location = new System.Drawing.Point(299, 36);
          this.chkBoxToTextBox1.Name = "chkBoxToTextBox1";
          this.chkBoxToTextBox1.Size = new System.Drawing.Size(101, 19);
          this.chkBoxToTextBox1.TabIndex = 63;
          this.chkBoxToTextBox1.Text = "複製到聽打";
          this.chkBoxToTextBox1.UseVisualStyleBackColor = true;
          this.chkBoxToTextBox1.CheckedChanged += new System.EventHandler(this.chkScripture_Click);
          // 
          // btnLast
          // 
          this.btnLast.Enabled = false;
          this.btnLast.Font = new System.Drawing.Font("新細明體", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
          this.btnLast.Location = new System.Drawing.Point(135, 25);
          this.btnLast.Name = "btnLast";
          this.btnLast.Size = new System.Drawing.Size(77, 30);
          this.btnLast.TabIndex = 62;
          this.btnLast.Text = "上一節";
          this.btnLast.UseVisualStyleBackColor = true;
          this.btnLast.Click += new System.EventHandler(this.btnLast_Click);
          // 
          // txtBoxQuery
          // 
          this.txtBoxQuery.Font = new System.Drawing.Font("Times New Roman", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
          this.txtBoxQuery.ImeMode = System.Windows.Forms.ImeMode.On;
          this.txtBoxQuery.Location = new System.Drawing.Point(8, 23);
          this.txtBoxQuery.MaxLength = 10;
          this.txtBoxQuery.Name = "txtBoxQuery";
          this.txtBoxQuery.Size = new System.Drawing.Size(115, 32);
          this.txtBoxQuery.TabIndex = 61;
          this.txtBoxQuery.Text = "xx xx xx";
          this.txtBoxQuery.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
          this.toolTip1.SetToolTip(this.txtBoxQuery, "輸入格式為:xx xx xx");
          this.txtBoxQuery.Enter += new System.EventHandler(this.txtBoxQuery_Click);
          // 
          // btnNext
          // 
          this.btnNext.Enabled = false;
          this.btnNext.Font = new System.Drawing.Font("新細明體", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
          this.btnNext.Location = new System.Drawing.Point(218, 25);
          this.btnNext.Name = "btnNext";
          this.btnNext.Size = new System.Drawing.Size(77, 30);
          this.btnNext.TabIndex = 60;
          this.btnNext.Text = "下一節";
          this.btnNext.UseVisualStyleBackColor = true;
          this.btnNext.Click += new System.EventHandler(this.btnNext_Click);
          // 
          // chkBoxScripture
          // 
          this.chkBoxScripture.AutoSize = true;
          this.chkBoxScripture.Font = new System.Drawing.Font("新細明體", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
          this.chkBoxScripture.Location = new System.Drawing.Point(299, 15);
          this.chkBoxScripture.Name = "chkBoxScripture";
          this.chkBoxScripture.Size = new System.Drawing.Size(86, 19);
          this.chkBoxScripture.TabIndex = 59;
          this.chkBoxScripture.Text = "多節顯示";
          this.chkBoxScripture.UseVisualStyleBackColor = true;
          this.chkBoxScripture.CheckedChanged += new System.EventHandler(this.chkScripture_Click);
          // 
          // groupBox2
          // 
          this.groupBox2.Controls.Add(this.label4);
          this.groupBox2.Controls.Add(this.btnCallPPS);
          this.groupBox2.Controls.Add(this.btnColorOfOther);
          this.groupBox2.Controls.Add(this.comboBox1);
          this.groupBox2.Controls.Add(this.label3);
          this.groupBox2.Controls.Add(this.txtBoxName4);
          this.groupBox2.Controls.Add(this.btnFontOfOther);
          this.groupBox2.Controls.Add(this.comboBoxTitle4);
          this.groupBox2.Controls.Add(this.btnSendName2Song2);
          this.groupBox2.Controls.Add(this.txtBoxSong3);
          this.groupBox2.Controls.Add(this.label2);
          this.groupBox2.Controls.Add(this.label1);
          this.groupBox2.Controls.Add(this.txtBoxName3);
          this.groupBox2.Controls.Add(this.comboBoxTitle3);
          this.groupBox2.Font = new System.Drawing.Font("新細明體", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
          this.groupBox2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
          this.groupBox2.Location = new System.Drawing.Point(721, 163);
          this.groupBox2.Name = "groupBox2";
          this.groupBox2.Size = new System.Drawing.Size(498, 141);
          this.groupBox2.TabIndex = 57;
          this.groupBox2.TabStop = false;
          this.groupBox2.Text = "讚美詩、領詩";
          // 
          // label4
          // 
          this.label4.ForeColor = System.Drawing.SystemColors.ControlText;
          this.label4.Location = new System.Drawing.Point(327, 106);
          this.label4.Name = "label4";
          this.label4.Size = new System.Drawing.Size(164, 21);
          this.label4.TabIndex = 62;
          this.label4.Text = "◎經文 && 文字同此顏色";
          // 
          // comboBox1
          // 
          this.comboBox1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
          this.comboBox1.Enabled = false;
          this.comboBox1.FlatStyle = System.Windows.Forms.FlatStyle.System;
          this.comboBox1.Font = new System.Drawing.Font("標楷體", 17.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
          this.comboBox1.FormattingEnabled = true;
          this.comboBox1.ImeMode = System.Windows.Forms.ImeMode.On;
          this.comboBox1.Items.AddRange(new object[] {
            "",
            "甲",
            "乙"});
          this.comboBox1.Location = new System.Drawing.Point(224, 28);
          this.comboBox1.Name = "comboBox1";
          this.comboBox1.Size = new System.Drawing.Size(56, 31);
          this.comboBox1.TabIndex = 66;
          // 
          // label3
          // 
          this.label3.AutoSize = true;
          this.label3.Location = new System.Drawing.Point(6, 109);
          this.label3.Name = "label3";
          this.label3.Size = new System.Drawing.Size(71, 15);
          this.label3.TabIndex = 65;
          this.label3.Text = "司琴人員:";
          // 
          // txtBoxName4
          // 
          this.txtBoxName4.Font = new System.Drawing.Font("新細明體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
          this.txtBoxName4.ImeMode = System.Windows.Forms.ImeMode.On;
          this.txtBoxName4.Location = new System.Drawing.Point(102, 103);
          this.txtBoxName4.MaxLength = 4;
          this.txtBoxName4.Multiline = true;
          this.txtBoxName4.Name = "txtBoxName4";
          this.txtBoxName4.Size = new System.Drawing.Size(107, 32);
          this.txtBoxName4.TabIndex = 63;
          // 
          // comboBoxTitle4
          // 
          this.comboBoxTitle4.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
          this.comboBoxTitle4.FlatStyle = System.Windows.Forms.FlatStyle.System;
          this.comboBoxTitle4.Font = new System.Drawing.Font("標楷體", 17.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
          this.comboBoxTitle4.FormattingEnabled = true;
          this.comboBoxTitle4.ImeMode = System.Windows.Forms.ImeMode.On;
          this.comboBoxTitle4.Items.AddRange(new object[] {
            "",
            "弟兄",
            "姊妹"});
          this.comboBoxTitle4.Location = new System.Drawing.Point(224, 103);
          this.comboBoxTitle4.Name = "comboBoxTitle4";
          this.comboBoxTitle4.Size = new System.Drawing.Size(86, 31);
          this.comboBoxTitle4.TabIndex = 64;
          // 
          // btnSendName2Song2
          // 
          this.btnSendName2Song2.Font = new System.Drawing.Font("新細明體", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
          this.btnSendName2Song2.Location = new System.Drawing.Point(330, 28);
          this.btnSendName2Song2.Name = "btnSendName2Song2";
          this.btnSendName2Song2.Size = new System.Drawing.Size(62, 30);
          this.btnSendName2Song2.TabIndex = 62;
          this.btnSendName2Song2.Text = "送出";
          this.btnSendName2Song2.UseVisualStyleBackColor = true;
          this.btnSendName2Song2.Click += new System.EventHandler(this.btnSendName3Song2_Click);
          // 
          // txtBoxSong3
          // 
          this.txtBoxSong3.Font = new System.Drawing.Font("Times New Roman", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
          this.txtBoxSong3.ImeMode = System.Windows.Forms.ImeMode.On;
          this.txtBoxSong3.Location = new System.Drawing.Point(139, 28);
          this.txtBoxSong3.MaxLength = 3;
          this.txtBoxSong3.Name = "txtBoxSong3";
          this.txtBoxSong3.Size = new System.Drawing.Size(70, 32);
          this.txtBoxSong3.TabIndex = 6;
          this.txtBoxSong3.Tag = "";
          this.txtBoxSong3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
          this.txtBoxSong3.TextChanged += new System.EventHandler(this.txtBoxSong3_TextChanged);
          // 
          // label2
          // 
          this.label2.AutoSize = true;
          this.label2.Location = new System.Drawing.Point(8, 34);
          this.label2.Name = "label2";
          this.label2.Size = new System.Drawing.Size(81, 15);
          this.label2.TabIndex = 5;
          this.label2.Text = "讚美詩(新):";
          // 
          // label1
          // 
          this.label1.AutoSize = true;
          this.label1.Location = new System.Drawing.Point(6, 72);
          this.label1.Name = "label1";
          this.label1.Size = new System.Drawing.Size(71, 15);
          this.label1.TabIndex = 5;
          this.label1.Text = "領詩人員:";
          // 
          // txtBoxName3
          // 
          this.txtBoxName3.Font = new System.Drawing.Font("新細明體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
          this.txtBoxName3.ImeMode = System.Windows.Forms.ImeMode.On;
          this.txtBoxName3.Location = new System.Drawing.Point(102, 66);
          this.txtBoxName3.MaxLength = 4;
          this.txtBoxName3.Multiline = true;
          this.txtBoxName3.Name = "txtBoxName3";
          this.txtBoxName3.Size = new System.Drawing.Size(107, 32);
          this.txtBoxName3.TabIndex = 3;
          // 
          // comboBoxTitle3
          // 
          this.comboBoxTitle3.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
          this.comboBoxTitle3.FlatStyle = System.Windows.Forms.FlatStyle.System;
          this.comboBoxTitle3.Font = new System.Drawing.Font("標楷體", 17.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
          this.comboBoxTitle3.FormattingEnabled = true;
          this.comboBoxTitle3.ImeMode = System.Windows.Forms.ImeMode.On;
          this.comboBoxTitle3.Items.AddRange(new object[] {
            "",
            "弟兄",
            "姊妹"});
          this.comboBoxTitle3.Location = new System.Drawing.Point(224, 66);
          this.comboBoxTitle3.Name = "comboBoxTitle3";
          this.comboBoxTitle3.Size = new System.Drawing.Size(86, 31);
          this.comboBoxTitle3.TabIndex = 4;
          // 
          // btnClear
          // 
          this.btnClear.Font = new System.Drawing.Font("新細明體", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
          this.btnClear.Location = new System.Drawing.Point(413, 438);
          this.btnClear.Name = "btnClear";
          this.btnClear.Size = new System.Drawing.Size(104, 30);
          this.btnClear.TabIndex = 58;
          this.btnClear.Text = "清除畫面";
          this.btnClear.UseVisualStyleBackColor = true;
          this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
          // 
          // fontDialog2
          // 
          this.fontDialog2.Font = new System.Drawing.Font("標楷體", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
          // 
          // colorDialog2
          // 
          this.colorDialog2.AllowFullOpen = false;
          // 
          // btnBrowse
          // 
          this.btnBrowse.Font = new System.Drawing.Font("新細明體", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
          this.btnBrowse.Location = new System.Drawing.Point(74, 26);
          this.btnBrowse.Name = "btnBrowse";
          this.btnBrowse.Size = new System.Drawing.Size(90, 30);
          this.btnBrowse.TabIndex = 59;
          this.btnBrowse.Text = "瀏覽圖片...";
          this.btnBrowse.UseVisualStyleBackColor = true;
          this.btnBrowse.Click += new System.EventHandler(this.btnBrowse_Click);
          // 
          // openFileDialog1
          // 
          this.openFileDialog1.FileName = "Background-01.bmp";
          this.openFileDialog1.Filter = "點陣圖檔案(*.bmp)|*.bmp";
          this.openFileDialog1.Title = "背景圖片選取";
          // 
          // groupBox3
          // 
          this.groupBox3.Controls.Add(this.btnSendBackground);
          this.groupBox3.Controls.Add(this.btnBrowse);
          this.groupBox3.Font = new System.Drawing.Font("新細明體", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
          this.groupBox3.Location = new System.Drawing.Point(870, 27);
          this.groupBox3.Name = "groupBox3";
          this.groupBox3.Size = new System.Drawing.Size(170, 62);
          this.groupBox3.TabIndex = 60;
          this.groupBox3.TabStop = false;
          this.groupBox3.Text = "背景圖片";
          // 
          // btnSendBackground
          // 
          this.btnSendBackground.Location = new System.Drawing.Point(6, 26);
          this.btnSendBackground.Name = "btnSendBackground";
          this.btnSendBackground.Size = new System.Drawing.Size(62, 30);
          this.btnSendBackground.TabIndex = 60;
          this.btnSendBackground.Text = "送出";
          this.btnSendBackground.UseVisualStyleBackColor = true;
          this.btnSendBackground.Click += new System.EventHandler(this.btnSendBackground_Click);
          // 
          // toolTip1
          // 
          this.toolTip1.AutoPopDelay = 2000;
          this.toolTip1.InitialDelay = 500;
          this.toolTip1.ReshowDelay = 100;
          // 
          // groupBox4
          // 
          this.groupBox4.Controls.Add(this.listBox2);
          this.groupBox4.Controls.Add(this.listBox1);
          this.groupBox4.Font = new System.Drawing.Font("新細明體", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
          this.groupBox4.Location = new System.Drawing.Point(12, 27);
          this.groupBox4.Name = "groupBox4";
          this.groupBox4.Size = new System.Drawing.Size(279, 652);
          this.groupBox4.TabIndex = 0;
          this.groupBox4.TabStop = false;
          this.groupBox4.Text = "經卷名稱";
          // 
          // listBox2
          // 
          this.listBox2.Font = new System.Drawing.Font("新細明體", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
          this.listBox2.FormattingEnabled = true;
          this.listBox2.ItemHeight = 15;
          this.listBox2.Items.AddRange(new object[] {
            "40馬太福音",
            "41馬可福音",
            "42路加福音",
            "43約翰福音",
            "44使徒行傳",
            "45羅馬書",
            "46哥林多前書",
            "47哥林多後書",
            "48加拉太書",
            "49以弗所書",
            "50腓立比書",
            "51歌羅西書",
            "52帖撒羅尼迦前書",
            "53帖撒羅尼迦後書",
            "54提摩太前書",
            "55提摩太後書",
            "56提多書",
            "57腓利門書(1)",
            "58希伯來書",
            "59雅各書",
            "60彼得前書",
            "61彼得後書",
            "62約翰一書",
            "63約翰二書(1)",
            "64約翰三書(1)",
            "65猶大書",
            "66啟示錄"});
          this.listBox2.Location = new System.Drawing.Point(138, 27);
          this.listBox2.Name = "listBox2";
          this.listBox2.SelectionMode = System.Windows.Forms.SelectionMode.None;
          this.listBox2.Size = new System.Drawing.Size(135, 439);
          this.listBox2.TabIndex = 3;
          // 
          // listBox1
          // 
          this.listBox1.Font = new System.Drawing.Font("新細明體", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
          this.listBox1.FormattingEnabled = true;
          this.listBox1.ImeMode = System.Windows.Forms.ImeMode.On;
          this.listBox1.ItemHeight = 15;
          this.listBox1.Items.AddRange(new object[] {
            "1創世記",
            "2出埃及記",
            "3利未記",
            "4民數記",
            "5申命記",
            "6約書亞記",
            "7士師記",
            "8路得記",
            "9撒母耳記上",
            "10撒母耳記下",
            "11列王紀上",
            "12列王紀下",
            "13歷代志上",
            "14歷代志下",
            "15以斯拉記",
            "16尼希米記",
            "17以斯帖記",
            "18約伯記",
            "19詩篇",
            "20箴言",
            "21傳道書",
            "22雅歌",
            "23以賽亞書",
            "24耶利米書",
            "25耶利米哀歌",
            "26以西結書",
            "27但以理書",
            "28何西阿書",
            "29約珥書",
            "30阿摩司書",
            "31俄巴底亞書(1)",
            "32約拿書",
            "33彌迦書",
            "34那鴻書",
            "35哈巴谷書",
            "36西番雅書",
            "37哈該書",
            "38撒迦利亞書",
            "39瑪拉基書"});
          this.listBox1.Location = new System.Drawing.Point(6, 27);
          this.listBox1.Name = "listBox1";
          this.listBox1.SelectionMode = System.Windows.Forms.SelectionMode.None;
          this.listBox1.Size = new System.Drawing.Size(126, 619);
          this.listBox1.TabIndex = 0;
          // 
          // groupBox5
          // 
          this.groupBox5.Controls.Add(this.btnEditText);
          this.groupBox5.Controls.Add(this.btnReloadText);
          this.groupBox5.Controls.Add(this.btnSendLoadedText);
          this.groupBox5.Controls.Add(this.listBox6);
          this.groupBox5.Font = new System.Drawing.Font("新細明體", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
          this.groupBox5.Location = new System.Drawing.Point(303, 483);
          this.groupBox5.Name = "groupBox5";
          this.groupBox5.Size = new System.Drawing.Size(412, 196);
          this.groupBox5.TabIndex = 61;
          this.groupBox5.TabStop = false;
          this.groupBox5.Text = "文字";
          // 
          // btnEditText
          // 
          this.btnEditText.Font = new System.Drawing.Font("新細明體", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
          this.btnEditText.Location = new System.Drawing.Point(220, 158);
          this.btnEditText.Name = "btnEditText";
          this.btnEditText.Size = new System.Drawing.Size(80, 30);
          this.btnEditText.TabIndex = 64;
          this.btnEditText.Text = "編輯文字";
          this.btnEditText.UseVisualStyleBackColor = true;
          this.btnEditText.Click += new System.EventHandler(this.btnEditText_Click);
          // 
          // btnReloadText
          // 
          this.btnReloadText.Font = new System.Drawing.Font("新細明體", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
          this.btnReloadText.Location = new System.Drawing.Point(66, 158);
          this.btnReloadText.Name = "btnReloadText";
          this.btnReloadText.Size = new System.Drawing.Size(80, 30);
          this.btnReloadText.TabIndex = 64;
          this.btnReloadText.Text = "重新載入";
          this.btnReloadText.UseVisualStyleBackColor = true;
          this.btnReloadText.Click += new System.EventHandler(this.btnReloadText_Click);
          // 
          // btnSendLoadedText
          // 
          this.btnSendLoadedText.Font = new System.Drawing.Font("新細明體", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
          this.btnSendLoadedText.Location = new System.Drawing.Point(152, 158);
          this.btnSendLoadedText.Name = "btnSendLoadedText";
          this.btnSendLoadedText.Size = new System.Drawing.Size(62, 30);
          this.btnSendLoadedText.TabIndex = 64;
          this.btnSendLoadedText.Text = "送出";
          this.btnSendLoadedText.UseVisualStyleBackColor = true;
          this.btnSendLoadedText.Click += new System.EventHandler(this.btnSendLoadedText_Click);
          // 
          // listBox6
          // 
          this.listBox6.Font = new System.Drawing.Font("標楷體", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
          this.listBox6.FormattingEnabled = true;
          this.listBox6.ImeMode = System.Windows.Forms.ImeMode.On;
          this.listBox6.ItemHeight = 20;
          this.listBox6.Location = new System.Drawing.Point(6, 28);
          this.listBox6.Name = "listBox6";
          this.listBox6.SelectionMode = System.Windows.Forms.SelectionMode.MultiSimple;
          this.listBox6.Size = new System.Drawing.Size(400, 124);
          this.listBox6.TabIndex = 62;
          // 
          // btnSaveDoc
          // 
          this.btnSaveDoc.Location = new System.Drawing.Point(419, 329);
          this.btnSaveDoc.Name = "btnSaveDoc";
          this.btnSaveDoc.Size = new System.Drawing.Size(62, 30);
          this.btnSaveDoc.TabIndex = 65;
          this.btnSaveDoc.Text = "存檔";
          this.btnSaveDoc.UseVisualStyleBackColor = true;
          this.btnSaveDoc.Click += new System.EventHandler(this.btnSaveDoc_Click);
          // 
          // textBox1
          // 
          this.textBox1.Font = new System.Drawing.Font("標楷體", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
          this.textBox1.ImeMode = System.Windows.Forms.ImeMode.On;
          this.textBox1.Location = new System.Drawing.Point(12, 48);
          this.textBox1.Multiline = true;
          this.textBox1.Name = "textBox1";
          this.textBox1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
          this.textBox1.Size = new System.Drawing.Size(473, 275);
          this.textBox1.TabIndex = 63;
          this.textBox1.TextChanged += new System.EventHandler(this.btnSendText_Click);
          this.textBox1.Click += new System.EventHandler(this.btnSendText_Click);
          this.textBox1.KeyUp += new System.Windows.Forms.KeyEventHandler(this.btnSendText_Click);
          // 
          // colorDialog3
          // 
          this.colorDialog3.AllowFullOpen = false;
          // 
          // groupBox6
          // 
          this.groupBox6.Controls.Add(this.btnCleanText);
          this.groupBox6.Controls.Add(this.lblRightScripture);
          this.groupBox6.Controls.Add(this.lblLeftScripture);
          this.groupBox6.Controls.Add(this.radBtnRightDisplay);
          this.groupBox6.Controls.Add(this.radBtnLeftDisplay);
          this.groupBox6.Controls.Add(this.radBtnDisplay);
          this.groupBox6.Controls.Add(this.label6);
          this.groupBox6.Controls.Add(this.btnNewDoc);
          this.groupBox6.Controls.Add(this.label5);
          this.groupBox6.Controls.Add(this.textBox1);
          this.groupBox6.Controls.Add(this.btnSaveDoc);
          this.groupBox6.Location = new System.Drawing.Point(721, 311);
          this.groupBox6.Name = "groupBox6";
          this.groupBox6.Size = new System.Drawing.Size(498, 368);
          this.groupBox6.TabIndex = 66;
          this.groupBox6.TabStop = false;
          this.groupBox6.Text = "聽打";
          // 
          // lblRightScripture
          // 
          this.lblRightScripture.Font = new System.Drawing.Font("新細明體", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
          this.lblRightScripture.Location = new System.Drawing.Point(255, 60);
          this.lblRightScripture.Name = "lblRightScripture";
          this.lblRightScripture.Size = new System.Drawing.Size(230, 100);
          this.lblRightScripture.TabIndex = 73;
          this.lblRightScripture.Visible = false;
          // 
          // lblLeftScripture
          // 
          this.lblLeftScripture.Font = new System.Drawing.Font("新細明體", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
          this.lblLeftScripture.Location = new System.Drawing.Point(17, 60);
          this.lblLeftScripture.Name = "lblLeftScripture";
          this.lblLeftScripture.Size = new System.Drawing.Size(230, 100);
          this.lblLeftScripture.TabIndex = 72;
          this.lblLeftScripture.Visible = false;
          // 
          // radBtnRightDisplay
          // 
          this.radBtnRightDisplay.AutoSize = true;
          this.radBtnRightDisplay.Location = new System.Drawing.Point(282, 23);
          this.radBtnRightDisplay.Name = "radBtnRightDisplay";
          this.radBtnRightDisplay.Size = new System.Drawing.Size(74, 20);
          this.radBtnRightDisplay.TabIndex = 71;
          this.radBtnRightDisplay.TabStop = true;
          this.radBtnRightDisplay.Text = "右投射";
          this.radBtnRightDisplay.UseVisualStyleBackColor = true;
          this.radBtnRightDisplay.CheckedChanged += new System.EventHandler(this.showDisplay_Click);
          // 
          // radBtnLeftDisplay
          // 
          this.radBtnLeftDisplay.AutoSize = true;
          this.radBtnLeftDisplay.Location = new System.Drawing.Point(175, 23);
          this.radBtnLeftDisplay.Name = "radBtnLeftDisplay";
          this.radBtnLeftDisplay.Size = new System.Drawing.Size(74, 20);
          this.radBtnLeftDisplay.TabIndex = 70;
          this.radBtnLeftDisplay.TabStop = true;
          this.radBtnLeftDisplay.Text = "左投射";
          this.radBtnLeftDisplay.UseVisualStyleBackColor = true;
          this.radBtnLeftDisplay.CheckedChanged += new System.EventHandler(this.showDisplay_Click);
          // 
          // radBtnDisplay
          // 
          this.radBtnDisplay.AutoSize = true;
          this.radBtnDisplay.Location = new System.Drawing.Point(101, 23);
          this.radBtnDisplay.Name = "radBtnDisplay";
          this.radBtnDisplay.Size = new System.Drawing.Size(42, 20);
          this.radBtnDisplay.TabIndex = 69;
          this.radBtnDisplay.TabStop = true;
          this.radBtnDisplay.Text = "無";
          this.radBtnDisplay.UseVisualStyleBackColor = true;
          this.radBtnDisplay.CheckedChanged += new System.EventHandler(this.showDisplay_Click);
          // 
          // label6
          // 
          this.label6.AutoSize = true;
          this.label6.Location = new System.Drawing.Point(17, 23);
          this.label6.Name = "label6";
          this.label6.Size = new System.Drawing.Size(72, 16);
          this.label6.TabIndex = 68;
          this.label6.Text = "聽打預覽";
          // 
          // btnNewDoc
          // 
          this.btnNewDoc.Location = new System.Drawing.Point(351, 330);
          this.btnNewDoc.Name = "btnNewDoc";
          this.btnNewDoc.Size = new System.Drawing.Size(62, 30);
          this.btnNewDoc.TabIndex = 67;
          this.btnNewDoc.Text = "新檔";
          this.btnNewDoc.UseVisualStyleBackColor = true;
          this.btnNewDoc.Click += new System.EventHandler(this.btnNewDoc_Click);
          // 
          // label5
          // 
          this.label5.AutoSize = true;
          this.label5.Font = new System.Drawing.Font("新細明體", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
          this.label5.Location = new System.Drawing.Point(9, 336);
          this.label5.Name = "label5";
          this.label5.Size = new System.Drawing.Size(59, 13);
          this.label5.TabIndex = 66;
          this.label5.Text = "檔案：無";
          // 
          // btnCleanText
          // 
          this.btnCleanText.Location = new System.Drawing.Point(398, 15);
          this.btnCleanText.Name = "btnCleanText";
          this.btnCleanText.Size = new System.Drawing.Size(87, 30);
          this.btnCleanText.TabIndex = 74;
          this.btnCleanText.Text = "清空聽打";
          this.btnCleanText.UseVisualStyleBackColor = true;
          this.btnCleanText.Click += new System.EventHandler(this.btnCleanText_Click);
          // 
          // MainFrame
          // 
          this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
          this.ClientSize = new System.Drawing.Size(1236, 688);
          this.Controls.Add(this.groupBox6);
          this.Controls.Add(this.groupBox5);
          this.Controls.Add(this.groupBox4);
          this.Controls.Add(this.groupBox3);
          this.Controls.Add(this.btnClear);
          this.Controls.Add(this.grpBoxSubItem);
          this.Controls.Add(this.grpBoxSubject);
          this.Controls.Add(this.groupBox2);
          this.Controls.Add(this.groupBox1);
          this.Controls.Add(this.grpBoxPreview);
          this.Controls.Add(this.btnHide);
          this.Controls.Add(this.btnShow);
          this.Controls.Add(this.MdiMenuStrip);
          this.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
          this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
          this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
          this.MainMenuStrip = this.MdiMenuStrip;
          this.MaximizeBox = false;
          this.Name = "MainFrame";
          this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
          this.Text = "教會視訊系統 V1.0";
          this.Load += new System.EventHandler(this.MainFrame_Load);
          this.Paint += new System.Windows.Forms.PaintEventHandler(this.MainFrame_Paint);
          this.Shown += new System.EventHandler(this.MainFrame_Shown);
          this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainFrame_FormClosing);
          this.MdiMenuStrip.ResumeLayout(false);
          this.MdiMenuStrip.PerformLayout();
          this.pnlPreview.ResumeLayout(false);
          this.pnlPreview.PerformLayout();
          this.grpBoxPreview.ResumeLayout(false);
          this.grpBoxSubject.ResumeLayout(false);
          this.grpBoxSubItem.ResumeLayout(false);
          this.grpBoxSubItem.PerformLayout();
          this.groupBox1.ResumeLayout(false);
          this.groupBox1.PerformLayout();
          this.groupBox2.ResumeLayout(false);
          this.groupBox2.PerformLayout();
          this.groupBox3.ResumeLayout(false);
          this.groupBox4.ResumeLayout(false);
          this.groupBox5.ResumeLayout(false);
          this.groupBox6.ResumeLayout(false);
          this.groupBox6.PerformLayout();
          this.ResumeLayout(false);
          this.PerformLayout();

      }

      #endregion

      private System.Windows.Forms.MenuStrip MdiMenuStrip;
      private System.Windows.Forms.ToolStripMenuItem SettingsToolStripMenuItem;
      private System.Windows.Forms.ToolStripMenuItem ResolutionToolStripMenuItem;
      private System.Windows.Forms.ToolStripMenuItem HelpToolStripMenuItem;
      private System.Windows.Forms.ToolStripMenuItem OnlineHelpToolStripMenuItem;
      private System.Windows.Forms.ToolStripMenuItem AboutToolStripMenuItem1;
      private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
      private System.Windows.Forms.Button btnHide;
      private System.Windows.Forms.Button btnShow;
      private System.Windows.Forms.GroupBox grpBoxPreview;
      private System.Windows.Forms.FontDialog fontDialog1;
      private System.Windows.Forms.Button btnFontOfSubject;
      private System.Windows.Forms.Button btnColorOfSubject;
      private System.Windows.Forms.GroupBox grpBoxSubject;
      private System.Windows.Forms.GroupBox grpBoxSubItem;
      private System.Windows.Forms.GroupBox groupBox1;
      private System.Windows.Forms.GroupBox groupBox2;
      private System.Windows.Forms.Button btnClear;
      private System.Windows.Forms.CheckBox chkBoxScripture;
      private System.Windows.Forms.Button btnNext;
      private System.Windows.Forms.Button btnFontOfOther;
      private System.Windows.Forms.Button btnColorOfOther;
      private System.Windows.Forms.FontDialog fontDialog2;
      private System.Windows.Forms.Button btnSendName1Song1;
      private System.Windows.Forms.Button btnBrowse;
      private System.Windows.Forms.OpenFileDialog openFileDialog1;
      private System.Windows.Forms.GroupBox groupBox3;
      private System.Windows.Forms.Button btnSendBackground;
      public System.Windows.Forms.Panel pnlPreview;
      public System.Windows.Forms.ComboBox comboBoxTitle1;
      public System.Windows.Forms.Label lblPoem2;
      public System.Windows.Forms.Label lblComma;
      public System.Windows.Forms.Label lblPoem1;
      private System.Windows.Forms.Button btnCallPPS;
      public System.Windows.Forms.TextBox txtBoxSubject;
      public System.Windows.Forms.TextBox txtBoxName1;
      public System.Windows.Forms.TextBox txtBoxSong1;
      public System.Windows.Forms.TextBox txtBoxSong2;
      public System.Windows.Forms.TextBox txtBoxName3;
      public System.Windows.Forms.ComboBox comboBoxTitle3;
      private System.Windows.Forms.Label label2;
      private System.Windows.Forms.Label label1;
      public System.Windows.Forms.TextBox txtBoxSong3;
      private System.Windows.Forms.Button btnSendName2Song2;
      private System.Windows.Forms.Label label3;
      public System.Windows.Forms.TextBox txtBoxName4;
      public System.Windows.Forms.ComboBox comboBoxTitle4;
      public System.Windows.Forms.Label lblNewPoem;
      public System.Windows.Forms.Label lblOldPoem;
      public System.Windows.Forms.Label lblName2;
      private System.Windows.Forms.ComboBox comboBox1;
      public System.Windows.Forms.TextBox txtBoxQuery;
      private System.Windows.Forms.ToolTip toolTip1;
      private System.Windows.Forms.GroupBox groupBox4;
      private System.Windows.Forms.ListBox listBox1;
      private System.Windows.Forms.ListBox listBox2;
      public System.Windows.Forms.Label lblVolChapSecPage;
      private System.Windows.Forms.GroupBox groupBox5;
      private System.Windows.Forms.TextBox textBox1;
      private System.Windows.Forms.ListBox listBox6;
      private System.Windows.Forms.Button btnSendLoadedText;
      private System.Windows.Forms.Button btnLast;
      private System.Windows.Forms.Button btnEditText;
      private System.Windows.Forms.Button btnReloadText;
      public System.Windows.Forms.Label lblName3;
      private System.Windows.Forms.Button btnColorOfTitle;
      public System.Windows.Forms.ColorDialog colorDialog1;
      public System.Windows.Forms.ColorDialog colorDialog2;
      public System.Windows.Forms.ColorDialog colorDialog3;
      public System.Windows.Forms.ComboBox comboBoxTitle2;
      public System.Windows.Forms.TextBox txtBoxName2;
      private System.Windows.Forms.CheckBox chkBoxTranslator;
      private System.Windows.Forms.Label label4;
      private System.Windows.Forms.Button btnHideName1Song1;
      private System.Windows.Forms.CheckBox chkBoxToTextBox1;
      private System.Windows.Forms.Button btnSaveDoc;
      private System.Windows.Forms.GroupBox groupBox6;
      private System.Windows.Forms.Label label5;
      private System.Windows.Forms.Button btnNewDoc;
      private System.Windows.Forms.RadioButton radBtnRightDisplay;
      private System.Windows.Forms.RadioButton radBtnLeftDisplay;
      private System.Windows.Forms.RadioButton radBtnDisplay;
      private System.Windows.Forms.Label label6;
      private System.Windows.Forms.Label lblLeftScripture;
      private System.Windows.Forms.Label lblRightScripture;
      private System.Windows.Forms.Button btnCleanText;
   }
}

