﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Data.OleDb;
using System.IO;
using System.Threading;
using Microsoft.VisualBasic.FileIO;
using System.Diagnostics;
using Microsoft.VisualBasic.Devices;    //For access to MS Access
using Microsoft.VisualBasic;    //For 半形轉全形

namespace Projector
{
   public partial class MainFrame : Form
   {
      private OleDbConnection oleConn;
      private OleDbCommand Cmd = new OleDbCommand();
      private Register myReg = new Register();
      private DisplayForm1024 L_displayForm;
      private DisplayForm1024 R_displayForm;
      private string[] praise = {  "3", "517", "103", "495", "107", "512", "138", "447",
                                  "178", "505", "197", "489", "255", "513", "274乙", "487乙",
                                  "296乙", "361", "308", "488", "313", "456", "314", "459",
                                  "324", "462", "331", "463", "335", "464", "337", "501",
                                  "344", "465", "348", "502", "349", "475", "350", "503",
                                  "351", "466", "354", "467", "358", "468", "361", "504",
                                  "366", "469", "368", "470", "369", "471", "372", "472",
                                  "375", "474", "382", "476", "384", "478", "385", "506",
                                  "393", "479", "397", "480", "404", "481", "408", "524",
                                  "409", "486", "410", "527", "413", "493", "414", "494",
                                  "417", "498", "422", "508", "425", "509", "428", "510",
                                  "432", "499", "435", "491", "437", "500", "440", "514",
                                  "441", "516", "443", "518", "447", "526", "451", "519",
                                  "456", "197", "457", "348", "458", "350", "459", "385",
                                  "460", "404", "461", "425", "462", "428", "463", "441",
                                  "464", "443", "465", "451", "466", "490", "467", "497",
                                  "468", "515" };
      private bool boolName1Song1 = true;

      public MainFrame()
      {
         SetStyle(ControlStyles.SupportsTransparentBackColor, true);
         InitializeComponent();
         InitializeMyComponent();
         ShareMem.Factor = (float)ShareMem.SizeOfClient.Width / (float)400;
      }

      public Label lblScripture;
      public Label lblScripture1;
      private void InitializeMyComponent()
      {
         Register reg = new Register();
         ShareMem.preDisplayMode = ShareMem.DisplayMode.None;
         ShareMem.curDisplayMode = ShareMem.DisplayMode.None;
         this.txtBoxSubject.ImeMode = System.Windows.Forms.ImeMode.OnHalf;
         this.txtBoxSubject.Cursor = System.Windows.Forms.Cursors.IBeam;
         this.txtBoxName1.ImeMode = System.Windows.Forms.ImeMode.OnHalf;
         this.txtBoxName1.Cursor = System.Windows.Forms.Cursors.IBeam;
         this.txtBoxSong1.ImeMode = System.Windows.Forms.ImeMode.OnHalf;
         this.txtBoxSong1.Cursor = System.Windows.Forms.Cursors.IBeam;
         this.txtBoxSong2.ImeMode = System.Windows.Forms.ImeMode.OnHalf;
         this.txtBoxSong2.Cursor = System.Windows.Forms.Cursors.IBeam;
         this.txtBoxSong3.ImeMode = System.Windows.Forms.ImeMode.OnHalf;
         this.txtBoxSong3.Cursor = System.Windows.Forms.Cursors.IBeam;
         this.txtBoxName2.ImeMode = System.Windows.Forms.ImeMode.OnHalf;
         this.txtBoxName2.Cursor = System.Windows.Forms.Cursors.IBeam;
         this.txtBoxName3.ImeMode = System.Windows.Forms.ImeMode.OnHalf;
         this.txtBoxName3.Cursor = System.Windows.Forms.Cursors.IBeam;
         this.txtBoxName4.ImeMode = System.Windows.Forms.ImeMode.OnHalf;
         this.txtBoxName4.Cursor = System.Windows.Forms.Cursors.IBeam;
         this.txtBoxQuery.ImeMode = System.Windows.Forms.ImeMode.OnHalf;
         this.txtBoxQuery.Cursor = System.Windows.Forms.Cursors.IBeam;
         this.comboBoxTitle1.ImeMode = System.Windows.Forms.ImeMode.OnHalf;
         this.comboBox1.ImeMode = System.Windows.Forms.ImeMode.OnHalf;
         this.comboBoxTitle3.ImeMode = System.Windows.Forms.ImeMode.OnHalf;
         this.comboBoxTitle4.ImeMode = System.Windows.Forms.ImeMode.OnHalf;
         this.listBox1.ImeMode = System.Windows.Forms.ImeMode.OnHalf;
         this.listBox2.ImeMode = System.Windows.Forms.ImeMode.OnHalf;
         this.listBox6.ImeMode = System.Windows.Forms.ImeMode.OnHalf;
         this.textBox1.ImeMode = System.Windows.Forms.ImeMode.OnHalf;
         this.textBox1.Cursor = System.Windows.Forms.Cursors.IBeam;

         //Subject-講題
         fontDialog1.Font = new Font(reg.GetFontNameOfSubject(),
                                     reg.GetFontSizeOfSubject(),
                                     (FontStyle)reg.GetFontStyleOfSubject(),
                                     System.Drawing.GraphicsUnit.Point,
                                     ((byte)(136)));
         txtBoxSubject.Font = new Font(fontDialog1.Font.Name,
                                    fontDialog1.Font.Size,
                                    fontDialog1.Font.Style,
                                    System.Drawing.GraphicsUnit.Point,
                                    ((byte)(136)));
         colorDialog1.Color = Color.FromArgb(reg.GetColorOfSubject());
         btnColorOfSubject.ForeColor = colorDialog1.Color;
         if (ShareMem.subjectX != reg.GetSubjectX())
             ShareMem.subjectX = reg.GetSubjectX();
         if (ShareMem.subjectY != reg.GetSubjectY())
             ShareMem.subjectY = reg.GetSubjectY();
         if (ShareMem.subjectWidth != reg.GetSubjectWidth())
             ShareMem.subjectWidth = reg.GetSubjectWidth();
         if (ShareMem.subjectHeight != reg.GetSubjectHeight())
             ShareMem.subjectHeight = reg.GetSubjectHeight();
         //Others-其他
         colorDialog2.Color = Color.FromArgb(reg.GetColorOfTitle());
         fontDialog2.Font = new Font(reg.GetFontNameOfOthers(),
                                     reg.GetFontSizeOfOthers(),
                                     (FontStyle)reg.GetFontStyleOfOthers(),
                                     System.Drawing.GraphicsUnit.Point,
                                     ((byte)(136)));
         txtBoxName1.Font = new Font(fontDialog2.Font.Name,
                                     fontDialog2.Font.Size,
                                     fontDialog2.Font.Style,
                                     System.Drawing.GraphicsUnit.Point,
                                     ((byte)(136)));
         lblPoem1.ForeColor = colorDialog2.Color;
         lblComma.ForeColor = colorDialog2.Color;
         lblPoem2.ForeColor = colorDialog2.Color;
         btnColorOfTitle.ForeColor = colorDialog2.Color;
         // 
         // lblScripture(左投射)
         // 
         colorDialog3.Color = Color.FromArgb(reg.GetColorOfOthers());
         this.lblScripture = new System.Windows.Forms.Label();
         this.lblScripture.BackColor = System.Drawing.Color.Transparent;
         this.lblScripture.ForeColor = colorDialog3.Color;
         //this.lblScripture.Font = new System.Drawing.Font("標楷體", 17.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
         this.lblScripture.Font = new System.Drawing.Font("標楷體", (float) reg.GetFontSizeOfOthers(), System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
         //this.lblScripture.Location = new System.Drawing.Point(2, 127);
         if (ShareMem.currentX != reg.GetCurrentX()) 
             ShareMem.currentX = reg.GetCurrentX();
         if (ShareMem.currentY != reg.GetCurrentY()) 
             ShareMem.currentY = reg.GetCurrentY();
         this.lblScripture.Location = new System.Drawing.Point(ShareMem.currentX, ShareMem.currentY);
         this.lblScripture.Name = "lblScripture";
         //this.lblScripture.Size = new System.Drawing.Size(396, 160);
         if (ShareMem.contentWidth != reg.GetContentWidth()) 
             ShareMem.contentWidth = reg.GetContentWidth();
         if (ShareMem.contentHeight != reg.GetContentHeight()) 
             ShareMem.contentHeight = reg.GetContentHeight();
         this.lblScripture.Size = new System.Drawing.Size(ShareMem.contentWidth, ShareMem.contentHeight);
         if (ShareMem.contentLines != reg.GetContentLines())
             ShareMem.contentLines = reg.GetContentLines();
         if (ShareMem.contentLength != reg.GetContentLength())
             ShareMem.contentLength = reg.GetContentLength();
         this.lblScripture.TabIndex = 38;
         this.lblScripture.Text = "";//12節  這是在測試每行最多可顯示的字元數";
         this.lblScripture.Visible = false;
         this.pnlPreview.Controls.Add(this.lblScripture);
         // 
         // lblScripture1(右投射)
         // 
         colorDialog3.Color = Color.FromArgb(reg.GetColorOfOthers());
         this.lblScripture1 = new System.Windows.Forms.Label();
         this.lblScripture1.BackColor = System.Drawing.Color.Transparent;
         this.lblScripture1.ForeColor = colorDialog3.Color;
         //this.lblScripture1.Font = new System.Drawing.Font("標楷體", 17.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
         this.lblScripture1.Font = new System.Drawing.Font("標楷體", (float)reg.GetFontSizeOfOthers(), System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
         //this.lblScripture1.Location = new System.Drawing.Point(2, 127);
         if (ShareMem.currentX1 != reg.GetCurrentX1())
             ShareMem.currentX1 = reg.GetCurrentX1();
         if (ShareMem.currentY1 != reg.GetCurrentY1())
             ShareMem.currentY1 = reg.GetCurrentY1();
         this.lblScripture1.Location = new System.Drawing.Point(ShareMem.currentX1, ShareMem.currentY1);
         this.lblScripture1.Name = "lblScripture1";
         //this.lblScripture1.Size = new System.Drawing.Size(396, 160);
         if (ShareMem.contentWidth1 != reg.GetContentWidth1())
             ShareMem.contentWidth1 = reg.GetContentWidth1();
         if (ShareMem.contentHeight1 != reg.GetContentHeight1())
             ShareMem.contentHeight1 = reg.GetContentHeight1();
         this.lblScripture1.Size = new System.Drawing.Size(ShareMem.contentWidth1, ShareMem.contentHeight1);
         if (ShareMem.contentLines1 != reg.GetContentLines1())
             ShareMem.contentLines1 = reg.GetContentLines1();
         if (ShareMem.contentLength1 != reg.GetContentLength1())
             ShareMem.contentLength1 = reg.GetContentLength1();
         this.lblScripture1.TabIndex = 38;
         this.lblScripture1.Text = "";//12節  這是在測試每行最多可顯示的字元數";
         this.lblScripture1.Visible = false;
         this.pnlPreview.Controls.Add(this.lblScripture1);
         //txtBoxName2
         txtBoxName2.Font = txtBoxName1.Font;
         //txtBoxName3
         txtBoxName3.Font = txtBoxName1.Font;
         //txtBoxName4
         txtBoxName4.Font = txtBoxName1.Font;
         //lblVolChapSecPage 
         lblVolChapSecPage.ForeColor = colorDialog3.Color;
         //lblNewPoem 
         lblNewPoem.ForeColor = colorDialog3.Color;
         //lblOldPoem 
         lblOldPoem.ForeColor = colorDialog3.Color;
         //lblName2 
         lblName2.ForeColor = colorDialog3.Color;
         //lblName3 
         lblName3.ForeColor = colorDialog3.Color;
         btnColorOfOther.ForeColor = colorDialog3.Color;
         //Background
         openFileDialog1.FileName = reg.GetPathOfBackgroundImage();
         if (!File.Exists(openFileDialog1.FileName)) openFileDialog1.FileName = Application.StartupPath + "\\Resources\\Background-01.bmp";
         try
         {
            pnlPreview.BackgroundImage = new Bitmap(openFileDialog1.FileName);
         }
         catch (Exception ex)
         {
            throw ex;
         }
         if (reg.GetCheckToTextBox1() && reg.GetCheckScripture())
             reg.SetCheckScripture(false);
         chkBoxScripture.Checked = reg.GetCheckScripture();
         chkBoxToTextBox1.Checked = reg.GetCheckToTextBox1();
         if (ShareMem.memo != reg.GetMemo())
             ShareMem.memo = reg.GetMemo();
         if (ShareMem.memo1 != reg.GetMemo1())
             ShareMem.memo1 = reg.GetMemo1();
         radBtnLeftDisplay.Text = ShareMem.memo;
         radBtnRightDisplay.Text = ShareMem.memo1;
         showDisplay();
         btnNewDoc.Enabled = false;
         btnHide.Enabled = false;
      }

      private void MainFrame_Load(object sender, EventArgs e)
      {
         ShareMem.oMainFrmAlias = this;
         L_displayForm = new DisplayForm1024("L"); //左投射
         R_displayForm = new DisplayForm1024("R"); //右投射
         try
         {
            string strConn = null;

            //使用OleDbConnectionStringBuilder類別來建立與管理連線字串
            OleDbConnectionStringBuilder builder = new OleDbConnectionStringBuilder();
            builder.Provider = "Microsoft.Jet.OLEDB.4.0";
            builder.DataSource = Application.StartupPath + "\\bibleXP.mdb";
            strConn = builder.ConnectionString;

            oleConn = new OleDbConnection(strConn);
            oleConn.Open();
         }
         catch (Exception ex)
         {
            MessageBox.Show(ex.Message);
         }
      }

      //聽打預覽
      private void showDisplay()
      {
          if (String.Compare(ShareMem.curDisplayLocation, "LR", true) == 0)
          {
              radBtnLeftDisplay.Checked = true;
              radBtnLeftDisplay.Enabled = true;
              radBtnRightDisplay.Enabled = true;
          }
          else if (String.Compare(ShareMem.curDisplayLocation, "L", true) == 0)
          {
              radBtnLeftDisplay.Checked = true;
              radBtnLeftDisplay.Enabled = true;
              radBtnRightDisplay.Enabled = false;
          }
          else if (String.Compare(ShareMem.curDisplayLocation, "R", true) == 0)
          {
              radBtnRightDisplay.Checked = true;
              radBtnRightDisplay.Enabled = true;
              radBtnLeftDisplay.Enabled = false;
          }
          else if (String.Compare(ShareMem.curDisplayLocation, "", true) == 0)
          {
              radBtnDisplay.Checked = true;
              radBtnLeftDisplay.Enabled = false;
              radBtnRightDisplay.Enabled = false;
          }
      }

      private void showDisplay_Click(object sender, EventArgs e)
      {
          if (radBtnDisplay.Checked == true)
          {
              this.lblScripture.Hide();
              this.lblScripture1.Hide();
          }
          if (radBtnLeftDisplay.Checked == true)
          {
              this.lblScripture.Show();
              this.lblScripture.Location = new System.Drawing.Point(ShareMem.currentX, ShareMem.currentY);
              this.lblScripture.Size = new System.Drawing.Size(ShareMem.contentWidth, ShareMem.contentHeight);
              this.lblScripture.Text = lblLeftScripture.Text;
              this.lblScripture1.Hide();
          }
          if (radBtnRightDisplay.Checked == true)
          {
              this.lblScripture1.Show();
              this.lblScripture1.Location = new System.Drawing.Point(ShareMem.currentX1, ShareMem.currentY1);
              this.lblScripture1.Size = new System.Drawing.Size(ShareMem.contentWidth1, ShareMem.contentHeight1);
              this.lblScripture1.Text = lblRightScripture.Text;
              this.lblScripture.Hide();
          }
      }

      private void MainFrame_Shown(object sender, EventArgs e)
      {
         comboBoxTitle1.SelectedIndex = 0;
         try
         {
            string myText = Application.StartupPath + "\\視訊系統文字.txt";
            if (!File.Exists(myText))
            {
               Computer myComputer = new Computer();

               using (StreamWriter sw = myComputer.FileSystem.OpenTextFileWriter(myText, false, Encoding.Default))
               {
                  sw.WriteLine("奉主耶穌聖名禱告");
                  sw.WriteLine("哈利路亞讚美主耶穌");
                  sw.WriteLine("阿們");
                  sw.Close();
               }
            }

            using (StreamReader myStreamReader = new StreamReader(myText, Encoding.Default))
            {
               string strReadLine = myStreamReader.ReadLine();
               while (strReadLine != null)
               {
                  listBox6.Items.Add(strReadLine);
                  strReadLine = myStreamReader.ReadLine();
               }
               myStreamReader.Close();
            }
         }
         catch (Exception ex)
         {
            MessageBox.Show(ex.Message);
         }
      }

      private void MainFrame_Paint(object sender, PaintEventArgs e)
      {
         DrawRectangle(colorDialog2.Color, new Point(6, 52), new Size(384, 35));
      }

      private void DrawRectangle(Color color, Point pt, Size size)
      {
         //Draw a rectangle
         //Sets g to a graphics object
         Graphics g = this.pnlPreview.CreateGraphics();
         //Create pen
         Pen p = new Pen(color, 1);
         //Draw outer rectangle to form
         g.DrawRectangle(p, pt.X, pt.Y, size.Width, size.Height);
         //Destroy a object
         g.Dispose();
         p.Dispose();
      }

      private void ResolutionToolStripMenuItem_Click(object sender, EventArgs e)
      {
         using (ResolutionDlg resolutionDlg = new ResolutionDlg())
         {
            resolutionDlg.ShowDialog();
         }

         //套用
         this.lblScripture.Location = new System.Drawing.Point(ShareMem.currentX, ShareMem.currentY);
         this.lblScripture.Size = new System.Drawing.Size(ShareMem.contentWidth, ShareMem.contentHeight);
         L_displayForm.SetScriptureLocation("L");
         L_displayForm.SetScriptureSize("L");
         L_displayForm.SetSubjectLocation("L");
         R_displayForm.SetScriptureLocation("R");
         R_displayForm.SetScriptureSize("R");
         R_displayForm.SetSubjectLocation("R");
         radBtnLeftDisplay.Text = ShareMem.memo;
         radBtnRightDisplay.Text = ShareMem.memo1;
         showDisplay();
      }

      private bool bDisplayFormShown = false;
      private void btnShow_Click(object sender, EventArgs e)
      {
         //確認投射方向
         if (String.Compare(ShareMem.curDisplayLocation, "R", true) == 0 || String.Compare(ShareMem.curDisplayLocation, "LR", true) == 0)
         {
             R_displayForm.Location = new Point(ShareMem.SizeOfHost.Width, 0);
             R_displayForm.Size = ShareMem.SizeOfClient;
             R_displayForm.BackgroundImage = pnlPreview.BackgroundImage;
             R_displayForm.Show();
         }
         if (String.Compare(ShareMem.curDisplayLocation, "L", true) == 0 || String.Compare(ShareMem.curDisplayLocation, "LR", true) == 0)
         {
             L_displayForm.Location = new Point(0 - ShareMem.SizeOfClient.Width, 0);
             L_displayForm.Size = ShareMem.SizeOfClient;
             L_displayForm.BackgroundImage = pnlPreview.BackgroundImage;
             L_displayForm.Show();
         }
         if (String.Compare(ShareMem.curDisplayLocation, "", true) == 0)
         {
             MessageBox.Show("請選取投射方向!!");
         }

         bDisplayFormShown = true;
         btnShow.Enabled = false;
         btnHide.Enabled = true;
      }

      private void btnHide_Click(object sender, EventArgs e)
      {
         L_displayForm.Hide();
         R_displayForm.Hide();
         bDisplayFormShown = false;
         btnShow.Enabled = true;
         btnHide.Enabled = false;
      }

      private void btnFontOfSubject_Click(object sender, EventArgs e)
      {
         if (fontDialog1.ShowDialog() == DialogResult.OK)
         {
            txtBoxSubject.Font = new Font(fontDialog1.Font.Name,
                                       fontDialog1.Font.Size,
                                       fontDialog1.Font.Style,
                                       System.Drawing.GraphicsUnit.Point,
                                       ((byte)(136)));
            L_displayForm.SetFontOfSubject(txtBoxSubject.Font);
            R_displayForm.SetFontOfSubject(txtBoxSubject.Font);
         }
         this.Invalidate();
      }

      private void btnColorOfSubject_Click(object sender, EventArgs e)
      {
         if (colorDialog1.ShowDialog() == DialogResult.OK)
         {
            btnColorOfSubject.ForeColor = colorDialog1.Color;
            L_displayForm.SetColorOfSuject(colorDialog1.Color);
            R_displayForm.SetColorOfSuject(colorDialog1.Color);
         }
      }

      //講題
      private void btnSendSubject_Click(object sender, EventArgs e)
      {
         L_displayForm.SetTextOfSuject(txtBoxSubject.Text);
         R_displayForm.SetTextOfSuject(txtBoxSubject.Text);
      }

      private void btnBrowse_Click(object sender, EventArgs e)
      {
         openFileDialog1.InitialDirectory = Application.StartupPath + "\\Resources";
         if (openFileDialog1.ShowDialog() == DialogResult.OK)
         {
            pnlPreview.BackgroundImage = new Bitmap(openFileDialog1.FileName);
         }
         Thread.Sleep(10);
      }

      private void btnSendBackground_Click(object sender, EventArgs e)
      {
         if (!bDisplayFormShown) return;
         L_displayForm.BackgroundImage = pnlPreview.BackgroundImage;
         R_displayForm.BackgroundImage = pnlPreview.BackgroundImage;
      }

      private void btnFontOfOthers_Click(object sender, EventArgs e)
      {
         if (fontDialog2.ShowDialog() == DialogResult.OK)
         {
            lblScripture.Font = new Font(fontDialog2.Font.Name,
                                    fontDialog2.Font.Size,
                                    fontDialog2.Font.Style,
                                    System.Drawing.GraphicsUnit.Point,
                                    ((byte)(136)));
            textBox1.Font = new Font(fontDialog2.Font.Name,
                                    fontDialog2.Font.Size,
                                    fontDialog2.Font.Style,
                                    System.Drawing.GraphicsUnit.Point,
                                    ((byte)(136)));
            L_displayForm.SetFontOfOthers(lblScripture.Font);
            R_displayForm.SetFontOfOthers(lblScripture.Font);
         }
         this.Invalidate();
      }

      private void btnColorOfTitle_Click(object sender, EventArgs e)
      {
         if (colorDialog2.ShowDialog() == DialogResult.OK)
         {
            lblPoem1.ForeColor = colorDialog2.Color;
            lblComma.ForeColor = colorDialog2.Color;
            lblPoem2.ForeColor = colorDialog2.Color;
            btnColorOfTitle.ForeColor = colorDialog2.Color;

            L_displayForm.SetColorOfTitle(colorDialog2.Color);
            R_displayForm.SetColorOfTitle(colorDialog2.Color);
         }
         this.Invalidate();
      }

      private void btnColorOfOthers_Click(object sender, EventArgs e)
      {
         if (colorDialog3.ShowDialog() == DialogResult.OK)
         {
            lblVolChapSecPage.ForeColor = colorDialog3.Color;
            lblScripture.ForeColor = colorDialog3.Color;

            lblNewPoem.ForeColor = colorDialog3.Color;
            lblOldPoem.ForeColor = colorDialog3.Color;
            lblName2.ForeColor = colorDialog3.Color;
            lblName3.ForeColor = colorDialog3.Color;
            btnColorOfOther.ForeColor = colorDialog3.Color;

            L_displayForm.SetColorOfOthers(colorDialog3.Color);
            R_displayForm.SetColorOfOthers(colorDialog3.Color);
         }
         this.Invalidate();
      }

      //主領、唱詩、翻譯
      private void btnSendName1Song1_Click(object sender, EventArgs e)
      {
         L_displayForm.SetColorOfTitle(colorDialog2.Color);
         R_displayForm.SetColorOfTitle(colorDialog2.Color);
         string strSongsTranslator = "";
         if (chkBoxTranslator.Checked)
         {
            strSongsTranslator = "翻譯：";
            strSongsTranslator += txtBoxName2.Text + " " + comboBoxTitle2.Text;
         }
         else
         {
            strSongsTranslator = "詩 ";
            if (txtBoxSong1.Text.Length > 0)
               strSongsTranslator += string.Format("{0, 3:G}", int.Parse(txtBoxSong1.Text));
            else
               strSongsTranslator += string.Format("{0, 3:G}", "");
            strSongsTranslator += "、";
            if (txtBoxSong2.Text.Length > 0)
               strSongsTranslator += string.Format("{0, 3:G}", int.Parse(txtBoxSong2.Text));
            else
               strSongsTranslator += string.Format("{0, 3:G}", "");
            strSongsTranslator += " 首";
         }
         L_displayForm.SetTextOfSpeakerSongsTranslator(txtBoxName1.Text + " " + comboBoxTitle1.Text,
                                                     strSongsTranslator);
         R_displayForm.SetTextOfSpeakerSongsTranslator(txtBoxName1.Text + " " + comboBoxTitle1.Text,
                                                     strSongsTranslator);
      }

      //讚美詩、領詩
      private void btnSendName3Song2_Click(object sender, EventArgs e)
      {
         L_displayForm.SetColorOfOthers(colorDialog3.Color);
         R_displayForm.SetColorOfOthers(colorDialog3.Color);
#if _NEW
         lblNewPoem.Text = "";
         lblOldPoem.Text = "讚美詩 " + txtBoxSong3.Text + comboBox1.SelectedItem + " 首";
#else
         lblNewPoem.Text = "新讚美詩 " + txtBoxSong3.Text + comboBox1.SelectedItem + " 首";
         string strOldPoem = txtBoxSong3.Text + comboBox1.SelectedItem;
         for (int i = 0; i < (praise.Length / 2); i++)
         {
            if (strOldPoem.Equals(praise[2 * i]))
            {
               strOldPoem = praise[2 * i + 1];
               break;
            }
         }
         lblOldPoem.Text = "舊讚美詩 " + strOldPoem + " 首";
#endif
         if (txtBoxName3.Text.Length != 0)
         {
            lblName2.Text = "領詩：" + txtBoxName3.Text + " " + comboBoxTitle3.Text;
            if (txtBoxName4.Text.Length != 0)
               lblName3.Text = "司琴：" + txtBoxName4.Text + " " + comboBoxTitle4.Text;
            else
               lblName3.Text = "";
         }
         else
         {
            if (txtBoxName4.Text.Length != 0)
            {
               lblName2.Text = "司琴：" + txtBoxName4.Text + " " + comboBoxTitle4.Text;
               lblName3.Text = "";
            }
            else
            {
               lblName2.Text = "";
               lblName3.Text = "";
            }
         }

         L_displayForm.SetTextOfPoemName2(lblNewPoem.Text, lblOldPoem.Text, lblName2.Text, lblName3.Text);
         R_displayForm.SetTextOfPoemName2(lblNewPoem.Text, lblOldPoem.Text, lblName2.Text, lblName3.Text);

         ShareMem.curDisplayMode = ShareMem.DisplayMode.Poem;
         if (ShareMem.preDisplayMode != ShareMem.curDisplayMode)
         {
            ClearProc(ShareMem.preDisplayMode);
            L_displayForm.ClearProc(ShareMem.preDisplayMode);
            R_displayForm.ClearProc(ShareMem.preDisplayMode);
            SwitchProc();
            L_displayForm.SwitchProc();
            R_displayForm.SwitchProc();
            ShareMem.preDisplayMode = ShareMem.curDisplayMode;
         }
         if (!btnCallPPS.Enabled) btnCallPPS.Enabled = true;
      }

      private void txtBoxSong3_TextChanged(object sender, EventArgs e)
      {
         if (txtBoxSong3.Text.Equals("51") || txtBoxSong3.Text.Equals("124") ||
             txtBoxSong3.Text.Equals("132") || txtBoxSong3.Text.Equals("274") ||
             txtBoxSong3.Text.Equals("296"))
         {
            comboBox1.Enabled = true;
            comboBox1.SelectedIndex = 1;
         }
         else
         {
            comboBox1.Enabled = false;
            comboBox1.SelectedIndex = 0;
         }
      }

      protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
      {
         const int WM_KEYDOWN = 0x100;
         const int WM_SYSKEYDOWN = 0x104;
         if (msg.Msg == WM_KEYDOWN || msg.Msg == WM_SYSKEYDOWN)
         {
            switch (keyData)
            {
               case Keys.Enter:
                  if (txtBoxSubject.Focused || txtBoxName1.Focused ||
                      txtBoxName2.Focused || txtBoxName3.Focused || txtBoxName4.Focused)
                     return true;
                  else if (txtBoxQuery.Focused)
                     btnQueryAll_Click(null, null);
                  break;
               case Keys.Escape:
                  break;
            }
         }
         this.Invalidate();
         return base.ProcessCmdKey(ref msg, keyData);
      }

      uint iIndex = 0;
      string[] strVolumes = { "創世記", "出埃及記", "利未記", "民數記", "申命記", "約書亞記",
                              "士師記", "路得記", "撒母耳記上", "撒母耳記下", "列王紀上", "列王紀下",
                              "歷代志上", "歷代志下", "以斯拉記", "尼希米記", "以斯帖記", "約伯記",
                              "詩篇", "箴言", "傳道書", "雅歌", "以賽亞書", "耶利米書", "耶利米哀歌",
                              "以西結書", "但以理書", "何西阿書", "約珥書", "阿摩司書", "俄巴底亞書", 
                              "約拿書", "彌迦書", "那鴻書", "哈巴谷書", "西番雅書", "哈該書", "撒迦利亞書",
                              "瑪拉基書", "馬太福音", "馬可福音", "路加福音", "約翰福音", "使徒行傳",
                              "羅馬書", "哥林多前書", "哥林多後書", "加拉太書", "以弗所書", "腓立比書",
                              "歌羅西書", "帖撒羅尼迦前書", "帖撒羅尼迦後書", "提摩太前書", "提摩太後書",
                              "提多書", "腓利門書", "希伯來書", "雅各書", "彼得前書", "彼得後書", "約翰一書",
                              "約翰二書", "約翰三書", "猶大書", "啟示錄" };

      string[] strVolumesAcronym = { "創", "出", "利", "民", "申", "書",
                                     "士", "得", "撒上", "撒下", "王上", "王下",
                                     "代上", "代下", "拉", "尼", "斯", "伯",
                                     "詩", "箴", "傳", "歌", "賽", "耶", "哀",
                                     "結", "但", "何", "珥", "摩", "俄",
                                     "拿", "彌", "鴻", "哈", "番", "該", "亞",
                                     "瑪", "太", "可", "路", "約", "徒",
                                     "羅", "林前", "林後", "加", "弗", "腓",
                                     "西", "帖前", "帖後", "提前", "提後",
                                     "多", "門", "來", "雅", "彼前", "彼後", "約一",
                                     "約二", "約三", "猶", "啟" };

      int iVol = 0;
      string[] strQuery = null;
      private void btnQueryAll_Click(object sender, EventArgs e)
      {
         //string[] strQuery = txtBoxQuery.Text.Split(new char[] { ' ' });
         strQuery = txtBoxQuery.Text.Split(new char[] { ' ' });
         if (strQuery.Length < 3)
         {
            MessageBox.Show("請確認輸入格式為:xx xx xx");
            return;
         }
         else if (strQuery.Length > 3)
         {
            MessageBox.Show("輸入空隔數太多\n請確認輸入格式為:xx xx xx");
            return;
         }
         if (strQuery[0].Length == 0 || strQuery[1].Length == 0 || strQuery[2].Length == 0)
         {
            MessageBox.Show("請確認輸入格式為:xx xx xx");
            return;
         }
         //int iVol = int.Parse(strQuery[0]);
         iVol = int.Parse(strQuery[0]);
         if (iVol < 1 || iVol > 66)
         {
            MessageBox.Show("請確認輸入卷號介於1~66之間");
            return;
         }

         string strSqlCmd = "";
         string strVolChapSecPage = "";

         strSqlCmd = "SELECT [no],volume,chap,sec,pageno,scripture ";
         strSqlCmd += "FROM bible " +
                      "WHERE volume = '" + strVolumes[iVol - 1] + "' " +
                      "AND chap = " + strQuery[1] + " " +
                      "AND sec = " + strQuery[2] + " ";

         Cmd.Connection = oleConn;
         Cmd.CommandType = CommandType.Text;
         OleDbDataReader DRdr;

         Cmd.CommandText = strSqlCmd;
         DRdr = Cmd.ExecuteReader();
         try
         {
            if (DRdr.Read())
            {
               iIndex = uint.Parse(DRdr[0].ToString());
                //複製到聽打
               if (chkBoxToTextBox1.Checked)
               {
                   lblVolChapSecPage.Text = strVolumesAcronym[iVol - 1] + DRdr[2].ToString() + ":"
                       + DRdr[3].ToString();
                   lblScripture.Text = DRdr[5].ToString();
               }
               else
               {
                   strVolChapSecPage = DRdr[1].ToString();
                   strVolChapSecPage += string.Format("{0,3:D}章", DRdr[2].ToString());
                   strVolChapSecPage += string.Format("{0,3:D}節", DRdr[3].ToString());
                   strVolChapSecPage += string.Format("{0,4:D}頁", DRdr[4].ToString());
                   lblVolChapSecPage.Text = strVolChapSecPage;
                   lblScripture.Text = string.Format("{0,2:D}", DRdr[3]) + "節  " + DRdr[5].ToString();
               }
            }
            else
            {
               lblVolChapSecPage.Text = "";
               lblScripture.Text = "";
               MessageBox.Show("查無此經節!");
            }
         }
         catch (OleDbException ex)
         {
            lblVolChapSecPage.Text = "";
            lblScripture.Text = "";
            MessageBox.Show("查無此經節!\n" + ex.Message);
         }
         finally
         {
            DRdr.Close();
            DRdr.Dispose();
         }

         //複製到聽打
         //if (chkBoxToTextBox1.Checked && lblVolChapSecPage.Text.Length > 0)
         if (chkBoxToTextBox1.Checked)
         {
             //設為focus
             textBox1.Focus();
             if (lblVolChapSecPage.Text.Length > 0)
             {
                 //目前游標位置
                 int currentPoint = textBox1.SelectionStart;
                 //經文
                 string tmpString = "(" + lblVolChapSecPage.Text + ")" + lblScripture.Text;
                 textBox1.Text = textBox1.Text.Substring(0, currentPoint) + tmpString + textBox1.Text.Substring(currentPoint);
                 //游標移至插入後的位置
                 int newPoint = currentPoint + tmpString.Length;
                 textBox1.Select(newPoint, 0);
                 //滾動到游標處
                 textBox1.ScrollToCaret();
             }
             SendText();
         }
         else
         {
             DefScriptureLocation();
             L_displayForm.SetTextOfVolChapSecPageScripture(lblVolChapSecPage.Text,
                                                          lblScripture.Text);
             R_displayForm.SetTextOfVolChapSecPageScripture(lblVolChapSecPage.Text,
                                                          lblScripture1.Text);
         }

         ShareMem.curDisplayMode = ShareMem.DisplayMode.Scripture;
         if (ShareMem.preDisplayMode != ShareMem.curDisplayMode)
         {
            ClearProc(ShareMem.preDisplayMode);
            L_displayForm.ClearProc(ShareMem.preDisplayMode);
            R_displayForm.ClearProc(ShareMem.preDisplayMode);
            SwitchProc();
            L_displayForm.SwitchProc();
            R_displayForm.SwitchProc();
            ShareMem.preDisplayMode = ShareMem.curDisplayMode;
         }
      }

      private bool bSubjectFirst = true;
      private void txtBoxSubject_Click(object sender, EventArgs e)
      {
         if (bSubjectFirst)
         {
            txtBoxSubject.Text = null;
            bSubjectFirst = false;
         }
      }

      private bool bQueryFirst = true;
      private void txtBoxQuery_Click(object sender, EventArgs e)
      {
         L_displayForm.SetColorOfOthers(colorDialog3.Color);
         R_displayForm.SetColorOfOthers(colorDialog3.Color);
         if (bQueryFirst)
         {
            txtBoxQuery.Text = null;
            btnNext.Enabled = true;
            btnLast.Enabled = true;
            bQueryFirst = false;
         }
         else
         {
            txtBoxQuery.SelectAll();
         }
      }

      private void btnLast_Click(object sender, EventArgs e)
      {

         if (!(chkBoxToTextBox1.Checked))
              if (ShareMem.curDisplayMode != ShareMem.DisplayMode.Scripture || iIndex == 0) return;
         string strSqlCmd = "";
         string strVolChapSecPage = "";

         strQuery[2] = (int.Parse(strQuery[2]) - 1).ToString();
         strSqlCmd = "SELECT [no],volume,chap,sec,pageno,scripture ";
         strSqlCmd += "FROM bible " +
                      "WHERE volume = '" + strVolumes[iVol - 1] + "' " +
                      "AND chap = " + strQuery[1] + " " +
                      "AND sec = " + strQuery[2] + " ";
         Cmd.Connection = oleConn;
         Cmd.CommandType = CommandType.Text;
         OleDbDataReader DRdr;

         Cmd.CommandText = strSqlCmd;
         DRdr = Cmd.ExecuteReader();
         try
         {
            if (DRdr.Read())
            {
               iIndex = uint.Parse(DRdr[0].ToString());
               //複製到聽打
               if (chkBoxToTextBox1.Checked)
               {
                   lblVolChapSecPage.Text = strVolumesAcronym[iVol - 1] + DRdr[2].ToString() + ":"
                       + DRdr[3].ToString();
                   lblScripture.Text = DRdr[5].ToString();
               }
               else
               {
                   strVolChapSecPage = DRdr[1].ToString();
                   strVolChapSecPage += string.Format("{0,3:D}章", DRdr[2].ToString());
                   strVolChapSecPage += string.Format("{0,3:D}節", DRdr[3].ToString());
                   strVolChapSecPage += string.Format("{0,4:D}頁", DRdr[4].ToString());
                   lblVolChapSecPage.Text = strVolChapSecPage;
                   lblScripture.Text = string.Format("{0,2:D}", DRdr[3]) + "節  " + DRdr[5].ToString();
               }
            }
            else
            {
               lblVolChapSecPage.Text = "";
               lblScripture.Text = "";
               MessageBox.Show("查無此經節!");
            }
         }
         catch (OleDbException ex)
         {
            lblVolChapSecPage.Text = "";
            lblScripture.Text = "";
            MessageBox.Show("查無此經節!\n" + ex.Message);
         }
         finally
         {
            DRdr.Close();
            DRdr.Dispose();
         }

         //複製到聽打
         //if (chkBoxToTextBox1.Checked && lblVolChapSecPage.Text.Length > 0)
         if (chkBoxToTextBox1.Checked)
         {
             //設為focus
             textBox1.Focus();
             if (lblVolChapSecPage.Text.Length > 0)
             {
                 //目前游標位置
                 int currentPoint = textBox1.SelectionStart;
                 //經文
                 string tmpString = "(" + lblVolChapSecPage.Text + ")" + lblScripture.Text;
                 textBox1.Text = textBox1.Text.Substring(0, currentPoint) + tmpString + textBox1.Text.Substring(currentPoint);
                 //游標移至插入後的位置
                 int newPoint = currentPoint + tmpString.Length;
                 textBox1.Select(newPoint, 0);
                 //滾動到游標處
                 textBox1.ScrollToCaret();
                 SendText();
             }
         }
         else
         {
             DefScriptureLocation();
             L_displayForm.SetTextOfVolChapSecPageScripture(lblVolChapSecPage.Text,
                                                          lblScripture.Text);
             R_displayForm.SetTextOfVolChapSecPageScripture(lblVolChapSecPage.Text,
                                                          lblScripture1.Text);
         }
      }

      private void btnNext_Click(object sender, EventArgs e)
      {
         if (!(chkBoxToTextBox1.Checked))
             if (ShareMem.curDisplayMode != ShareMem.DisplayMode.Scripture || iIndex == 0) return;
         string strSqlCmd = "";
         string strVolChapSecPage = "";

         strQuery[2] = (int.Parse(strQuery[2]) + 1).ToString();
         strSqlCmd = "SELECT [no],volume,chap,sec,pageno,scripture ";
         strSqlCmd += "FROM bible " +
                      "WHERE volume = '" + strVolumes[iVol - 1] + "' " +
                      "AND chap = " + strQuery[1] + " " +
                      "AND sec = " + strQuery[2] + " ";
         Cmd.Connection = oleConn;
         Cmd.CommandType = CommandType.Text;
         OleDbDataReader DRdr;

         Cmd.CommandText = strSqlCmd;
         DRdr = Cmd.ExecuteReader();
         try
         {
            if (DRdr.Read())
            {
               iIndex = uint.Parse(DRdr[0].ToString());
               //複製到聽打
               if (chkBoxToTextBox1.Checked)
               {
                   lblVolChapSecPage.Text = strVolumesAcronym[iVol - 1] + DRdr[2].ToString() + ":"
                       + DRdr[3].ToString();
                   lblScripture.Text = DRdr[5].ToString();
               }
               else
               {
                   strVolChapSecPage = DRdr[1].ToString();
                   strVolChapSecPage += string.Format("{0,3:D}章", DRdr[2].ToString());
                   strVolChapSecPage += string.Format("{0,3:D}節", DRdr[3].ToString());
                   strVolChapSecPage += string.Format("{0,4:D}頁", DRdr[4].ToString());
                   lblVolChapSecPage.Text = strVolChapSecPage;
                   if (chkBoxScripture.Checked)
                       lblScripture.Text += Environment.NewLine + string.Format("{0,2:D}", DRdr[3]) + "節  " + DRdr[5].ToString();
                   else
                       lblScripture.Text = string.Format("{0,2:D}", DRdr[3]) + "節  " + DRdr[5].ToString();
               }
            }
            else
            {
               lblVolChapSecPage.Text = "";
               lblScripture.Text = "";
               MessageBox.Show("查無此經節!");
            }
         }
         catch (OleDbException ex)
         {
            lblVolChapSecPage.Text = "";
            lblScripture.Text = "";
            MessageBox.Show("查無此經節!\n" + ex.Message);
         }
         finally
         {
            DRdr.Close();
            DRdr.Dispose();
         }

         //複製到聽打
         //if (chkBoxToTextBox1.Checked && lblVolChapSecPage.Text.Length > 0)
         if (chkBoxToTextBox1.Checked)
         {
             //設為focus
             textBox1.Focus();
             if (lblVolChapSecPage.Text.Length > 0)
             {
                 //目前游標位置
                 int currentPoint = textBox1.SelectionStart;
                 //經文
                 string tmpString = "(" + lblVolChapSecPage.Text + ")" + lblScripture.Text;
                 textBox1.Text = textBox1.Text.Substring(0, currentPoint) + tmpString + textBox1.Text.Substring(currentPoint);
                 //游標移至插入後的位置
                 int newPoint = currentPoint + tmpString.Length;
                 textBox1.Select(newPoint, 0);
                 //滾動到游標處
                 textBox1.ScrollToCaret();
                 SendText();
             }
         }
         else
         {
             DefScriptureLocation();
             L_displayForm.SetTextOfVolChapSecPageScripture(lblVolChapSecPage.Text,
                                                          lblScripture.Text);
             R_displayForm.SetTextOfVolChapSecPageScripture(lblVolChapSecPage.Text,
                                                          lblScripture1.Text);
         }
      }

      //經文-使用大版面
      private void DefScriptureLocation()
      {
          this.lblScripture.Location = new System.Drawing.Point(2, 127);
          this.lblScripture.Size = new System.Drawing.Size(396, 160);
          L_displayForm.SetScriptureLocation("L");
          L_displayForm.SetScriptureSize("L");
          this.lblScripture1.Location = new System.Drawing.Point(2, 127);
          this.lblScripture1.Size = new System.Drawing.Size(396, 160);
          R_displayForm.SetScriptureLocation("R");
          R_displayForm.SetScriptureSize("R");
      }

      private void SwitchProc()
      {
         //Current display mode
         switch (ShareMem.preDisplayMode)
         {
            case ShareMem.DisplayMode.Scripture:
               if (lblVolChapSecPage.Visible)
                  lblVolChapSecPage.Visible = false;
               if (lblScripture.Visible || lblScripture1.Visible)
               {
                   lblScripture.Visible = false;
                   lblScripture1.Visible = false;
               }
               break;
            case ShareMem.DisplayMode.LoadText:
            case ShareMem.DisplayMode.Text:
               if (lblScripture.Visible || lblScripture1.Visible)
               {
                   lblScripture.Visible = false;
                   lblScripture1.Visible = false;
               }
               break;
            case ShareMem.DisplayMode.Poem:
               if (lblNewPoem.Visible)
                  lblNewPoem.Visible = false;
               if (lblOldPoem.Visible)
                  lblOldPoem.Visible = false;
               if (lblName2.Visible)
                  lblName2.Visible = false;
               if (lblName3.Visible)
                  lblName3.Visible = false;
               if (btnCallPPS.Enabled)
                  btnCallPPS.Enabled = false;
               break;
            default:
               break;
         }
         //Next display mode
         switch (ShareMem.curDisplayMode)
         {
            case ShareMem.DisplayMode.Scripture:
               if (!lblVolChapSecPage.Visible)
                  lblVolChapSecPage.Visible = true;
               if (radBtnDisplay.Checked == true)
               {
                   lblScripture.Visible = false;
                   lblScripture1.Visible = false;
               }
               else if (radBtnLeftDisplay.Checked == true)
               {
                   lblScripture.Visible = true;
                   lblScripture.Text = lblLeftScripture.Text;
                   lblScripture1.Visible = false;
               }
               else if (radBtnRightDisplay.Checked == true)
               {
                   lblScripture1.Visible = true;
                   lblScripture1.Text = lblRightScripture.Text;
                   lblScripture.Visible = false;
               }
               break;
            case ShareMem.DisplayMode.LoadText:
            case ShareMem.DisplayMode.Text:
               if (radBtnDisplay.Checked == true)
               {
                   lblScripture.Visible = false;
                   lblScripture1.Visible = false;
               }
               else if (radBtnLeftDisplay.Checked == true)
               {
                   lblScripture.Visible = true;
                   lblScripture.Text = lblLeftScripture.Text;
                   lblScripture1.Visible = false;
               }
               else if (radBtnRightDisplay.Checked == true)
               {
                   lblScripture1.Visible = true;
                   lblScripture1.Text = lblRightScripture.Text;
                   lblScripture.Visible = false;
               }
               break;
            case ShareMem.DisplayMode.Poem:
               if (!lblNewPoem.Visible)
                  lblNewPoem.Visible = true;
               if (!lblOldPoem.Visible)
                  lblOldPoem.Visible = true;
               if (!lblName2.Visible)
                  lblName2.Visible = true;
               if (!lblName3.Visible)
                  lblName3.Visible = true;
               break;
            default:
               break;
         }
      }

      private void ClearProc(ShareMem.DisplayMode clearDislayMode)
      {
         switch (clearDislayMode)
         {
            case ShareMem.DisplayMode.Scripture:
               if (lblVolChapSecPage.Visible)
                  lblVolChapSecPage.Text = "";
               if ((lblScripture.Visible || lblScripture1.Visible) 
                   && ShareMem.curDisplayMode != ShareMem.DisplayMode.LoadText
                   && ShareMem.curDisplayMode != ShareMem.DisplayMode.Text)
               {
                   lblScripture.Text = "";
                   lblScripture1.Text = "";
               }
               break;
            case ShareMem.DisplayMode.LoadText:
               if ((lblScripture.Visible || lblScripture1.Visible) 
                   && ShareMem.curDisplayMode != ShareMem.DisplayMode.Scripture
                   && ShareMem.curDisplayMode != ShareMem.DisplayMode.Text)
               {
                   lblScripture.Text = "";
                   lblScripture1.Text = "";
               }
               break;
            case ShareMem.DisplayMode.Text:
               if ((lblScripture.Visible || lblScripture1.Visible) 
                   && ShareMem.curDisplayMode != ShareMem.DisplayMode.Scripture
                   && ShareMem.curDisplayMode != ShareMem.DisplayMode.LoadText)
               {
                   lblScripture.Text = "";
                   lblScripture1.Text = "";
               }
               break;
            case ShareMem.DisplayMode.Poem:
               if (lblNewPoem.Visible)
                  lblNewPoem.Text = "";
               if (lblOldPoem.Visible)
                  lblOldPoem.Text = "";
               if (lblName2.Visible)
                  lblName2.Text = "";
               if (lblName3.Visible)
                  lblName3.Text = "";
               break;
            default:
               break;
         }
      }

      private void btnClear_Click(object sender, EventArgs e)
      {
         ClearProc(ShareMem.curDisplayMode);
         L_displayForm.ClearProc(ShareMem.curDisplayMode);
         R_displayForm.ClearProc(ShareMem.curDisplayMode); 
      }

      private void MainFrame_FormClosing(object sender, FormClosingEventArgs e)
      {
         Register reg = new Register();
         reg.SetFontNameOfSubject(txtBoxSubject.Font.Name);
         reg.SetFontSizeOfSubject(txtBoxSubject.Font.Size);
         reg.SetFontStyleOfSubject((int)txtBoxSubject.Font.Style);

         reg.SetColorOfSubject(colorDialog1.Color.ToArgb());
         reg.SetColorOfTitle(colorDialog2.Color.ToArgb());
         reg.SetColorOfOthers(colorDialog3.Color.ToArgb());

         reg.SetFontNameOfOthers(textBox1.Font.Name);
         reg.SetFontSizeOfOthers(textBox1.Font.Size);
         reg.SetFontStyleOfOthers((int)textBox1.Font.Style);

         reg.SetPathOfBackgroundImage(openFileDialog1.FileName);
         reg.SetCheckScripture(chkBoxScripture.Checked);
         reg.SetCheckToTextBox1(chkBoxToTextBox1.Checked);

         if (oleConn != null)
         {
            oleConn.Close();
            oleConn.Dispose();
         }
      }

      //文字(公告事項)
      private void btnSendLoadedText_Click(object sender, EventArgs e)
      {
         L_displayForm.SetColorOfOthers(colorDialog3.Color);
         R_displayForm.SetColorOfOthers(colorDialog3.Color);
         string strSelected = "";
         int iCount = listBox6.SelectedItems.Count;
         for (int i = 0; i < iCount; i++)
            strSelected += listBox6.SelectedItems[i].ToString() + Environment.NewLine;
         for (int j = 0; j < iCount; j++)
            listBox6.SetSelected(listBox6.SelectedIndices[0], false);

         lblVolChapSecPage.Text = "";
         lblScripture.Text = strSelected;
         if (chkBoxToTextBox1.Checked)
         {
             L_displayForm.SetTextOfVolChapSecPageScripture("HIDE", lblScripture.Text);
             R_displayForm.SetTextOfVolChapSecPageScripture("HIDE", lblScripture.Text);
         }
         else
         {
             L_displayForm.SetTextOfVolChapSecPageScripture(lblVolChapSecPage.Text,
                                                          lblScripture.Text);
             R_displayForm.SetTextOfVolChapSecPageScripture(lblVolChapSecPage.Text,
                                                lblScripture.Text);
         }

         ShareMem.curDisplayMode = ShareMem.DisplayMode.LoadText;
         if (ShareMem.preDisplayMode != ShareMem.curDisplayMode)
         {
            ClearProc(ShareMem.preDisplayMode);
            L_displayForm.ClearProc(ShareMem.preDisplayMode);
            R_displayForm.ClearProc(ShareMem.preDisplayMode);
            SwitchProc();
            L_displayForm.SwitchProc();
            R_displayForm.SwitchProc();
            ShareMem.preDisplayMode = ShareMem.curDisplayMode;
         }
      }

      //Click &  TextChanged
      private void btnSendText_Click(object sender, EventArgs e)
      {
          SendText();
      }

      //KeyUp
      private void btnSendText_Click(object sender, KeyEventArgs e)
      {
          SendText();
      }

      //文字(即時聽打)
      private void SendText()
      {
          //目前游標位置
          int currentPoint = textBox1.SelectionStart;
          //半形轉全形
          textBox1.Text = Strings.StrConv(textBox1.Text, VbStrConv.Wide, 0);
          //游標移回原處
          textBox1.Select(currentPoint, 0);
          //滾動到游標處
          textBox1.ScrollToCaret();

          //copy 到 LeftScripture、RightScripture (預覽使用)
          lblLeftScripture.Text = DealwithContxt(textBox1.Text, ShareMem.contentLength, ShareMem.contentLines, currentPoint);
          lblRightScripture.Text = DealwithContxt(textBox1.Text, ShareMem.contentLength1, ShareMem.contentLines1, currentPoint);
          //copy 到 lblScripture、lblScripture1 (控制台使用)
          lblScripture.Text = lblLeftScripture.Text;
          lblScripture1.Text = lblRightScripture.Text;

          L_displayForm.SetColorOfOthers(colorDialog3.Color);
          R_displayForm.SetColorOfOthers(colorDialog3.Color);
          lblVolChapSecPage.Text = "";

          //確認經文是否使用大版面
          if (chkBoxToTextBox1.Checked == false)
          {
              this.lblScripture.Location = new System.Drawing.Point(ShareMem.currentX, ShareMem.currentY);
              this.lblScripture.Size = new System.Drawing.Size(ShareMem.contentWidth, ShareMem.contentHeight);
              L_displayForm.SetScriptureLocation("L");
              L_displayForm.SetScriptureSize("L");
              this.lblScripture1.Location = new System.Drawing.Point(ShareMem.currentX1, ShareMem.currentY1);
              this.lblScripture1.Size = new System.Drawing.Size(ShareMem.contentWidth1, ShareMem.contentHeight1);
              R_displayForm.SetScriptureLocation("R");
              R_displayForm.SetScriptureSize("R");
          }

          if (radBtnDisplay.Checked == true)
          {
              lblScripture.Hide();
              lblScripture1.Hide();
          }
          else if (radBtnLeftDisplay.Checked == true)
          {
              if (chkBoxToTextBox1.Checked) lblScripture.Hide();
              else lblScripture.Show();
              lblScripture.Text = lblLeftScripture.Text;
              lblScripture1.Hide();
          }
          else if (radBtnRightDisplay.Checked == true)
          {
              if (chkBoxToTextBox1.Checked) lblScripture1.Hide();
              else lblScripture1.Show();
              lblScripture1.Text = lblRightScripture.Text;
              lblScripture.Hide();
          }

          L_displayForm.SetTextOfVolChapSecPageScripture("HIDE",
                                                       lblScripture.Text);
          R_displayForm.SetTextOfVolChapSecPageScripture("HIDE",
                                                       lblScripture1.Text);

          ShareMem.curDisplayMode = ShareMem.DisplayMode.Text;
          if (ShareMem.preDisplayMode != ShareMem.curDisplayMode)
          {
              ClearProc(ShareMem.preDisplayMode);
              L_displayForm.ClearProc(ShareMem.preDisplayMode);
              R_displayForm.ClearProc(ShareMem.preDisplayMode);
              SwitchProc();
              L_displayForm.SwitchProc();
              R_displayForm.SwitchProc();
              ShareMem.preDisplayMode = ShareMem.curDisplayMode;
          }
      }

      //產生固定長度Text
      private string DealwithContxt(string contxt, int length, int lines, int point)
      {
          StringBuilder result = new StringBuilder();

          int len = 0;
          int star = 0;
          int tmplength = length * 2;

          int varLen1 = contxt.Length / (lines * length); //字整
          int varLen2 = point / (lines * length);   //游整
          int varLen3 = (contxt.Length - (varLen1 * lines * length)) / length;  //字餘行
          int varLen4 = (point - (varLen2 * lines * length)) / length;  //游餘行

          if (contxt.Length > 0)
          {
              if (varLen1 == varLen2)
              {
                  if ((contxt.Length / length) > lines)
                  {
                      contxt = contxt.Substring(((contxt.Length / length) - lines) * length);
                  }
              }
              else
              {
                  if ((point / length) > lines)
                  {
                      int varLen5 = contxt.Length - (((point / length) - (lines / 2)) * length) - (lines * length);
                      if (varLen5 > 0)
                          contxt = contxt.Substring(((point / length) - (lines / 2)) * length, lines * length);
                      else
                          contxt = contxt.Substring(((point / length) - (lines / 2) + (varLen5 / length) - 1) * length, lines * length);
                  }
                  else
                  {
                      contxt = contxt.Substring(0, lines * length);
                  }
              }
          }
          
          //目前字數
          int conLen = contxt.Length;
          //去除換行符號
          string conNoNewLine = contxt.Replace("\r\n", "");
          int conLenNoNewLine = conNoNewLine.Length;
          //無換行符號
          if (conLen == conLenNoNewLine)
          {
              for (int i = 0; i < contxt.Length; i++)
              {
                  byte[] byte_len = Encoding.Default.GetBytes(contxt.Substring(i, 1));
                  if (byte_len.Length > 1)
                      len += 2;
                  else
                      len += 1;
                  
                  if (len >= tmplength)
                  {
                      result.AppendLine(contxt.Substring(star, length));
                      star = i + 1;
                      len = 0;
                  }
                  else if (i == contxt.Length - 1)
                  {
                      result.AppendLine(contxt.Substring(star));
                      star = i;
                      len = 0;
                  }
              }
          }
          else
          //有換行符號
          {
              string[] conNewLine = contxt.Split(new string[] {"\r\n"}, StringSplitOptions.RemoveEmptyEntries);
              for (int i = 0; i < conNewLine.Length; i++)
              {
                  if (conNewLine[i].Length > length)
                  {
                      len = 0;
                      star = 0;
                      for (int j = 0; j < conNewLine[i].Length; j++)
                      {
                          byte[] byte_len = Encoding.Default.GetBytes(conNewLine[i].Substring(j, 1));
                          if (byte_len.Length > 1)
                              len += 2;
                          else
                              len += 1;
                          
                          if (len >= tmplength)
                          {
                              result.AppendLine(conNewLine[i].Substring(star, length));
                              star = j + 1;
                              len = 0;
                          }
                          else if (j == conNewLine[i].Length - 1)
                          {
                              result.AppendLine(conNewLine[i].Substring(star));
                              star = j;
                              len = 0;
                          }
                      }
                  }
                  else
                  {
                      result.AppendLine(conNewLine[i]);
                  }
              }
              
              //只顯示最後的幾行
              contxt = result.ToString();
              string[] conNewLineTmp = contxt.Split(new string[] { "\r\n" }, StringSplitOptions.RemoveEmptyEntries);
              if (conNewLineTmp.Length > lines)
              {
                  result.Remove(0, result.Length);
                  for (int i = (conNewLineTmp.Length - lines); i < conNewLineTmp.Length; i++)
                  {
                      result.AppendLine(conNewLineTmp[i]);
                  }
              }
          }

          return result.ToString();
      }

      private Process pPPS;
      private void btnCallPPS_Click(object sender, EventArgs e)
      {
         string myText = Application.StartupPath + "\\Songs of Praise\\";
         myText += txtBoxSong3.Text;

         if (txtBoxSong3.Text.Equals("51") || txtBoxSong3.Text.Equals("124") ||
             txtBoxSong3.Text.Equals("132") || txtBoxSong3.Text.Equals("274") ||
             txtBoxSong3.Text.Equals("296"))
         {
            myText +=  comboBox1.SelectedItem;
         }
         myText += ".pps";

         ProcessStartInfo pInfo = new ProcessStartInfo();
         pInfo.FileName = myText;
         pInfo.UseShellExecute = true;
         try
         {
            pPPS = Process.Start(pInfo);
         }
         catch (Exception ex)
         {
            MessageBox.Show(ex.Message + Environment.NewLine + myText);
         }
         if (btnCallPPS.Enabled) btnCallPPS.Enabled = false;
      }

      private Process pText;
      private void btnEditText_Click(object sender, EventArgs e)
      {
         string myText = Application.StartupPath + "\\視訊系統文字.txt";

         ProcessStartInfo pInfo = new ProcessStartInfo();
         pInfo.FileName = myText;
         pInfo.UseShellExecute = true;
         try
         {
            pText = Process.Start(pInfo);
         }
         catch (Exception ex)
         {
            MessageBox.Show(ex.Message + Environment.NewLine + myText);
         }
      }

      private void btnReloadText_Click(object sender, EventArgs e)
      {
         if (pText != null && pText.HasExited == false)
         {
            MessageBox.Show("請先存檔後，再關閉記事本.");
            return;
         }
         try
         {
            using (StreamReader myStreamReader = new StreamReader(Application.StartupPath + "\\視訊系統文字.txt", Encoding.Default))
            {
               listBox6.Items.Clear();
               string strReadLine = myStreamReader.ReadLine();
               while (strReadLine != null)
               {
                  listBox6.Items.Add(strReadLine);
                  strReadLine = myStreamReader.ReadLine();
               }
               myStreamReader.Close();
            }
         }
         catch (Exception ex)
         {
            MessageBox.Show(ex.Message);
         }
      }

      private void OnlineHelpToolStripMenuItem_Click(object sender, EventArgs e)
      {
         string myPdf = Application.StartupPath + "\\教會視訊程式操作說明.pdf";

         ProcessStartInfo pInfo = new ProcessStartInfo();
         pInfo.FileName = myPdf;
         pInfo.UseShellExecute = true;
         try
         {
            Process.Start(pInfo);
         }
         catch (Exception ex)
         {
            MessageBox.Show(ex.Message + Environment.NewLine + myPdf);
         }
      }

      private void AboutToolStripMenuItem1_Click(object sender, EventArgs e)
      {
         using (AboutBox aboutBox = new AboutBox())
         {
            aboutBox.ShowDialog();
         }
      }

      private void btnHideName1Song1_Click(object sender, EventArgs e)
      {
          if (boolName1Song1 == true)
          {
              txtBoxName1.Visible = false;
              comboBoxTitle1.Visible = false;
              lblPoem1.Visible = false;
              txtBoxSong1.Visible = false;
              lblComma.Visible = false;
              txtBoxSong2.Visible = false;
              lblPoem2.Visible = false;
              boolName1Song1 = false;
              L_displayForm.SetShowStatus(false);
              R_displayForm.SetShowStatus(false);
              btnHideName1Song1.Text = "顯示";
          }
          else
          {
              txtBoxName1.Visible = true;
              comboBoxTitle1.Visible = true;
              lblPoem1.Visible = true;
              txtBoxSong1.Visible = true;
              lblComma.Visible = true;
              txtBoxSong2.Visible = true;
              lblPoem2.Visible = true;
              boolName1Song1 = true;
              L_displayForm.SetShowStatus(true);
              R_displayForm.SetShowStatus(true);
              btnHideName1Song1.Text = "隱藏";
          }
      }

      private void chkScripture_Click(object sender, EventArgs e)
      {
          if (chkBoxScripture.Checked == true)
              chkBoxToTextBox1.Enabled = false;
          else
              chkBoxToTextBox1.Enabled = true;

          if (chkBoxToTextBox1.Checked == true)
              chkBoxScripture.Enabled = false;
          else
              chkBoxScripture.Enabled = true;
      }

      //存檔
      string docFile = "";
      private void btnSaveDoc_Click(object sender, EventArgs e)
      {
          string dt = System.DateTime.Now.ToString("yyyyMMddHHmmss");
          Register reg = new Register();

          SaveAsDoc sDoc = new SaveAsDoc();
          if (!txtBoxSubject.Text.Equals("請輸入講題"))
            sDoc.SetSubject(txtBoxSubject.Text);
          sDoc.SetSubjectSize((int) txtBoxSubject.Font.Size);
          sDoc.SetSubjectFontName(txtBoxSubject.Font.Name);
          if (txtBoxName1.Text.Length > 0)
            sDoc.SetName1("主領：" + txtBoxName1.Text + comboBoxTitle1.Text);
          if (txtBoxSong1.Text.Length > 0 || txtBoxSong2.Text.Length > 0)
            sDoc.SetSong("詩" + txtBoxSong1.Text + "、" + txtBoxSong2.Text + "首");
          if (txtBoxName2.Text.Length > 0)
            sDoc.SetName2("翻譯：" + txtBoxName2.Text + comboBoxTitle2.Text);
          sDoc.SetText(textBox1.Text);
          sDoc.SetTextSize((int) textBox1.Font.Size);
          sDoc.SetTextFontName(textBox1.Font.Name);
          if (docFile.Length == 0)
            docFile = reg.GetFilePath() + "\\" + dt.ToString() + "_" + txtBoxName1.Text + "_" + txtBoxSubject.Text + ".doc";
          sDoc.SetFileName(docFile);
          sDoc.SetSaveAsDoc();
          label5.Text = "檔案：" + docFile;
          btnNewDoc.Enabled = true;
          MessageBox.Show("存檔成功\r\n" + docFile);
      }

      //新檔
      private void btnNewDoc_Click(object sender, EventArgs e)
      {
          docFile = "";
          label5.Text = "檔案：無";
          btnNewDoc.Enabled = false;
      }

      //清空聽打
      private void btnCleanText_Click(object sender, EventArgs e)
      {
          textBox1.Text = "";
      }
   }
}